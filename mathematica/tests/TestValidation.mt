(*  qmmlpack                     *)
(*  Matthias Rupp, 2006-2016.    *)
(*  See LICENSE.txt for license. *)

(*  Tests for validation.  *)

Needs["QMMLPack`"];
Needs["ApproximatelyEqual`", "ApproximatelyEqual.m"];
Needs["Developer`"];
On[Assert];

(*  ********************  *)
(*  *  Stratification  *  *)
(*  ********************  *)

(* Invalid arguments return unevaluated *)
VerificationTest[
    {
        Stratify[{1,2,3,4}, 0],  (* no strata *)
        Stratify[{1,2}, 3],  (* number of strata larger than number of indices *)
        Stratify[{a,b,c}, 2]   (* non-numerical labels *)
    }
    ,
    {
        Stratify[{1,2,3,4}, 0],
        Stratify[{1,2}, 3],
        Stratify[{a,b,c}, 2]
    }
    ,
    TestID->"Evaluation-20160912-B0Q5R1"
]

checkStratification[labels_, list_, n_Integer, k_Integer, checkRange_: True] := Module[
    {m,h,tmp,i,cond1,cond2,cond3,cond4,cond5},

    (* 1) Number of lists must be k *)
    cond1 = (Length[list] == k);
    If[Not[cond1], Print["Stratification condition 1 failed for ", list]];

    (* 2) Lists must have right lengths *)
    {m, h} = QuotientRemainder[n, k];
    tmp = Map[m + If[# <= h, 1, 0] &, Range[k]];
    cond2 = Map[Length, list] == tmp;
    If[Not[cond2], Print["Stratification condition 2 failed for ", list]];

    (* 3) Each index must be used once. *)
    cond3 = ( Sort[Flatten[list]] == Union[Flatten[list]] );
    If[Not[cond3], Print["Stratification condition 3 failed for ", list]];

    (* 4) Index range must be 1,...,n *)
    cond4 = If[checkRange, ( Sort[Flatten[list]] == Range[n] ), True];
    If[Not[cond4], Print["Stratification condition 4 failed for ", list]];

    (* 5) Labels must increase per list *)
    cond5 = And @@ Table[Max[labels[[list[[i]]]]] <= Min[labels[[Flatten[list[[i+1;;]]]]]],{i,k-1}];
    If[Not[cond5], Print["Stratification condition 5 failed for ", list]];

    cond1 && cond2 && cond3 && cond4 && cond5
];

VerificationTest[
    Module[
        {labels1,labels2,labels3,labels4,labels5,k1,k2,k3,k4,k5},

        labels1 = Reverse[Range[10]]; k1 = 3;
        labels2 = Range[11]; k2 = 3;
        labels3 = Range[12]; k3 = 3;
        labels4 = RandomReal[{-1,1}, 30]; k4 = 17;
        labels5 = Range[3]; k5 = 3;
        {
            checkStratification[labels1, Stratify[labels1, k1], Length[labels1], k1],
            checkStratification[labels2, Stratify[labels2, k2], Length[labels2], k2],
            checkStratification[labels3, Stratify[labels3, k3], Length[labels3], k3],
            checkStratification[labels4, Stratify[labels4, k4], Length[labels4], k4],
            checkStratification[labels5, Stratify[labels5, k5], Length[labels5], k5]
        }
    ]
    ,
    ConstantArray[True, 5]
    ,
    TestID->"Evaluation-20160912-J2I0O4"
]

VerificationTest[
    {
        Stratify[Range[10], 3],
        Stratify[{9,1},1]
    }
    ,
    {
        {{1,2,3,4}, {5,6,7}, {8,9,10}},
        {{2,1}}
    }
    ,
    TestID->"Evaluation-20160912-W5T5C5"
]


(*  *********************  *)
(*  *  LocalGridSearch  *  *)
(*  *********************  *)

VerificationTest[
    Module[{c1,f1},
        c1 = 0; f1 = Function[{arg}, ++c1; 1.];
        N[{
            LocalGridSearch[((#-1)^2 &), {{"value" -> 2, "stepsize" -> 1, "direction" -> -1}}, "MaxEvaluations" -> 5],
            LocalGridSearch[Function[{a,b}, 3*a+2*b], {{0, 1, 1., -5, 5, 1, 2.718}, {2, 1, 1., -5, 5, 0, 2.718}}, "MaxEvaluations" -> 30],
            LocalGridSearch[f1, {{0., 1, 1, 0, 0, 0, 2, {False, False}}, {0., 1, 1, 0, 0, 0, 2, {False, False}}}, "MaxEvaluations" -> 30], c1
        }]
    ]
    ,
    N[{
        {{0.}, 0.},
        {{-5., -5.}, 3*Power[2.718,-5]+2*Power[2.718,-5]},
        {{0., 0.}, 1.}, 1
    }]
    ,
    {
        LocalGridSearch::warning, LocalGridSearch::warning
    }
    ,
    TestID->"Validation-20180511-LocalGridSearch-er3"
]

(* direction must not influence result for non-degenerate cases *)
VerificationTest[
    {
        LocalGridSearch[(#&), {{3., 1, 1., -5, 5, +1, 2., {False, False}}}, "MaxEvaluations" -> 15],
        LocalGridSearch[(#&), {{3., 1, 1., -5, 5, -1, 2., {False, False}}}, "MaxEvaluations" -> 15],
        LocalGridSearch[(#&), {{3., 1, 1., -5, 5,  0, 2., {False, False}}}, "MaxEvaluations" -> 15]
    }
    ,
    {
        {{-5.}, 2^-5.},
        {{-5.}, 2^-5.},
        {{-5.}, 2^-5.}
    }
    ,
    TestID->"Validation-20180511-LocalGridSearch-jDo"
]

(* evaluation monitor *)
VerificationTest[
    Module[{f, em, state, bestv, bestf},
        f[x_] := (x-1)^2;
        state = {};
        em[trialv_, trialf_, bestv_, bestf_, statep_] := AppendTo[state, statep];
        {bestv, bestf} = LocalGridSearch[f, {{"value"->2, "stepsize"->1, "direction"->-1}}, EvaluationMonitor->em];
        N[{bestv, bestf, state[[-1]]["num_evals"]}]
    ]
    ,
    N[{{0.}, 0., 4}]
    ,
    TestID->"Validation-20180512-LocalGridSearch-kKl"
]

(* warnings for parameter boundaries *)
VerificationTest[
    N[{
        LocalGridSearch[(+#&), {{3., 1, 1., -5, 5, +1, 2.}}, "MaxEvaluations" -> 15],
        LocalGridSearch[(-#&), {{3., 1, 1., -5, 5, +1, 2.}}, "MaxEvaluations" -> 15],
        LocalGridSearch[(+#&), {{3., 1, 1., -5, 5, +1, 2., {False, False}}}, "MaxEvaluations" -> 15],
        LocalGridSearch[(-#&), {{3., 1, 1., -5, 5, +1, 2., {False, False}}}, "MaxEvaluations" -> 15]
    }]
    ,
    N[{
        {{-5.}, 2^-5},
        {{+5.}, -2^5},
        {{-5.}, 2^-5},
        {{+5.}, -2^5}
    }]
    ,
    {
        LocalGridSearch::warning,
        LocalGridSearch::warning
        (* no warning *)
        (* no warning *)
    }
    ,
    TestID->"Validation-20180511-LocalGridSearch-901"
]
