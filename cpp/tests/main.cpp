// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Main executable for unit tests of C++ functionality of qmmlpack library

#define CATCH_CONFIG_MAIN // Provide main function executing tests
#define CATCH_CONFIG_FAST_COMPILE
#include "catch/catch.hpp"
