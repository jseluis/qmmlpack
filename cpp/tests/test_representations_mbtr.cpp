// qmmlpack
// (c) Matthias Rupp, 2006-2016.
// See LICENSE.txt for license.

// Unit tests for many-body tensor representation

#include "catch/catch.hpp"
#include "qmmlpack/representations_mbtr.hpp"

#include <cstdint>
#include <cmath>
#include <iostream>
#include <tuple>
#include <type_traits>
#include <numeric>
#include <algorithm>

using namespace qmml;

// Contents
// --------

// Test systems            -  finite and periodic pre-defined atomistic systems as test cases [FiniteTestSystems, PeriodicTestSystems]
// Auxiliary functions     -  rounding of floating point vectors for comparison, easy MBTR call [round, mbtr_call]
// Geometry functions      -  tests for each geometry function
// Weighting functions     -  tests for each weighting function
// Distribution functions  -  tests for each distribution function
// Correlation functions   -  currently no tests
// Element indexing        -  tests for each element indexing function
// Atom indexing           -  tests for each atom indexing function
// Many-body tensor        -  tests for some parametrizations of MBTR

//  ////////////////////
//  //  Test systems  //
//  ////////////////////

// Factored out functionality for atomistic test systems
class TestSystems
{
    public:
        TestSystems() : ns_(0), bb_(nullptr), zz_(nullptr), rr_(nullptr), sizes_(nullptr), inds_(nullptr), ne_(0), els_(nullptr), tinds_count_(0) {}

        ~TestSystems()
        {
            delete[] bb_;
            delete[] zz_;
            delete[] rr_;
            delete[] sizes_;
            delete[] inds_;
            delete[] els_;
        }

        void debug()
        {
            using std::vector; using std::endl; using std::cout;
            const long n = inds_[ns_-1] + sizes_[ns_-1];

            cout << endl << endl << "ns = "  << ns_ << endl;
            cout << "zz = "; for(long i = 0; i < ns_; ++i) { cout << endl << "  "; for(long j = 0; j < sizes_[i]; ++j) cout << zz_[inds_[i]+j] << " "; } cout << endl;
            if( bb_ != nullptr ) { cout << "bb = "; for(long i = 0; i < ns_; ++i) { cout << endl << "  "; for(long j = 0; j < 3; ++j) { cout << bb_[(inds_[i]+j)*3+0] << " " << bb_[(inds_[i]+j)*3+1] << " " << bb_[(inds_[i]+j)*3+2]; if(!(i == ns_-1 && j == 2)) cout << endl << "  "; } } cout << endl; }
            cout << "rr = "; for(long i = 0; i < ns_; ++i) { cout << endl << "  "; for(long j = 0; j < sizes_[i]; ++j) { cout << rr_[(inds_[i]+j)*3+0] << "  " << rr_[(inds_[i]+j)*3+1] << "  " << rr_[(inds_[i]+j)*3+2]; if(!(i == ns_-1 && j == sizes_[i]-1)) cout << endl << "  "; } } cout << endl;
            cout << "sizes = "; for(auto i : vector<long>(sizes_, sizes_+ns_)) cout << i << " "; cout << endl;
            cout << "inds = "; for(auto i : vector<long>(inds_, inds_+ns_)) cout << i << " "; cout << endl;
            cout << "ne = " << ne_ << "; els = "; for(auto i : vector<long>(els_, els_+ne_)) cout << i << " "; cout << endl;
            cout << std::flush;
        };

    public:
        long ns_;        // number of atomistic systems
        double * bb_;    // unit cell basis vectors (null for finite systems)
        long * zz_;      // atomic numbers
        double * rr_;    // atomic coordinates (Cartesian) / Angstrom
        long * sizes_;   // number of atoms per system
        long * inds_;    // starting index of each system
        long ne_;        // number of elements
        long * els_;     // elements in dataset

    private:
        std::vector<long> tzz_, tsizes_, tinds_, tels_;
        std::vector<double> tbb_, trr_;
        long tinds_count_;

    protected:
        void add_system(std::vector<double> const & b, std::vector<long> const & z, std::vector<double> const & r)
        {
            ++ns_;
            tsizes_.push_back( z.size() );
            tbb_.insert( tbb_.end(), b.begin(), b.end() );
            tzz_.insert( tzz_.end(), z.begin(), z.end() );
            trr_.insert( trr_.end(), r.begin(), r.end() );
            tinds_.push_back( tinds_count_ ); tinds_count_ += tsizes_.back();
            tels_.insert( tels_.end(), z.begin(), z.end() );
        }

        void add_system(std::vector<long> const & z, std::vector<double> const & r) { add_system({}, z, r); }

        void finalize()
        {
            // ns_ remains as is
            if( tbb_.size() == 0 ) bb_ = nullptr; else
            {
                bb_ = new double[tbb_.size()]; std::copy(tbb_.begin(), tbb_.end(), bb_);
            }
            zz_ = new long[tzz_.size()]; std::copy(tzz_.begin(), tzz_.end(), zz_);
            rr_ = new double[trr_.size()]; std::copy(trr_.begin(), trr_.end(), rr_);
            sizes_ = new long[tsizes_.size()]; std::copy(tsizes_.begin(), tsizes_.end(), sizes_);
            inds_  = new long[tinds_.size()]; std::copy(tinds_.begin(), tinds_.end(), inds_);
            std::sort(tels_.begin(), tels_.end()); tels_.erase( std::unique(tels_.begin(), tels_.end()), tels_.end() );
            ne_ = tels_.size(); els_ = new long[tels_.size()]; std::copy(tels_.begin(), tels_.end(), els_);
        }
};

struct FiniteTestSystems : public TestSystems
{
    // bit flags indicating whether a system is included in a dataset (instance of this class)
    static const unsigned long EMPTY = (1 <<  0);  // empty system
    static const unsigned long H     = (1 <<  1);  // free H atom
    static const unsigned long H3    = (1 <<  2);  // chain of 3 H
    static const unsigned long O2    = (1 <<  3);  // oxygen
    static const unsigned long HCl   = (1 <<  4);  // HCl diatomic
    static const unsigned long H2O   = (1 <<  5);  // H2O, water
    static const unsigned long C6H6  = (1 <<  6);  // benzene
    static const unsigned long HCN   = (1 <<  7);  // hydrogen cyanide
    static const unsigned long HCNO  = (1 <<  8);  // fulminic acid
    static const unsigned long CHNOS = (1 <<  9);  // hypothiocyanite
    static const unsigned long H4    = (1 << 10);  // linear chain of 4 H
    static const unsigned long SF6   = (1 << 11);  // sulfur hexafluoride (octahedral)
    static const unsigned long ALL  = EMPTY | H | H3 | O2 | HCl | H2O | C6H6 | HCN | HCNO | CHNOS | H4 | SF6;

    FiniteTestSystems(unsigned long const sysnames = ALL)
    {
        // empty system
        if( (unsigned long)sysnames & EMPTY ) add_system
        (
            { },
            { }
        );

        // free H atom
        if( (unsigned long)sysnames & H ) add_system
        (
            { 1 },
            { 0., 0., 0. }
        );

        // chain of 3 hydrogens
        if( (unsigned long)sysnames & H3 ) add_system
        (
            { 1, 1, 1 },
            { 0., 0., 0.,  0.72, 0., 0.,  1.44, 0., 0. }
        );

        // oxygen
        if( (unsigned long)sysnames & O2 ) add_system
        (
            { 8, 8 },
            { 0., 0., 0.,  1.48, 0., 0. }
        );

        // HCl
        if( (unsigned long)sysnames & HCl ) add_system
        (
            { 1, 17 },
            { 0., 0., 0.,  0., 1.27, 0. }
        );

        // water
        if( (unsigned long)sysnames & H2O ) add_system
        (
            { 8, 1, 1 },
            {
                 0.16597615219814216, -0.3140092250490285, -0.07027664259186725,
                -0.03622260506424052,  0.5665229602793138,  0.24958159518024825,
                -0.63725621934564140, -0.5997033905165634, -0.50814324621155720
            }
        );

        // benzene
        if( (unsigned long)sysnames & C6H6 ) add_system
        (
            { 6, 6, 6, 6, 6, 6, 1, 1, 1, 1, 1, 1 },
            {
                -1.19070, -0.015997, -0.75474,
                -0.13956, +0.846770, -1.10680,
                -1.05110, -0.862770, +0.35210,
                +1.05110, +0.862770, -0.35210
                +0.13956, -0.846770, +1.10680,
                +1.19070, +0.015997, +0.75474,
                -2.09330, -0.026239, -1.32920,
                -0.24311, +1.494900, -1.95190,
                -1.84850, -1.522100, +0.62495
                +1.84850, +1.522100, -0.62495
                +0.24311, -1.494900, +1.95190,
                +2.09330, +0.026239, +1.32920
            }
        );

        // hydrogen cyanide
        if( (unsigned long)sysnames & HCN ) add_system
        (
            { 1, 6, 7 },
            {
                2.5369, -0.095, 0,
                3.4030,  0.405, 0,
                2.0000, -0.405, 0
            }
        );

        // fulminic acid
        if( (unsigned long)sysnames & HCNO ) add_system
        (
            { 1, 6, 7, 8 },
            {
                 2.2791,  0.0002, 0.0000,
                 1.2141,  0.0000, 0.0000,
                 0.1008, -0.0001, 0.0001,
                -1.3149,  0.0000, 0.0000
            }
        );

        // hypothiocyanite
        if( (unsigned long)sysnames & CHNOS ) add_system
        (
            { 6, 1, 7, 8, 16 },
            {
                 -0.7233, -0.1470, 0.0000,
                  1.8716,  0.9319, 0.9237,
                 -1.7970,  0.2922, 0.0000,
                  1.6801,  0.6441, 0.0000,
                  0.8403, -0.7894, 0.0000
            }
        );

        // chain of 4 hydrogens
        if( (unsigned long)sysnames & H4 ) add_system
        (
            { 1, 1, 1, 1 },
            { 0., 0., 0.,  0.72, 0., 0.,  1.44, 0., 0.,  2.16, 0., 0. }
        );

        // sulfur hexafluoride
        if( (unsigned long)sysnames & SF6 ) add_system
        (
            { 16, 9, 9, 9, 9, 9, 9 },
            { 0., 0., 0.,   1.561, 0., 0.,   -1.561, 0., 0.,   0., 1.561, 0.,   0., -1.561, 0.,   0., 0., 1.561,   0., 0., -1.561 }
        );

        finalize();
    };
};

struct PeriodicTestSystems : TestSystems
{
    static const unsigned long EMPTY = (1 << 0);  // empty system
    static const unsigned long NaCl  = (1 << 1);  // salt
    static const unsigned long Al    = (1 << 2);  // aluminum
    static const unsigned long SF6   = (1 << 3);  // sulfur hexafluoride
    static const unsigned long M1    = (1 << 4);  // model system 1
    static const unsigned long M2    = (1 << 5);  // model system 2
    static const unsigned long M3    = (1 << 6);  // model system 3
    static const unsigned long M4    = (1 << 7);  // model system 4
    static const unsigned long ALL   = EMPTY | NaCl | Al | SF6 | M1 | M2 | M3 | M4;

    PeriodicTestSystems(const unsigned long sysnames = ALL)
    {
        // empty system
        if( (unsigned long)sysnames & EMPTY ) add_system
        (
            { 1, 0, 0,  0, 1, 0,  0, 0, 1},
            { },
            { }
        );

        // NaCl
        // fcc (octahedral interstitial), Fm\bar{3}m, 225, a = 5.6402 A, Z = 4
        // primitive cell: a = b = c = 4.025 A (ICSD: 3.988 A), alpha = beta = gamma = 60 degree
        if( (unsigned long)sysnames & NaCl ) add_system
        (
            { 3.988, 0, 0,  1.994, 3.45371, 0,  1.994, 1.15124, 3.25619 },
            { 11, 17 },
            { 0., 0., 0.,  3.988, 2.302475, 1.628095 } // fractional: { 0., 0., 0.,  0.5, 0.5, 0.5 }
        );

        // Al
        // fcc, Fm\bar{3}m, 225, a = 4.0495 A (ICSD)
        // primitive cell: a = b = c = 2.8634 A, alpha = beta = gamma = 60 degree
        if( (unsigned long)sysnames & Al ) add_system
        (
            { 0, 2.02475, 2.02475,  2.02475, 2.02475, 0,  2.02475, 0, 2.02475 },
            { 13 },
            { 0., 0., 0. }  // fractional: { 0, 0, 0 }
        );

        // SF6
        // hexoctahedral; Im\bar{3}m, 229, a = 5.817 A (ICSD)
        if( (unsigned long)sysnames & SF6 ) add_system
        (
            { 5.187, 0, 0,  -1.729, 4.891, 0,  -1.729, -2.445, 4.235 },
            { 16, 9, 9, 9, 9, 9, 9 },
            // fractional: { 0,0,0,  0.2666,1,0.2666,  0.7334,0,0.7334,  0,0.7334,0.7334,  0,0.2666,0.2666,  0.2666,0.2666,0,  0.7334,0.7334,0  }
            {
                 0.    ,  0.    , 0.    ,
                -0.8070,  4.2386, 1.1293,
                 2.5361, -1.7933, 3.1061,
                -2.5361,  1.7933, 3.1061,
                -0.9220,  0.6510, 1.1293,
                 0.9220,  1.3040, 0.    ,
                 2.5361,  3.5866, 0.
            }
        );

        // M1 model system 1
        // unit cube with atom in the middle
        if( (unsigned long)sysnames & M1 ) add_system
        (
            { 1, 0, 0,   0, 1, 0,   0, 0, 1 },
            { 9 },
            { 0.5, 0.5, 0.5 }  // same as fractional
        );

        // M2 model system 2
        // unit cube with two atoms, one at the origin, one in the center
        if( (unsigned long)sysnames & M2 ) add_system
        (
            { 1, 0, 0,   0, 1, 0,   0, 0, 1 },
            { 8, 9 },
            { 0, 0, 0,   0.5, 0.5, 0.5 }
        );

        // M3 model system 3
        // unit cube with three atoms in the middle
        if( (unsigned long)sysnames & M3 ) add_system
        (
            { 1, 0, 0,  0, 1, 0,  0, 0, 1 },
            { 7, 8, 9 },
            { 0.55, 0.50, 0.50,   0.50, 0.55, 0.50,   0.50, 0.50, 0.55 }
        );

        // M5 model system 5
        // unit cube with three atoms in corners
        if( (unsigned long)sysnames & M4 ) add_system
        (
            { 1, 0, 0,  0, 1, 0,  0, 0, 1 },
            { 7, 8, 9 },
            { 0, 0, 0.9,   0, 0.9, 0.9,   0, 0.9, 0 }
        );

        finalize();
    };
};


//  ///////////////////////////
//  //  Auxiliary functions  //
//  ///////////////////////////

// cmp_arr_abs below works as "double res[] = { ... }; CHECK( cmp_arr_abs(mbtr, res, n, 1e-10) );", but yields only false without additional information.
// Use rounding instead to compare arrays of floating point values (where absolute difference comparison is desired)
// bool cmp_arr_abs(double const * const lhs, double const * const rhs, const long n, const double eps = 1e-10)
// {
//     return = std::equal(lhs, lhs+n, rhs, [eps](double x, double y) -> bool { return std::abs(x-y) < eps; } );
// }

// rounds one number to given absolute accuracy, use as round(x, 1e-10)
double round(double const arg, double const eps)
{
    return std::round(arg / eps) * eps;
}

// rounds all entries of an array
std::vector<double> round(double const * const array, const long n, double const eps)
{
    std::vector<double> result(n);
    std::transform(array, array + n, result.begin(), [eps](double x) { return round(x, eps); } );
    return result;
}

// rounds all entries of a vector
std::vector<double> round(std::vector<double> const& arg, double const eps)
{
    auto result = arg;
    std::transform(arg.begin(), arg.end(), result.begin(), [eps](double x) { return round(x, eps); } );
    return result;
}

// helper function for shorter syntax in calling many_body_tensor_representation
template<int K, typename G, typename W, typename D, typename C, typename E, typename A>
many_body_tensor_representation<K, G, W, D, C, E, A> mbtr_call
(
    double * const dest, TestSystems const & s, double const xmin, double const deltax, long const xdim,
    G g = G(), W w = W(), D d = D(), C c = C(), E e = E(), A a = A()
)
{
    auto mbtr = many_body_tensor_representation<K, G, W, D, C, E, A>(dest, s.ns_, s.sizes_, s.inds_, s.bb_, s.zz_, s.rr_, s.ne_, s.els_, xmin, deltax, xdim, g, w, d, c, e, a);

    if( dest == nullptr ) mbtr.mbtr(new double[mbtr.mbtr_size()]);
    mbtr.compute();
    if( dest == nullptr ) delete[] mbtr.mbtr();

    return mbtr;
}


//  //////////////////////////
//  //  Geometry functions  //
//  //////////////////////////

template<int K, typename G> void geomf_stateful_setup(TestSystems const & s, G & geomf)
{
    auto mbtr = mbtr_call<K>(
        nullptr, s, 0, 0.1, 20, geomf, mbtr_weightf_unity(), mbtr_normal_distribution(1e-10),
        mbtr_identity_correlation(), mbtr_eindexf_dense_noreversals(), mbtr_aindexf_finite_noreversals<long>()
    );
    geomf.init(mbtr); geomf.reset(0);
}

TEST_CASE("many_body_tensor_representation-geometry_functions", "[representations],[many-body_tensor]")
{
    using std::vector;
    const auto H = FiniteTestSystems::H; const auto H3 = FiniteTestSystems::H3; const auto O2 = FiniteTestSystems::O2; const auto HCl = FiniteTestSystems::HCl; const auto H2O = FiniteTestSystems::H2O; const auto C6H6 = FiniteTestSystems::C6H6; const auto HCN = FiniteTestSystems::HCN; const auto HCNO = FiniteTestSystems::HCNO; const auto CHNOS = FiniteTestSystems::CHNOS; const auto EMPTY = FiniteTestSystems::EMPTY; const auto H4 = FiniteTestSystems::H4; const auto SF6 = FiniteTestSystems::SF6;

    SECTION( "mbtr_geomf_unity" )
    {
        static_assert( std::is_base_of<mbtr_stateless, mbtr_geomf_unity>::value, "implementation changed, please update test" );
        FiniteTestSystems s(C6H6);
        auto gf = mbtr_geomf_unity();
        CHECK( gf(s.zz_[0], s.rr_+0*3) == 1. );  // k = 1
        CHECK( gf(s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3) == 1. );  // k = 2
        CHECK( gf(s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3) == 1. );  // k = 3
        CHECK( gf(s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3, s.zz_[3], s.rr_+3*3) == 1. );  // k = 4
    }

    SECTION( "mbtr_geomf1_count" )
    {
        static_assert( ! std::is_base_of<mbtr_stateless, mbtr_geomf1_count<long>>::value, "implementation changed, please update test" );
        FiniteTestSystems s(H2O);
        auto gf = mbtr_geomf1_count<long>();
        geomf_stateful_setup<1, mbtr_geomf1_count<long>>(s, gf);
        CHECK( gf(s.zz_[0], s.rr_+0*3) == 1 );  // O
        CHECK( gf(s.zz_[1], s.rr_+1*3) == 2 );  // H2
    }

    SECTION( "mbtr_geomf2_one_over_distance" )
    {
        static_assert( std::is_base_of<mbtr_stateless, mbtr_geomf2_one_over_distance>::value, "implementation changed, please update test" );
        FiniteTestSystems s(H2O);
        auto gf = mbtr_geomf2_one_over_distance();
        CHECK( gf(s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3) == 1 / 0.958399999534767 );
    }

    SECTION( "mbtr_geomf2_one_over_dot" )
    {
        static_assert( std::is_base_of<mbtr_stateless, mbtr_geomf2_one_over_dot>::value, "implementation changed, please update test" );
        FiniteTestSystems s(H2O);
        auto gf = mbtr_geomf2_one_over_dot();
        CHECK( gf(s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3) == 1 / 0.9185305591082413 );
    }

    SECTION( "mbtr_geomf3_angle" )
    {
        static_assert( std::is_base_of<mbtr_stateless, mbtr_geomf3_angle>::value, "implementation changed, please update test" );
        FiniteTestSystems s(H2O);
        auto gf = mbtr_geomf3_angle();
        CHECK( gf(s.zz_[1], s.rr_+1*3, s.zz_[0], s.rr_+0*3, s.zz_[2], s.rr_+2*3) == 1.822996403791758 );
    }

    SECTION( "mbtr_geomf3_cos_angle" )
    {
        static_assert( std::is_base_of<mbtr_stateless, mbtr_geomf3_cos_angle>::value, "implementation changed, please update test" );
        FiniteTestSystems s(H2O);
        auto gf = mbtr_geomf3_cos_angle();
        CHECK( round(gf(s.zz_[1], s.rr_+1*3, s.zz_[0], s.rr_+0*3, s.zz_[2], s.rr_+2*3), 1e-10) == round(-0.249535040706576, 1e-10) );
    }

    SECTION( "mbtr_geomf3_dot_over_dotdot" )
    {
        static_assert( std::is_base_of<mbtr_stateless, mbtr_geomf3_dot_over_dotdot>::value, "implementation changed, please update test" );
        FiniteTestSystems s(H2O);
        auto gf = mbtr_geomf3_dot_over_dotdot();
        CHECK( round(gf(s.zz_[1], s.rr_+1*3, s.zz_[0], s.rr_+0*3, s.zz_[2], s.rr_+2*3), 1e-10) == round(-0.2716676521156063, 1e-10) );
    }

    SECTION( "mbtr_geomf4_dihedral_angle" )
    {
        // ill-defined angles
        {
            static_assert( std::is_base_of<mbtr_stateless, mbtr_geomf4_dihedral_angle>::value, "implementation changed, please update test" );
            FiniteTestSystems s(H4);
            const double res = mbtr_geomf4_dihedral_angle()(s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3, s.zz_[3], s.rr_+3*3);
            CHECK( res == mbtr_undefined );
        }

        // octahedral (right angles)
        {
            static_assert( std::is_base_of<mbtr_stateless, mbtr_geomf4_dihedral_angle>::value, "implementation changed, please update test" );
            FiniteTestSystems s(SF6);
            const double res = mbtr_geomf4_dihedral_angle()(s.zz_[5], s.rr_+5*3, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[4], s.rr_+4*3);
            CHECK( round(res, 1e-10)  == round(1.5707963267948966, 1e-10) );
        }
    }

    SECTION( "mbtr_geomf4_cos_dihedral_angle" )
    {
        // ill-defined angles
        {
            static_assert( std::is_base_of<mbtr_stateless, mbtr_geomf4_cos_dihedral_angle>::value, "implementation changed, please update test" );
            FiniteTestSystems s(H4);
            const double res = mbtr_geomf4_cos_dihedral_angle()(s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3, s.zz_[3], s.rr_+3*3);
            CHECK( res == mbtr_undefined );
        }

        // octahedral (right angles)
        {
            static_assert( std::is_base_of<mbtr_stateless, mbtr_geomf4_cos_dihedral_angle>::value, "implementation changed, please update test" );
            FiniteTestSystems s(SF6);
            const double res = mbtr_geomf4_cos_dihedral_angle()(s.zz_[5], s.rr_+5*3, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[4], s.rr_+4*3);
            CHECK( round(res, 1e-10)  == round(0., 1e-10) );
        }
    }
}


//  ///////////////////////////
//  //  Weighting functions  //
//  ///////////////////////////

template<int K, typename W> void weightf_stateful_setup(TestSystems const & s, W & weightf)
{
    auto mbtr = mbtr_call<K>(
        nullptr, s, 0, 0.1, 20, mbtr_geomf_unity(), weightf, mbtr_normal_distribution(1e-10),
        mbtr_identity_correlation(), mbtr_eindexf_dense_noreversals(), mbtr_aindexf_finite_noreversals<long>()
    );
    weightf.init(mbtr); weightf.reset(0);
}

TEST_CASE("many_body_tensor_representation-weighting_functions", "[representations],[many-body_tensor]")
{
    using std::vector;
    const auto H = FiniteTestSystems::H; const auto H3 = FiniteTestSystems::H3; const auto O2 = FiniteTestSystems::O2; const auto HCl = FiniteTestSystems::HCl; const auto H2O = FiniteTestSystems::H2O; const auto C6H6 = FiniteTestSystems::C6H6; const auto HCN = FiniteTestSystems::HCN; const auto HCNO = FiniteTestSystems::HCNO; const auto CHNOS = FiniteTestSystems::CHNOS; const auto EMPTY = FiniteTestSystems::EMPTY; const auto H4 = FiniteTestSystems::H4; const auto SF6 = FiniteTestSystems::SF6;

    SECTION( "mbtr_weightf_unity" )
    {
        static_assert( std::is_base_of<mbtr_stateless, mbtr_weightf_unity>::value, "implementation changed, please update test" );
        FiniteTestSystems s(C6H6);
        auto wf = mbtr_weightf_unity();
        CHECK( wf(3.14, s.zz_[0], s.rr_+0*3) == 1. );  // k = 1
        CHECK( wf(3.14, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3) == 1. );  // k = 2
        CHECK( wf(3.14, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3) == 1. );  // k = 3
        CHECK( wf(3.14, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3, s.zz_[3], s.rr_+3*3) == 1. );  // k = 4
    }

    SECTION( "mbtr_weightf_identity" )
    {
        static_assert( std::is_base_of<mbtr_stateless, mbtr_weightf_identity>::value, "implementation changed, please update test" );
        const double x = 3.141592653589793;
        FiniteTestSystems s(C6H6);
        auto wf = mbtr_weightf_identity();  // stateless
        CHECK( wf(x, s.zz_[0], s.rr_+0*3) == x );  // k = 1
        CHECK( wf(x, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3) == x );  // k = 2
        CHECK( wf(x, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3) == x );  // k = 3
        CHECK( wf(x, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3, s.zz_[3], s.rr_+3*3) == x );  // k = 4
    }

    SECTION( "mbtr_weightf_identity_squared" )
    {
        static_assert( std::is_base_of<mbtr_stateless, mbtr_weightf_identity_squared>::value, "implementation changed, please update test" );
        const double x = 3.141592653589793;
        FiniteTestSystems s(C6H6);
        auto wf = mbtr_weightf_identity_squared();  // stateless
        CHECK( wf(x, s.zz_[0], s.rr_+0*3) == x*x );  // k = 1
        CHECK( wf(x, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3) == x*x );  // k = 2
        CHECK( wf(x, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3) == x*x );  // k = 3
        CHECK( wf(x, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3, s.zz_[3], s.rr_+3*3) == x*x );  // k = 4
    }

    SECTION( "mbtr_weightf_identity_root" )
    {
        static_assert( std::is_base_of<mbtr_stateless, mbtr_weightf_identity_root>::value, "implementation changed, please update test" );
        const double x = 3.141592653589793;
        FiniteTestSystems s(C6H6);
        auto wf = mbtr_weightf_identity_root();  // stateless
        CHECK( wf(x, s.zz_[0], s.rr_+0*3) == std::sqrt(x) );  // k = 1
        CHECK( wf(x, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3) == std::sqrt(x) );  // k = 2
        CHECK( wf(x, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3) == std::sqrt(x) );  // k = 3
        CHECK( wf(x, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3, s.zz_[3], s.rr_+3*3) == std::sqrt(x) );  // k = 4
    }

    SECTION( "mbtr_weightf_one_over_identity" )
    {
        static_assert( std::is_base_of<mbtr_stateless, mbtr_weightf_one_over_identity>::value, "implementation changed, please update test" );
        const double x = 3.141592653589793;
        FiniteTestSystems s(C6H6);
        auto wf = mbtr_weightf_one_over_identity();  // stateless
        CHECK( wf(x, s.zz_[0], s.rr_+0*3) == 1 / x );  // k = 1
        CHECK( wf(x, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3) == 1 / x );  // k = 2
        CHECK( wf(x, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3) == 1 / x );  // k = 3
        CHECK( wf(x, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3, s.zz_[3], s.rr_+3*3) == 1 / x );  // k = 4
    }

    SECTION( "mbtr_weightf_delta_one_over_identity" )
    {
        static_assert( ! std::is_base_of<mbtr_stateless, mbtr_weightf_delta_one_over_identity>::value, "implementation changed, please update test" );

        const double delta = 1.;
        FiniteTestSystems s(C6H6);

        // k = 1
        {
            auto wf1 = mbtr_weightf_delta_one_over_identity(delta);
            weightf_stateful_setup<1, mbtr_weightf_delta_one_over_identity>(s, wf1);
            CHECK( wf1( delta - 0    , s.zz_[0], s.rr_+0*3) == 1 );
            CHECK( wf1( delta - 1e-10, s.zz_[0], s.rr_+0*3) == 0 );
            CHECK( wf1( delta + 1e+10, s.zz_[0], s.rr_+0*3) == 1 );
        }

        // k = 2
        {
            auto wf2 = mbtr_weightf_delta_one_over_identity(delta);
            weightf_stateful_setup<2, mbtr_weightf_delta_one_over_identity>(s, wf2);
            CHECK( wf2( delta - 0    , s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3) == 1 );
            CHECK( wf2( delta - 1e-10, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3) == 0 );
            CHECK( wf2( delta + 1e-10, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3) == 1 );
        }

        // k = 3
        {
            auto wf3 = mbtr_weightf_delta_one_over_identity(delta);
            weightf_stateful_setup<3, mbtr_weightf_delta_one_over_identity>(s, wf3);
            CHECK( wf3( delta - 0    , s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3) == 1 );
            CHECK( wf3( delta - 1e-10, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3) == 0 );
            CHECK( wf3( delta + 1e-10, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3) == 1 );

        }

        // k = 4
        {
            auto wf4 = mbtr_weightf_delta_one_over_identity(delta);
            weightf_stateful_setup<4, mbtr_weightf_delta_one_over_identity>(s, wf4);
            CHECK( wf4( delta - 0    , s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3, s.zz_[3], s.rr_+3*3) == 1 );
            CHECK( wf4( delta - 1e-10, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3, s.zz_[3], s.rr_+3*3) == 0 );
            CHECK( wf4( delta + 1e-10, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3, s.zz_[3], s.rr_+3*3) == 1 );
        }
    }

    SECTION( "mbtr_weightf_exp_neg_one_over_identity" )
    {
        static_assert( ! std::is_base_of<mbtr_stateless, mbtr_weightf_exp_neg_one_over_identity>::value, "implementation changed, please update test" );

        const double ls = 2.;
        FiniteTestSystems s(C6H6);

        // k = 1
        {
            auto wf1 = mbtr_weightf_exp_neg_one_over_identity(ls);
            weightf_stateful_setup<1, mbtr_weightf_exp_neg_one_over_identity>(s, wf1);
            CHECK( round(wf1( 1 / 0.5, s.zz_[0], s.rr_+0*3), 1e-10) == round(0.778800783071404900, 1e-10) );
            CHECK( round(wf1( 1 / 1.0, s.zz_[0], s.rr_+0*3), 1e-10) == round(0.606530659712633400, 1e-10) );
            CHECK( round(wf1( 1 / 10., s.zz_[0], s.rr_+0*3), 1e-10) == round(0.006737946999085467, 1e-10) );
        }

        // k = 2
        {
            auto wf2 = mbtr_weightf_exp_neg_one_over_identity(ls);
            weightf_stateful_setup<2, mbtr_weightf_exp_neg_one_over_identity>(s, wf2);
            CHECK( round(wf2( 1 / 0.5, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3), 1e-10) == round(0.778800783071404900, 1e-10) );
            CHECK( round(wf2( 1 / 1.0, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3), 1e-10) == round(0.606530659712633400, 1e-10) );
            CHECK( round(wf2( 1 / 10., s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3), 1e-10) == round(0.006737946999085467, 1e-10) );
        }

        // k = 3
        {
            auto wf3 = mbtr_weightf_exp_neg_one_over_identity(ls);
            weightf_stateful_setup<3, mbtr_weightf_exp_neg_one_over_identity>(s, wf3);
            CHECK( round(wf3( 1 / 0.5, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3), 1e-10) == round(0.778800783071404900, 1e-10) );
            CHECK( round(wf3( 1 / 1.0, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3), 1e-10) == round(0.606530659712633400, 1e-10) );
            CHECK( round(wf3( 1 / 10., s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3), 1e-10) == round(0.006737946999085467, 1e-10) );
        }

        // k = 4
        {
            auto wf4 = mbtr_weightf_exp_neg_one_over_identity(ls);
            weightf_stateful_setup<4, mbtr_weightf_exp_neg_one_over_identity>(s, wf4);
            CHECK( round(wf4( 1 / 0.5, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3, s.zz_[3], s.rr_+3*3), 1e-10) == round(0.778800783071404900, 1e-10) );
            CHECK( round(wf4( 1 / 1.0, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3, s.zz_[3], s.rr_+3*3), 1e-10) == round(0.606530659712633400, 1e-10) );
            CHECK( round(wf4( 1 / 10., s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3, s.zz_[3], s.rr_+3*3), 1e-10) == round(0.006737946999085467, 1e-10) );
        }
    }

    SECTION( "mbtr_weightf_exp_neg_one_over_identity_squared" )
    {
        static_assert( ! std::is_base_of<mbtr_stateless, mbtr_weightf_exp_neg_one_over_identity_squared>::value, "implementation changed, please update test" );

        const double ls = 2.;
        FiniteTestSystems s(C6H6);

        // k = 1
        {
            auto wf1 = mbtr_weightf_exp_neg_one_over_identity_squared(ls);
            weightf_stateful_setup<1, mbtr_weightf_exp_neg_one_over_identity_squared>(s, wf1);
            CHECK( round(wf1( 1 / 0.5, s.zz_[0], s.rr_+0*3), 1e-10) == round(0.8824969025845955, 1e-10) );
            CHECK( round(wf1( 1 / 1.0, s.zz_[0], s.rr_+0*3), 1e-10) == round(0.6065306597126334, 1e-10) );
            CHECK( round(wf1( 1 / 2.0, s.zz_[0], s.rr_+0*3), 1e-10) == round(0.1353352832366127, 1e-10) );
        }

        // k = 2
        {
            auto wf2 = mbtr_weightf_exp_neg_one_over_identity_squared(ls);
            weightf_stateful_setup<2, mbtr_weightf_exp_neg_one_over_identity_squared>(s, wf2);
            CHECK( round(wf2( 1 / 0.5, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3), 1e-10) == round(0.8824969025845955, 1e-10) );
            CHECK( round(wf2( 1 / 1.0, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3), 1e-10) == round(0.6065306597126334, 1e-10) );
            CHECK( round(wf2( 1 / 2.0, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3), 1e-10) == round(0.1353352832366127, 1e-10) );
        }

        // k = 3
        {
            auto wf3 = mbtr_weightf_exp_neg_one_over_identity_squared(ls);
            weightf_stateful_setup<3, mbtr_weightf_exp_neg_one_over_identity_squared>(s, wf3);
            CHECK( round(wf3( 1 / 0.5, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3), 1e-10) == round(0.8824969025845955, 1e-10) );
            CHECK( round(wf3( 1 / 1.0, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3), 1e-10) == round(0.6065306597126334, 1e-10) );
            CHECK( round(wf3( 1 / 2.0, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3), 1e-10) == round(0.1353352832366127, 1e-10) );
        }

        // k = 4
        {
            auto wf4 = mbtr_weightf_exp_neg_one_over_identity_squared(ls);
            weightf_stateful_setup<4, mbtr_weightf_exp_neg_one_over_identity_squared>(s, wf4);
            CHECK( round(wf4( 1 / 0.5, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3, s.zz_[3], s.rr_+3*3), 1e-10) == round(0.8824969025845955, 1e-10) );
            CHECK( round(wf4( 1 / 1.0, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3, s.zz_[3], s.rr_+3*3), 1e-10) == round(0.606530659712633400, 1e-10) );
            CHECK( round(wf4( 1 / 2.0, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3, s.zz_[3], s.rr_+3*3), 1e-10) == round(0.1353352832366127, 1e-10) );
        }
    }

    SECTION( "mbtr_weightf1_one_over_count" )
    {
        static_assert( ! std::is_base_of<mbtr_stateless, mbtr_weightf1_one_over_count<long>>::value, "implementation changed, please update test" );
        FiniteTestSystems s(H2O);
        auto wf1 = mbtr_weightf1_one_over_count<long>();
        weightf_stateful_setup<1, mbtr_weightf1_one_over_count<long>>(s, wf1);
        CHECK( wf1( 1, s.zz_[0], s.rr_+0*3 ) == 1. );
        CHECK( wf1( 1, s.zz_[1], s.rr_+1*3 ) == 0.5 );
    }

    SECTION( "mbtr_weightf3_one_over_normnorm" )
    {
        static_assert( std::is_base_of<mbtr_stateless, mbtr_weightf3_one_over_normnorm>::value, "implementation changed, please update test" );
        FiniteTestSystems s(H2O);
        auto wf3 = mbtr_weightf3_one_over_normnorm();  // stateless
        CHECK( wf3( 1, s.zz_[1], s.rr_+1*3, s.zz_[0], s.rr_+0*3, s.zz_[2], s.rr_+2*3 ) == 1 / 0.9185305602758629 );
    }

    SECTION( "mbtr_weightf3_one_over_dotdot" )
    {
        static_assert( std::is_base_of<mbtr_stateless, mbtr_weightf3_one_over_dotdot>::value, "implementation changed, please update test" );
        FiniteTestSystems s(H2O);
        auto wf3 = mbtr_weightf3_one_over_dotdot();  // stateless
        CHECK( wf3( 1, s.zz_[1], s.rr_+1*3, s.zz_[0], s.rr_+0*3, s.zz_[2], s.rr_+2*3 ) == 1 / 0.8436983901606906 );
    }

    SECTION( "mbtr_weightf3_one_over_normnormnorm" )
    {
        static_assert( std::is_base_of<mbtr_stateless, mbtr_weightf3_one_over_normnormnorm>::value, "implementation changed, please update test" );
        FiniteTestSystems s(H2O);
        auto wf3 = mbtr_weightf3_one_over_normnormnorm();  // stateless
        CHECK( wf3( 1, s.zz_[1], s.rr_+1*3, s.zz_[0], s.rr_+0*3, s.zz_[2], s.rr_+2*3 ) == 1 / 1.3916487470893475 );
    }

    SECTION( "mbtr_weightf3_one_over_dotdotdot" )
    {
        static_assert( std::is_base_of<mbtr_stateless, mbtr_weightf3_one_over_dotdotdot>::value, "implementation changed, please update test" );
        FiniteTestSystems s(H2O);
        auto wf3 = mbtr_weightf3_one_over_dotdotdot();  // stateless
        CHECK( wf3( 1, s.zz_[1], s.rr_+1*3, s.zz_[0], s.rr_+0*3, s.zz_[2], s.rr_+2*3 ) == 1 / 1.9366862352753507 );
    }

    SECTION( "mbtr_weightf3_exp_neg_one_over_normnormnorm" )
    {
        static_assert( ! std::is_base_of<mbtr_stateless, mbtr_weightf3_exp_neg_one_over_normnormnorm>::value, "implementation changed, please update test" );

        const double ls = 2.;
        FiniteTestSystems s(C6H6);

        // k = 3
        {
            auto wf3 = mbtr_weightf3_exp_neg_one_over_normnormnorm(ls);
            weightf_stateful_setup<3, mbtr_weightf3_exp_neg_one_over_normnormnorm>(s, wf3);
            CHECK( round(wf3( 1, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3), 1e-10) == round(0.0920232131934628, 1e-10) );
        }
    }

    SECTION( "mbtr_weightf3_exp_neg_one_over_sum_normnormnorm" )
    {
        static_assert( ! std::is_base_of<mbtr_stateless, mbtr_weightf3_exp_neg_one_over_sum_normnormnorm>::value, "implementation changed, please update test" );

        const double ls = 2.;
        FiniteTestSystems s(C6H6);

        // k = 3
        {
            auto wf3 = mbtr_weightf3_exp_neg_one_over_sum_normnormnorm(ls);
            weightf_stateful_setup<3, mbtr_weightf3_exp_neg_one_over_sum_normnormnorm>(s, wf3);
            CHECK( round(wf3( 1, s.zz_[0], s.rr_+0*3, s.zz_[1], s.rr_+1*3, s.zz_[2], s.rr_+2*3), 1e-10) == round(0.07314840105601644, 1e-10) );
        }
    }
}


//  //////////////////////////////
//  //  Distribution functions  //
//  //////////////////////////////

TEST_CASE("many_body_tensor_representation-distribution_functions", "[representations],[many-body_tensor]")
{
    using std::vector;
    const auto H = FiniteTestSystems::H; const auto H3 = FiniteTestSystems::H3; const auto O2 = FiniteTestSystems::O2; const auto HCl = FiniteTestSystems::HCl; const auto H2O = FiniteTestSystems::H2O; const auto C6H6 = FiniteTestSystems::C6H6; const auto HCN = FiniteTestSystems::HCN; const auto HCNO = FiniteTestSystems::HCNO; const auto CHNOS = FiniteTestSystems::CHNOS; const auto EMPTY = FiniteTestSystems::EMPTY;

    SECTION( "mbtr_normal_distribution" )
    {
        // range_from, range_to
        {
            const double sigma = 1;
            mbtr_normal_distribution distrf(sigma);
            distrf.reset(99);
            CHECK( distrf.range_from() <= distrf.range_to() );
        }

        // cdf
        // -> Mathematica development notebook
        {
            const double sigma = 1;
            mbtr_normal_distribution distrf(sigma);
            distrf.reset(0);
            vector<double> cdf; for(long i = 0; i < 25; ++i) cdf.push_back( distrf.cdf(-6. + i*0.5) );
            vector<double> res {
                9.865876450377018e-10 , 1.898956246588779e-8 , 2.866515718791944e-7   , 3.3976731247300603e-6, 0.000031671241833119985,
                0.00023262907903552548, 0.0013498980316300959, 0.006209665325776138   , 0.02275013194817922  , 0.06680720126885809    ,
                0.15865525393145707   , 0.3085375387259869   , 0.5                    , 0.6914624612740131   , 0.8413447460685429     ,
                0.9331927987311419    , 0.9772498680518208   , 0.9937903346742238     , 0.9986501019683699   , 0.9997673709209645     ,
                0.9999683287581669    , 0.9999966023268753   , 0.9999997133484281     , 0.9999999810104375   , 0.9999999990134123
            };
            CHECK( round(cdf, 1e-10) == round(res, 1e-10) );
        }
    }
}


//  /////////////////////////////
//  //  Correlation functions  //
//  /////////////////////////////

// TODO: currently no tests


//  //////////////////////////////////
//  //  Element indexing functions  //
//  //////////////////////////////////

// Tests whether generated tensor row indices match expected ones.
// res contains atomic number tuples generating row indices 0, 1, 2, 3, ..., nec-1.
// Possible extension is to pass the expected sequence of row indices.
template<int K, typename E> void eindexf_test_indices(TestSystems const & s, std::vector<long> const & res)
{
    using std::vector;

    const long n = res.size() / K;  // number of row indices

    E eindexf;
    auto mbtr = mbtr_call<K>(
        nullptr, s, 0, 0.1, 20, mbtr_geomf_unity(), mbtr_weightf_unity(), mbtr_normal_distribution(1e-10),
        mbtr_identity_correlation(), eindexf, mbtr_aindexf_finite_noreversals<long>()
    );
    eindexf.init(mbtr); eindexf.reset(0);

    CHECK( eindexf.size(mbtr) == s.ns_ * 20 * n );

    vector<long> inds(n);
    for(long i = 0; i < n; ++i)
        switch(K)
        {
            case 1: inds.at(i) = eindexf( res.at(i*K+0) ); break;
            case 2: inds.at(i) = eindexf( res.at(i*K+0), res.at(i*K+1) ); break;
            case 3: inds.at(i) = eindexf( res.at(i*K+0), res.at(i*K+1), res.at(i*K+2) ); break;
            case 4: inds.at(i) = eindexf( res.at(i*K+0), res.at(i*K+1), res.at(i*K+2), res.at(i*K+3) ); break;
        };

    vector<long> rhs(n); std::iota(rhs.begin(), rhs.end(), 0);  // range 0,1,...,n-1
    CHECK( inds == rhs );
}

TEST_CASE("many_body_tensor_representation-element_indexing_functions", "[representations],[many-body_tensor]")
{
    using std::vector;
    const auto H = FiniteTestSystems::H; const auto H3 = FiniteTestSystems::H3; const auto O2 = FiniteTestSystems::O2; const auto HCl = FiniteTestSystems::HCl; const auto H2O = FiniteTestSystems::H2O; const auto C6H6 = FiniteTestSystems::C6H6; const auto HCN = FiniteTestSystems::HCN; const auto HCNO = FiniteTestSystems::HCNO; const auto CHNOS = FiniteTestSystems::CHNOS; const auto EMPTY = FiniteTestSystems::EMPTY; const auto H4 = FiniteTestSystems::H4; const auto SF6 = FiniteTestSystems::SF6;

    SECTION( "mbtr_eindexf_dense_full" )
    {
        // C6H6 - k = 1, n = 2
        {
            FiniteTestSystems s(C6H6);
            vector<long> res = { 1, 6 };
            eindexf_test_indices<1, mbtr_eindexf_dense_full>(s, res);
        }

        // H3 - k = 2, n = 1
        {
            FiniteTestSystems s(H3);
            vector<long> res = { 1, 1 };
            eindexf_test_indices<2, mbtr_eindexf_dense_full>(s, res);
        }

        // H2O - k = 2, n = 2
        {
            FiniteTestSystems s(H2O);
            vector<long> res = { 1,1,  1,8,  8,1,  8,8 };
            eindexf_test_indices<2, mbtr_eindexf_dense_full>(s, res);
        }

        // HCN - k = 2, n = 3
        {
            FiniteTestSystems s(HCN);
            vector<long> res = { 1,1,  1,6,  1,7,  6,1,  6,6,  6,7,  7,1,  7,6,  7,7 };
            eindexf_test_indices<2, mbtr_eindexf_dense_full>(s, res);
        }

        // HCN - k = 3, n = 3
        {
            FiniteTestSystems s(HCN);
            vector<long> res = {
                1,1,1,  1,1,6,  1,1,7,  1,6,1,  1,6,6,  1,6,7,  1,7,1,  1,7,6,  1,7,7,
                6,1,1,  6,1,6,  6,1,7,  6,6,1,  6,6,6,  6,6,7,  6,7,1,  6,7,6,  6,7,7,
                7,1,1,  7,1,6,  7,1,7,  7,6,1,  7,6,6,  7,6,7,  7,7,1,  7,7,6,  7,7,7,
            };
            eindexf_test_indices<3, mbtr_eindexf_dense_full>(s, res);
        }

        // HCNO - k = 3, n = 4
        {
            FiniteTestSystems s(HCNO);
            vector<long> res = {  // 1 6 7 8
                1,1,1,  1,1,6,  1,1,7,  1,1,8,  1,6,1,  1,6,6,  1,6,7,  1,6,8,  1,7,1,  1,7,6,  1,7,7,  1,7,8,  1,8,1,  1,8,6,  1,8,7,  1,8,8,
                6,1,1,  6,1,6,  6,1,7,  6,1,8,  6,6,1,  6,6,6,  6,6,7,  6,6,8,  6,7,1,  6,7,6,  6,7,7,  6,7,8,  6,8,1,  6,8,6,  6,8,7,  6,8,8,
                7,1,1,  7,1,6,  7,1,7,  7,1,8,  7,6,1,  7,6,6,  7,6,7,  7,6,8,  7,7,1,  7,7,6,  7,7,7,  7,7,8,  7,8,1,  7,8,6,  7,8,7,  7,8,8,
                8,1,1,  8,1,6,  8,1,7,  8,1,8,  8,6,1,  8,6,6,  8,6,7,  8,6,8,  8,7,1,  8,7,6,  8,7,7,  8,7,8,  8,8,1,  8,8,6,  8,8,7,  8,8,8
            };
            eindexf_test_indices<3, mbtr_eindexf_dense_full>(s, res);
        }

        // HCNO - k = 4, n = 4
        // -> Mathematica notebook
        {
            FiniteTestSystems s(HCNO);
            vector<long> res = {  // 1 6 7 8
                1,1,1,1,  1,1,1,6,  1,1,1,7,  1,1,1,8,  1,1,6,1,  1,1,6,6,  1,1,6,7,  1,1,6,8,
                1,1,7,1,  1,1,7,6,  1,1,7,7,  1,1,7,8,  1,1,8,1,  1,1,8,6,  1,1,8,7,  1,1,8,8,
                1,6,1,1,  1,6,1,6,  1,6,1,7,  1,6,1,8,  1,6,6,1,  1,6,6,6,  1,6,6,7,  1,6,6,8,
                1,6,7,1,  1,6,7,6,  1,6,7,7,  1,6,7,8,  1,6,8,1,  1,6,8,6,  1,6,8,7,  1,6,8,8,
                1,7,1,1,  1,7,1,6,  1,7,1,7,  1,7,1,8,  1,7,6,1,  1,7,6,6,  1,7,6,7,  1,7,6,8,
                1,7,7,1,  1,7,7,6,  1,7,7,7,  1,7,7,8,  1,7,8,1,  1,7,8,6,  1,7,8,7,  1,7,8,8,
                1,8,1,1,  1,8,1,6,  1,8,1,7,  1,8,1,8,  1,8,6,1,  1,8,6,6,  1,8,6,7,  1,8,6,8,
                1,8,7,1,  1,8,7,6,  1,8,7,7,  1,8,7,8,  1,8,8,1,  1,8,8,6,  1,8,8,7,  1,8,8,8,
                6,1,1,1,  6,1,1,6,  6,1,1,7,  6,1,1,8,  6,1,6,1,  6,1,6,6,  6,1,6,7,  6,1,6,8,
                6,1,7,1,  6,1,7,6,  6,1,7,7,  6,1,7,8,  6,1,8,1,  6,1,8,6,  6,1,8,7,  6,1,8,8,
                6,6,1,1,  6,6,1,6,  6,6,1,7,  6,6,1,8,  6,6,6,1,  6,6,6,6,  6,6,6,7,  6,6,6,8,
                6,6,7,1,  6,6,7,6,  6,6,7,7,  6,6,7,8,  6,6,8,1,  6,6,8,6,  6,6,8,7,  6,6,8,8,
                6,7,1,1,  6,7,1,6,  6,7,1,7,  6,7,1,8,  6,7,6,1,  6,7,6,6,  6,7,6,7,  6,7,6,8,
                6,7,7,1,  6,7,7,6,  6,7,7,7,  6,7,7,8,  6,7,8,1,  6,7,8,6,  6,7,8,7,  6,7,8,8,
                6,8,1,1,  6,8,1,6,  6,8,1,7,  6,8,1,8,  6,8,6,1,  6,8,6,6,  6,8,6,7,  6,8,6,8,
                6,8,7,1,  6,8,7,6,  6,8,7,7,  6,8,7,8,  6,8,8,1,  6,8,8,6,  6,8,8,7,  6,8,8,8,
                7,1,1,1,  7,1,1,6,  7,1,1,7,  7,1,1,8,  7,1,6,1,  7,1,6,6,  7,1,6,7,  7,1,6,8,
                7,1,7,1,  7,1,7,6,  7,1,7,7,  7,1,7,8,  7,1,8,1,  7,1,8,6,  7,1,8,7,  7,1,8,8,
                7,6,1,1,  7,6,1,6,  7,6,1,7,  7,6,1,8,  7,6,6,1,  7,6,6,6,  7,6,6,7,  7,6,6,8,
                7,6,7,1,  7,6,7,6,  7,6,7,7,  7,6,7,8,  7,6,8,1,  7,6,8,6,  7,6,8,7,  7,6,8,8,
                7,7,1,1,  7,7,1,6,  7,7,1,7,  7,7,1,8,  7,7,6,1,  7,7,6,6,  7,7,6,7,  7,7,6,8,
                7,7,7,1,  7,7,7,6,  7,7,7,7,  7,7,7,8,  7,7,8,1,  7,7,8,6,  7,7,8,7,  7,7,8,8,
                7,8,1,1,  7,8,1,6,  7,8,1,7,  7,8,1,8,  7,8,6,1,  7,8,6,6,  7,8,6,7,  7,8,6,8,
                7,8,7,1,  7,8,7,6,  7,8,7,7,  7,8,7,8,  7,8,8,1,  7,8,8,6,  7,8,8,7,  7,8,8,8,
                8,1,1,1,  8,1,1,6,  8,1,1,7,  8,1,1,8,  8,1,6,1,  8,1,6,6,  8,1,6,7,  8,1,6,8,
                8,1,7,1,  8,1,7,6,  8,1,7,7,  8,1,7,8,  8,1,8,1,  8,1,8,6,  8,1,8,7,  8,1,8,8,
                8,6,1,1,  8,6,1,6,  8,6,1,7,  8,6,1,8,  8,6,6,1,  8,6,6,6,  8,6,6,7,  8,6,6,8,
                8,6,7,1,  8,6,7,6,  8,6,7,7,  8,6,7,8,  8,6,8,1,  8,6,8,6,  8,6,8,7,  8,6,8,8,
                8,7,1,1,  8,7,1,6,  8,7,1,7,  8,7,1,8,  8,7,6,1,  8,7,6,6,  8,7,6,7,  8,7,6,8,
                8,7,7,1,  8,7,7,6,  8,7,7,7,  8,7,7,8,  8,7,8,1,  8,7,8,6,  8,7,8,7,  8,7,8,8,
                8,8,1,1,  8,8,1,6,  8,8,1,7,  8,8,1,8,  8,8,6,1,  8,8,6,6,  8,8,6,7,  8,8,6,8,
                8,8,7,1,  8,8,7,6,  8,8,7,7,  8,8,7,8,  8,8,8,1,  8,8,8,6,  8,8,8,7,  8,8,8,8
            };
            eindexf_test_indices<4, mbtr_eindexf_dense_full>(s, res);
        }

        // CHNOS - k = 5, n = 4
        // -> Mathematica notebook
        {
            FiniteTestSystems s(CHNOS);
            vector<long> res = {  // 1 6 7 8 16
                 1, 1, 1, 1,   1, 1, 1, 6,   1, 1, 1, 7,   1, 1, 1, 8,   1, 1, 1,16,   1, 1, 6, 1,   1, 1, 6, 6,   1, 1, 6, 7,   1, 1, 6, 8,   1, 1, 6,16,
                 1, 1, 7, 1,   1, 1, 7, 6,   1, 1, 7, 7,   1, 1, 7, 8,   1, 1, 7,16,   1, 1, 8, 1,   1, 1, 8, 6,   1, 1, 8, 7,   1, 1, 8, 8,   1, 1, 8,16,
                 1, 1,16, 1,   1, 1,16, 6,   1, 1,16, 7,   1, 1,16, 8,   1, 1,16,16,   1, 6, 1, 1,   1, 6, 1, 6,   1, 6, 1, 7,   1, 6, 1, 8,   1, 6, 1,16,
                 1, 6, 6, 1,   1, 6, 6, 6,   1, 6, 6, 7,   1, 6, 6, 8,   1, 6, 6,16,   1, 6, 7, 1,   1, 6, 7, 6,   1, 6, 7, 7,   1, 6, 7, 8,   1, 6, 7,16,
                 1, 6, 8, 1,   1, 6, 8, 6,   1, 6, 8, 7,   1, 6, 8, 8,   1, 6, 8,16,   1, 6,16, 1,   1, 6,16, 6,   1, 6,16, 7,   1, 6,16, 8,   1, 6,16,16,
                 1, 7, 1, 1,   1, 7, 1, 6,   1, 7, 1, 7,   1, 7, 1, 8,   1, 7, 1,16,   1, 7, 6, 1,   1, 7, 6, 6,   1, 7, 6, 7,   1, 7, 6, 8,   1, 7, 6,16,
                 1, 7, 7, 1,   1, 7, 7, 6,   1, 7, 7, 7,   1, 7, 7, 8,   1, 7, 7,16,   1, 7, 8, 1,   1, 7, 8, 6,   1, 7, 8, 7,   1, 7, 8, 8,   1, 7, 8,16,
                 1, 7,16, 1,   1, 7,16, 6,   1, 7,16, 7,   1, 7,16, 8,   1, 7,16,16,   1, 8, 1, 1,   1, 8, 1, 6,   1, 8, 1, 7,   1, 8, 1, 8,   1, 8, 1,16,
                 1, 8, 6, 1,   1, 8, 6, 6,   1, 8, 6, 7,   1, 8, 6, 8,   1, 8, 6,16,   1, 8, 7, 1,   1, 8, 7, 6,   1, 8, 7, 7,   1, 8, 7, 8,   1, 8, 7,16,
                 1, 8, 8, 1,   1, 8, 8, 6,   1, 8, 8, 7,   1, 8, 8, 8,   1, 8, 8,16,   1, 8,16, 1,   1, 8,16, 6,   1, 8,16, 7,   1, 8,16, 8,   1, 8,16,16,
                 1,16, 1, 1,   1,16, 1, 6,   1,16, 1, 7,   1,16, 1, 8,   1,16, 1,16,   1,16, 6, 1,   1,16, 6, 6,   1,16, 6, 7,   1,16, 6, 8,   1,16, 6,16,
                 1,16, 7, 1,   1,16, 7, 6,   1,16, 7, 7,   1,16, 7, 8,   1,16, 7,16,   1,16, 8, 1,   1,16, 8, 6,   1,16, 8, 7,   1,16, 8, 8,   1,16, 8,16,
                 1,16,16, 1,   1,16,16, 6,   1,16,16, 7,   1,16,16, 8,   1,16,16,16,   6, 1, 1, 1,   6, 1, 1, 6,   6, 1, 1, 7,   6, 1, 1, 8,   6, 1, 1,16,
                 6, 1, 6, 1,   6, 1, 6, 6,   6, 1, 6, 7,   6, 1, 6, 8,   6, 1, 6,16,   6, 1, 7, 1,   6, 1, 7, 6,   6, 1, 7, 7,   6, 1, 7, 8,   6, 1, 7,16,
                 6, 1, 8, 1,   6, 1, 8, 6,   6, 1, 8, 7,   6, 1, 8, 8,   6, 1, 8,16,   6, 1,16, 1,   6, 1,16, 6,   6, 1,16, 7,   6, 1,16, 8,   6, 1,16,16,
                 6, 6, 1, 1,   6, 6, 1, 6,   6, 6, 1, 7,   6, 6, 1, 8,   6, 6, 1,16,   6, 6, 6, 1,   6, 6, 6, 6,   6, 6, 6, 7,   6, 6, 6, 8,   6, 6, 6,16,
                 6, 6, 7, 1,   6, 6, 7, 6,   6, 6, 7, 7,   6, 6, 7, 8,   6, 6, 7,16,   6, 6, 8, 1,   6, 6, 8, 6,   6, 6, 8, 7,   6, 6, 8, 8,   6, 6, 8,16,
                 6, 6,16, 1,   6, 6,16, 6,   6, 6,16, 7,   6, 6,16, 8,   6, 6,16,16,   6, 7, 1, 1,   6, 7, 1, 6,   6, 7, 1, 7,   6, 7, 1, 8,   6, 7, 1,16,
                 6, 7, 6, 1,   6, 7, 6, 6,   6, 7, 6, 7,   6, 7, 6, 8,   6, 7, 6,16,   6, 7, 7, 1,   6, 7, 7, 6,   6, 7, 7, 7,   6, 7, 7, 8,   6, 7, 7,16,
                 6, 7, 8, 1,   6, 7, 8, 6,   6, 7, 8, 7,   6, 7, 8, 8,   6, 7, 8,16,   6, 7,16, 1,   6, 7,16, 6,   6, 7,16, 7,   6, 7,16, 8,   6, 7,16,16,
                 6, 8, 1, 1,   6, 8, 1, 6,   6, 8, 1, 7,   6, 8, 1, 8,   6, 8, 1,16,   6, 8, 6, 1,   6, 8, 6, 6,   6, 8, 6, 7,   6, 8, 6, 8,   6, 8, 6,16,
                 6, 8, 7, 1,   6, 8, 7, 6,   6, 8, 7, 7,   6, 8, 7, 8,   6, 8, 7,16,   6, 8, 8, 1,   6, 8, 8, 6,   6, 8, 8, 7,   6, 8, 8, 8,   6, 8, 8,16,
                 6, 8,16, 1,   6, 8,16, 6,   6, 8,16, 7,   6, 8,16, 8,   6, 8,16,16,   6,16, 1, 1,   6,16, 1, 6,   6,16, 1, 7,   6,16, 1, 8,   6,16, 1,16,
                 6,16, 6, 1,   6,16, 6, 6,   6,16, 6, 7,   6,16, 6, 8,   6,16, 6,16,   6,16, 7, 1,   6,16, 7, 6,   6,16, 7, 7,   6,16, 7, 8,   6,16, 7,16,
                 6,16, 8, 1,   6,16, 8, 6,   6,16, 8, 7,   6,16, 8, 8,   6,16, 8,16,   6,16,16, 1,   6,16,16, 6,   6,16,16, 7,   6,16,16, 8,   6,16,16,16,
                 7, 1, 1, 1,   7, 1, 1, 6,   7, 1, 1, 7,   7, 1, 1, 8,   7, 1, 1,16,   7, 1, 6, 1,   7, 1, 6, 6,   7, 1, 6, 7,   7, 1, 6, 8,   7, 1, 6,16,
                 7, 1, 7, 1,   7, 1, 7, 6,   7, 1, 7, 7,   7, 1, 7, 8,   7, 1, 7,16,   7, 1, 8, 1,   7, 1, 8, 6,   7, 1, 8, 7,   7, 1, 8, 8,   7, 1, 8,16,
                 7, 1,16, 1,   7, 1,16, 6,   7, 1,16, 7,   7, 1,16, 8,   7, 1,16,16,   7, 6, 1, 1,   7, 6, 1, 6,   7, 6, 1, 7,   7, 6, 1, 8,   7, 6, 1,16,
                 7, 6, 6, 1,   7, 6, 6, 6,   7, 6, 6, 7,   7, 6, 6, 8,   7, 6, 6,16,   7, 6, 7, 1,   7, 6, 7, 6,   7, 6, 7, 7,   7, 6, 7, 8,   7, 6, 7,16,
                 7, 6, 8, 1,   7, 6, 8, 6,   7, 6, 8, 7,   7, 6, 8, 8,   7, 6, 8,16,   7, 6,16, 1,   7, 6,16, 6,   7, 6,16, 7,   7, 6,16, 8,   7, 6,16,16,
                 7, 7, 1, 1,   7, 7, 1, 6,   7, 7, 1, 7,   7, 7, 1, 8,   7, 7, 1,16,   7, 7, 6, 1,   7, 7, 6, 6,   7, 7, 6, 7,   7, 7, 6, 8,   7, 7, 6,16,
                 7, 7, 7, 1,   7, 7, 7, 6,   7, 7, 7, 7,   7, 7, 7, 8,   7, 7, 7,16,   7, 7, 8, 1,   7, 7, 8, 6,   7, 7, 8, 7,   7, 7, 8, 8,   7, 7, 8,16,
                 7, 7,16, 1,   7, 7,16, 6,   7, 7,16, 7,   7, 7,16, 8,   7, 7,16,16,   7, 8, 1, 1,   7, 8, 1, 6,   7, 8, 1, 7,   7, 8, 1, 8,   7, 8, 1,16,
                 7, 8, 6, 1,   7, 8, 6, 6,   7, 8, 6, 7,   7, 8, 6, 8,   7, 8, 6,16,   7, 8, 7, 1,   7, 8, 7, 6,   7, 8, 7, 7,   7, 8, 7, 8,   7, 8, 7,16,
                 7, 8, 8, 1,   7, 8, 8, 6,   7, 8, 8, 7,   7, 8, 8, 8,   7, 8, 8,16,   7, 8,16, 1,   7, 8,16, 6,   7, 8,16, 7,   7, 8,16, 8,   7, 8,16,16,
                 7,16, 1, 1,   7,16, 1, 6,   7,16, 1, 7,   7,16, 1, 8,   7,16, 1,16,   7,16, 6, 1,   7,16, 6, 6,   7,16, 6, 7,   7,16, 6, 8,   7,16, 6,16,
                 7,16, 7, 1,   7,16, 7, 6,   7,16, 7, 7,   7,16, 7, 8,   7,16, 7,16,   7,16, 8, 1,   7,16, 8, 6,   7,16, 8, 7,   7,16, 8, 8,   7,16, 8,16,
                 7,16,16, 1,   7,16,16, 6,   7,16,16, 7,   7,16,16, 8,   7,16,16,16,   8, 1, 1, 1,   8, 1, 1, 6,   8, 1, 1, 7,   8, 1, 1, 8,   8, 1, 1,16,
                 8, 1, 6, 1,   8, 1, 6, 6,   8, 1, 6, 7,   8, 1, 6, 8,   8, 1, 6,16,   8, 1, 7, 1,   8, 1, 7, 6,   8, 1, 7, 7,   8, 1, 7, 8,   8, 1, 7,16,
                 8, 1, 8, 1,   8, 1, 8, 6,   8, 1, 8, 7,   8, 1, 8, 8,   8, 1, 8,16,   8, 1,16, 1,   8, 1,16, 6,   8, 1,16, 7,   8, 1,16, 8,   8, 1,16,16,
                 8, 6, 1, 1,   8, 6, 1, 6,   8, 6, 1, 7,   8, 6, 1, 8,   8, 6, 1,16,   8, 6, 6, 1,   8, 6, 6, 6,   8, 6, 6, 7,   8, 6, 6, 8,   8, 6, 6,16,
                 8, 6, 7, 1,   8, 6, 7, 6,   8, 6, 7, 7,   8, 6, 7, 8,   8, 6, 7,16,   8, 6, 8, 1,   8, 6, 8, 6,   8, 6, 8, 7,   8, 6, 8, 8,   8, 6, 8,16,
                 8, 6,16, 1,   8, 6,16, 6,   8, 6,16, 7,   8, 6,16, 8,   8, 6,16,16,   8, 7, 1, 1,   8, 7, 1, 6,   8, 7, 1, 7,   8, 7, 1, 8,   8, 7, 1,16,
                 8, 7, 6, 1,   8, 7, 6, 6,   8, 7, 6, 7,   8, 7, 6, 8,   8, 7, 6,16,   8, 7, 7, 1,   8, 7, 7, 6,   8, 7, 7, 7,   8, 7, 7, 8,   8, 7, 7,16,
                 8, 7, 8, 1,   8, 7, 8, 6,   8, 7, 8, 7,   8, 7, 8, 8,   8, 7, 8,16,   8, 7,16, 1,   8, 7,16, 6,   8, 7,16, 7,   8, 7,16, 8,   8, 7,16,16,
                 8, 8, 1, 1,   8, 8, 1, 6,   8, 8, 1, 7,   8, 8, 1, 8,   8, 8, 1,16,   8, 8, 6, 1,   8, 8, 6, 6,   8, 8, 6, 7,   8, 8, 6, 8,   8, 8, 6,16,
                 8, 8, 7, 1,   8, 8, 7, 6,   8, 8, 7, 7,   8, 8, 7, 8,   8, 8, 7,16,   8, 8, 8, 1,   8, 8, 8, 6,   8, 8, 8, 7,   8, 8, 8, 8,   8, 8, 8,16,
                 8, 8,16, 1,   8, 8,16, 6,   8, 8,16, 7,   8, 8,16, 8,   8, 8,16,16,   8,16, 1, 1,   8,16, 1, 6,   8,16, 1, 7,   8,16, 1, 8,   8,16, 1,16,
                 8,16, 6, 1,   8,16, 6, 6,   8,16, 6, 7,   8,16, 6, 8,   8,16, 6,16,   8,16, 7, 1,   8,16, 7, 6,   8,16, 7, 7,   8,16, 7, 8,   8,16, 7,16,
                 8,16, 8, 1,   8,16, 8, 6,   8,16, 8, 7,   8,16, 8, 8,   8,16, 8,16,   8,16,16, 1,   8,16,16, 6,   8,16,16, 7,   8,16,16, 8,   8,16,16,16,
                16, 1, 1, 1,  16, 1, 1, 6,  16, 1, 1, 7,  16, 1, 1, 8,  16, 1, 1,16,  16, 1, 6, 1,  16, 1, 6, 6,  16, 1, 6, 7,  16, 1, 6, 8,  16, 1, 6,16,
                16, 1, 7, 1,  16, 1, 7, 6,  16, 1, 7, 7,  16, 1, 7, 8,  16, 1, 7,16,  16, 1, 8, 1,  16, 1, 8, 6,  16, 1, 8, 7,  16, 1, 8, 8,  16, 1, 8,16,
                16, 1,16, 1,  16, 1,16, 6,  16, 1,16, 7,  16, 1,16, 8,  16, 1,16,16,  16, 6, 1, 1,  16, 6, 1, 6,  16, 6, 1, 7,  16, 6, 1, 8,  16, 6, 1,16,
                16, 6, 6, 1,  16, 6, 6, 6,  16, 6, 6, 7,  16, 6, 6, 8,  16, 6, 6,16,  16, 6, 7, 1,  16, 6, 7, 6,  16, 6, 7, 7,  16, 6, 7, 8,  16, 6, 7,16,
                16, 6, 8, 1,  16, 6, 8, 6,  16, 6, 8, 7,  16, 6, 8, 8,  16, 6, 8,16,  16, 6,16, 1,  16, 6,16, 6,  16, 6,16, 7,  16, 6,16, 8,  16, 6,16,16,
                16, 7, 1, 1,  16, 7, 1, 6,  16, 7, 1, 7,  16, 7, 1, 8,  16, 7, 1,16,  16, 7, 6, 1,  16, 7, 6, 6,  16, 7, 6, 7,  16, 7, 6, 8,  16, 7, 6,16,
                16, 7, 7, 1,  16, 7, 7, 6,  16, 7, 7, 7,  16, 7, 7, 8,  16, 7, 7,16,  16, 7, 8, 1,  16, 7, 8, 6,  16, 7, 8, 7,  16, 7, 8, 8,  16, 7, 8,16,
                16, 7,16, 1,  16, 7,16, 6,  16, 7,16, 7,  16, 7,16, 8,  16, 7,16,16,  16, 8, 1, 1,  16, 8, 1, 6,  16, 8, 1, 7,  16, 8, 1, 8,  16, 8, 1,16,
                16, 8, 6, 1,  16, 8, 6, 6,  16, 8, 6, 7,  16, 8, 6, 8,  16, 8, 6,16,  16, 8, 7, 1,  16, 8, 7, 6,  16, 8, 7, 7,  16, 8, 7, 8,  16, 8, 7,16,
                16, 8, 8, 1,  16, 8, 8, 6,  16, 8, 8, 7,  16, 8, 8, 8,  16, 8, 8,16,  16, 8,16, 1,  16, 8,16, 6,  16, 8,16, 7,  16, 8,16, 8,  16, 8,16,16,
                16,16, 1, 1,  16,16, 1, 6,  16,16, 1, 7,  16,16, 1, 8,  16,16, 1,16,  16,16, 6, 1,  16,16, 6, 6,  16,16, 6, 7,  16,16, 6, 8,  16,16, 6,16,
                16,16, 7, 1,  16,16, 7, 6,  16,16, 7, 7,  16,16, 7, 8,  16,16, 7,16,  16,16, 8, 1,  16,16, 8, 6,  16,16, 8, 7,  16,16, 8, 8,  16,16, 8,16,
                16,16,16, 1,  16,16,16, 6,  16,16,16, 7,  16,16,16, 8,  16,16,16,16
            };
            eindexf_test_indices<4, mbtr_eindexf_dense_full>(s, res);
        }
    }

    SECTION( "mbtr_eindexf_dense_noreversals" )
    {
        // C6H6 - k = 1, n = 2
        {
            FiniteTestSystems s(C6H6);
            vector<long> res = { 1, 6 };
            eindexf_test_indices<1, mbtr_eindexf_dense_noreversals>(s, res);
        }

        // H3 - k = 2, n = 1
        {
            FiniteTestSystems s(H3);
            vector<long> res = { 1, 1 };
            eindexf_test_indices<2, mbtr_eindexf_dense_noreversals>(s, res);
        }

        // H2O - k = 2, n = 2
        {
            FiniteTestSystems s(H2O);
            vector<long> res = { 1,1,  1,8,  8,8 };
            eindexf_test_indices<2, mbtr_eindexf_dense_noreversals>(s, res);
        }

        // HCN - k = 2, n = 3
        {
            FiniteTestSystems s(HCN);
            vector<long> res = { 1,1,  1,6,  1,7,  6,6,  6,7,  7,7 };
            eindexf_test_indices<2, mbtr_eindexf_dense_noreversals>(s, res);
        }

        // HCN - k = 3, n = 3
        {
            FiniteTestSystems s(HCN);
            vector<long> res = {
                1,1,1,  1,1,6,  1,1,7,  1,6,1,  1,6,6,  1,6,7,  1,7,1,  1,7,6,  1,7,7,
                6,1,6,  6,1,7,  6,6,6,  6,6,7,  6,7,6,  6,7,7,  7,1,7,  7,6,7,  7,7,7,
            };
            eindexf_test_indices<3, mbtr_eindexf_dense_noreversals>(s, res);
        }

        // HCNO - k = 3, n = 4
        {
            FiniteTestSystems s(HCNO);
            vector<long> res = {  // 1 6 7 8
                1,1,1,  1,1,6,  1,1,7,  1,1,8,  1,6,1,  1,6,6,  1,6,7,  1,6,8,  1,7,1,  1,7,6,  1,7,7,  1,7,8,  1,8,1,  1,8,6,  1,8,7,  1,8,8,
                6,1,6,  6,1,7,  6,1,8,  6,6,6,  6,6,7,  6,6,8,  6,7,6,  6,7,7,  6,7,8,  6,8,6,  6,8,7,  6,8,8,  7,1,7,  7,1,8,  7,6,7,  7,6,8,
                7,7,7,  7,7,8,  7,8,7,  7,8,8,  8,1,8,  8,6,8,  8,7,8,  8,8,8
            };
            eindexf_test_indices<3, mbtr_eindexf_dense_noreversals>(s, res);
        }

        // HCNO - k = 4, n = 4
        // -> Mathematica notebook
        {
            FiniteTestSystems s(HCNO);
            vector<long> res = {  // 1 6 7 8
                1,1,1,1,  1,1,1,6,  1,1,1,7,  1,1,1,8,  1,1,6,1,  1,1,6,6,  1,1,6,7,  1,1,6,8,  1,1,7,1,  1,1,7,6,  1,1,7,7,  1,1,7,8,
                1,1,8,1,  1,1,8,6,  1,1,8,7,  1,1,8,8,  1,6,1,6,  1,6,1,7,  1,6,1,8,  1,6,6,1,  1,6,6,6,  1,6,6,7,  1,6,6,8,  1,6,7,1,
                1,6,7,6,  1,6,7,7,  1,6,7,8,  1,6,8,1,  1,6,8,6,  1,6,8,7,  1,6,8,8,  1,7,1,6,  1,7,1,7,  1,7,1,8,  1,7,6,6,  1,7,6,7,
                1,7,6,8,  1,7,7,1,  1,7,7,6,  1,7,7,7,  1,7,7,8,  1,7,8,1,  1,7,8,6,  1,7,8,7,  1,7,8,8,  1,8,1,6,  1,8,1,7,  1,8,1,8,
                1,8,6,6,  1,8,6,7,  1,8,6,8,  1,8,7,6,  1,8,7,7,  1,8,7,8,  1,8,8,1,  1,8,8,6,  1,8,8,7,  1,8,8,8,  6,1,1,6,  6,1,1,7,
                6,1,1,8,  6,1,6,6,  6,1,6,7,  6,1,6,8,  6,1,7,6,  6,1,7,7,  6,1,7,8,  6,1,8,6,  6,1,8,7,  6,1,8,8,  6,6,1,7,  6,6,1,8,
                6,6,6,6,  6,6,6,7,  6,6,6,8,  6,6,7,6,  6,6,7,7,  6,6,7,8,  6,6,8,6,  6,6,8,7,  6,6,8,8,  6,7,1,7,  6,7,1,8,  6,7,6,7,
                6,7,6,8,  6,7,7,6,  6,7,7,7,  6,7,7,8,  6,7,8,6,  6,7,8,7,  6,7,8,8,  6,8,1,7,  6,8,1,8,  6,8,6,7,  6,8,6,8,  6,8,7,7,
                6,8,7,8,  6,8,8,6,  6,8,8,7,  6,8,8,8,  7,1,1,7,  7,1,1,8,  7,1,6,7,  7,1,6,8,  7,1,7,7,  7,1,7,8,  7,1,8,7,  7,1,8,8,
                7,6,1,8,  7,6,6,7,  7,6,6,8,  7,6,7,7,  7,6,7,8,  7,6,8,7,  7,6,8,8,  7,7,1,8,  7,7,6,8,  7,7,7,7,  7,7,7,8,  7,7,8,7,
                7,7,8,8,  7,8,1,8,  7,8,6,8,  7,8,7,8,  7,8,8,7,  7,8,8,8,  8,1,1,8,  8,1,6,8,  8,1,7,8,  8,1,8,8,  8,6,6,8,  8,6,7,8,
                8,6,8,8,  8,7,7,8,  8,7,8,8,  8,8,8,8
            };
            eindexf_test_indices<4, mbtr_eindexf_dense_noreversals>(s, res);
        }

        // CHNOS - k = 4, n = 5
        // -> Mathematica notebook
        {
            FiniteTestSystems s(CHNOS);
            vector<long> res = {  // 1 6 7 8 16
                 1, 1, 1, 1,   1, 1, 1, 6,   1, 1, 1, 7,   1, 1, 1, 8,   1, 1, 1,16,   1, 1, 6, 1,   1, 1, 6, 6,   1, 1, 6, 7,   1, 1, 6, 8,
                 1, 1, 6,16,   1, 1, 7, 1,   1, 1, 7, 6,   1, 1, 7, 7,   1, 1, 7, 8,   1, 1, 7,16,   1, 1, 8, 1,   1, 1, 8, 6,   1, 1, 8, 7,
                 1, 1, 8, 8,   1, 1, 8,16,   1, 1,16, 1,   1, 1,16, 6,   1, 1,16, 7,   1, 1,16, 8,   1, 1,16,16,   1, 6, 1, 6,   1, 6, 1, 7,
                 1, 6, 1, 8,   1, 6, 1,16,   1, 6, 6, 1,   1, 6, 6, 6,   1, 6, 6, 7,   1, 6, 6, 8,   1, 6, 6,16,   1, 6, 7, 1,   1, 6, 7, 6,
                 1, 6, 7, 7,   1, 6, 7, 8,   1, 6, 7,16,   1, 6, 8, 1,   1, 6, 8, 6,   1, 6, 8, 7,   1, 6, 8, 8,   1, 6, 8,16,   1, 6,16, 1,
                 1, 6,16, 6,   1, 6,16, 7,   1, 6,16, 8,   1, 6,16,16,   1, 7, 1, 6,   1, 7, 1, 7,   1, 7, 1, 8,   1, 7, 1,16,   1, 7, 6, 6,
                 1, 7, 6, 7,   1, 7, 6, 8,   1, 7, 6,16,   1, 7, 7, 1,   1, 7, 7, 6,   1, 7, 7, 7,   1, 7, 7, 8,   1, 7, 7,16,   1, 7, 8, 1,
                 1, 7, 8, 6,   1, 7, 8, 7,   1, 7, 8, 8,   1, 7, 8,16,   1, 7,16, 1,   1, 7,16, 6,   1, 7,16, 7,   1, 7,16, 8,   1, 7,16,16,
                 1, 8, 1, 6,   1, 8, 1, 7,   1, 8, 1, 8,   1, 8, 1,16,   1, 8, 6, 6,   1, 8, 6, 7,   1, 8, 6, 8,   1, 8, 6,16,   1, 8, 7, 6,
                 1, 8, 7, 7,   1, 8, 7, 8,   1, 8, 7,16,   1, 8, 8, 1,   1, 8, 8, 6,   1, 8, 8, 7,   1, 8, 8, 8,   1, 8, 8,16,   1, 8,16, 1,
                 1, 8,16, 6,   1, 8,16, 7,   1, 8,16, 8,   1, 8,16,16,   1,16, 1, 6,   1,16, 1, 7,   1,16, 1, 8,   1,16, 1,16,   1,16, 6, 6,
                 1,16, 6, 7,   1,16, 6, 8,   1,16, 6,16,   1,16, 7, 6,   1,16, 7, 7,   1,16, 7, 8,   1,16, 7,16,   1,16, 8, 6,   1,16, 8, 7,
                 1,16, 8, 8,   1,16, 8,16,   1,16,16, 1,   1,16,16, 6,   1,16,16, 7,   1,16,16, 8,   1,16,16,16,   6, 1, 1, 6,   6, 1, 1, 7,
                 6, 1, 1, 8,   6, 1, 1,16,   6, 1, 6, 6,   6, 1, 6, 7,   6, 1, 6, 8,   6, 1, 6,16,   6, 1, 7, 6,   6, 1, 7, 7,   6, 1, 7, 8,
                 6, 1, 7,16,   6, 1, 8, 6,   6, 1, 8, 7,   6, 1, 8, 8,   6, 1, 8,16,   6, 1,16, 6,   6, 1,16, 7,   6, 1,16, 8,   6, 1,16,16,
                 6, 6, 1, 7,   6, 6, 1, 8,   6, 6, 1,16,   6, 6, 6, 6,   6, 6, 6, 7,   6, 6, 6, 8,   6, 6, 6,16,   6, 6, 7, 6,   6, 6, 7, 7,
                 6, 6, 7, 8,   6, 6, 7,16,   6, 6, 8, 6,   6, 6, 8, 7,   6, 6, 8, 8,   6, 6, 8,16,   6, 6,16, 6,   6, 6,16, 7,   6, 6,16, 8,
                 6, 6,16,16,   6, 7, 1, 7,   6, 7, 1, 8,   6, 7, 1,16,   6, 7, 6, 7,   6, 7, 6, 8,   6, 7, 6,16,   6, 7, 7, 6,   6, 7, 7, 7,
                 6, 7, 7, 8,   6, 7, 7,16,   6, 7, 8, 6,   6, 7, 8, 7,   6, 7, 8, 8,   6, 7, 8,16,   6, 7,16, 6,   6, 7,16, 7,   6, 7,16, 8,
                 6, 7,16,16,   6, 8, 1, 7,   6, 8, 1, 8,   6, 8, 1,16,   6, 8, 6, 7,   6, 8, 6, 8,   6, 8, 6,16,   6, 8, 7, 7,   6, 8, 7, 8,
                 6, 8, 7,16,   6, 8, 8, 6,   6, 8, 8, 7,   6, 8, 8, 8,   6, 8, 8,16,   6, 8,16, 6,   6, 8,16, 7,   6, 8,16, 8,   6, 8,16,16,
                 6,16, 1, 7,   6,16, 1, 8,   6,16, 1,16,   6,16, 6, 7,   6,16, 6, 8,   6,16, 6,16,   6,16, 7, 7,   6,16, 7, 8,   6,16, 7,16,
                 6,16, 8, 7,   6,16, 8, 8,   6,16, 8,16,   6,16,16, 6,   6,16,16, 7,   6,16,16, 8,   6,16,16,16,   7, 1, 1, 7,   7, 1, 1, 8,
                 7, 1, 1,16,   7, 1, 6, 7,   7, 1, 6, 8,   7, 1, 6,16,   7, 1, 7, 7,   7, 1, 7, 8,   7, 1, 7,16,   7, 1, 8, 7,   7, 1, 8, 8,
                 7, 1, 8,16,   7, 1,16, 7,   7, 1,16, 8,   7, 1,16,16,   7, 6, 1, 8,   7, 6, 1,16,   7, 6, 6, 7,   7, 6, 6, 8,   7, 6, 6,16,
                 7, 6, 7, 7,   7, 6, 7, 8,   7, 6, 7,16,   7, 6, 8, 7,   7, 6, 8, 8,   7, 6, 8,16,   7, 6,16, 7,   7, 6,16, 8,   7, 6,16,16,
                 7, 7, 1, 8,   7, 7, 1,16,   7, 7, 6, 8,   7, 7, 6,16,   7, 7, 7, 7,   7, 7, 7, 8,   7, 7, 7,16,   7, 7, 8, 7,   7, 7, 8, 8,
                 7, 7, 8,16,   7, 7,16, 7,   7, 7,16, 8,   7, 7,16,16,   7, 8, 1, 8,   7, 8, 1,16,   7, 8, 6, 8,   7, 8, 6,16,   7, 8, 7, 8,
                 7, 8, 7,16,   7, 8, 8, 7,   7, 8, 8, 8,   7, 8, 8,16,   7, 8,16, 7,   7, 8,16, 8,   7, 8,16,16,   7,16, 1, 8,   7,16, 1,16,
                 7,16, 6, 8,   7,16, 6,16,   7,16, 7, 8,   7,16, 7,16,   7,16, 8, 8,   7,16, 8,16,   7,16,16, 7,   7,16,16, 8,   7,16,16,16,
                 8, 1, 1, 8,   8, 1, 1,16,   8, 1, 6, 8,   8, 1, 6,16,   8, 1, 7, 8,   8, 1, 7,16,   8, 1, 8, 8,   8, 1, 8,16,   8, 1,16, 8,
                 8, 1,16,16,   8, 6, 1,16,   8, 6, 6, 8,   8, 6, 6,16,   8, 6, 7, 8,   8, 6, 7,16,   8, 6, 8, 8,   8, 6, 8,16,   8, 6,16, 8,
                 8, 6,16,16,   8, 7, 1,16,   8, 7, 6,16,   8, 7, 7, 8,   8, 7, 7,16,   8, 7, 8, 8,   8, 7, 8,16,   8, 7,16, 8,   8, 7,16,16,
                 8, 8, 1,16,   8, 8, 6,16,   8, 8, 7,16,   8, 8, 8, 8,   8, 8, 8,16,   8, 8,16, 8,   8, 8,16,16,   8,16, 1,16,   8,16, 6,16,
                 8,16, 7,16,   8,16, 8,16,   8,16,16, 8,   8,16,16,16,   16, 1, 1,16,  16, 1, 6,16,  16, 1, 7,16,  16, 1, 8,16,  16, 1,16,16,
                16, 6, 6,16,  16, 6, 7,16,  16, 6, 8,16,  16, 6,16,16,   16, 7, 7,16,  16, 7, 8,16,  16, 7,16,16,  16, 8, 8,16,  16, 8,16,16,
                16,16,16,16
            };
            eindexf_test_indices<4, mbtr_eindexf_dense_noreversals>(s, res);
        }
    }
}


//  ///////////////////////////////
//  //  Atom indexing functions  //
//  ///////////////////////////////

// Helper class (geometry function)
// Maximal distance from center atom a; only for periodic systems
struct test_geomf_log_maxdist
{
    template<typename T> void init(T const &) {}

    void reset(long) {}

    double distance(long const, double const ra[3], long const, double const rb[3]) const
    {
        // sqrt(<ra-rb,ra-rb>)
        return std::sqrt
        (
            (ra[0] - rb[0]) * (ra[0] - rb[0]) +
            (ra[1] - rb[1]) * (ra[1] - rb[1]) +
            (ra[2] - rb[2]) * (ra[2] - rb[2])
        );
    }

    double operator()(long const za, double const ra[3], long const zb, double const rb[3]) const
    {
        return 1. / distance(za, ra, zb, rb);
    }

    double operator()(long const za, double const ra[3], long const zb, double const rb[3], long const zc, double const rc[3]) const
    {
        return 1. / std::max(distance(za, ra, zb, rb), distance(za, ra, zc, rc));  // note special role of a
    }

    double operator()(long const, double const ra[3], long const, double const rb[3], long const, double const rc[3], long const, double const rd[3]) const
    {
        return 1. / std::max( { distance(0, ra, 0, rb), distance(0, ra, 0, rc), distance(0, ra, 0, rd) } );  // note special role of a
    }
};

// Helper class (geometry function) that logs passed atoms.
// Enables testing via public interface, that is without depending on internals of tested atom indices classes.
struct test_geomf_log
{
    test_geomf_log(std::vector<double> & log) : log_(log) { log_.reserve(500); }

    template<typename T> void init(T const &) {}

    void reset(long) {}

    double operator()(long za, double const * ra)
    {
        CHECK( za > 0 ); log_.push_back(za); log_.insert(log_.end(), ra, ra+3);
        return 1;
    }

    double operator()(long const za, double const * ra, long const zb, double const * rb)
    {
        CHECK( za > 0 ); log_.push_back(za); log_.insert(log_.end(), ra, ra+3);
        CHECK( zb > 0 ); log_.push_back(zb); log_.insert(log_.end(), rb, rb+3);
        return test_geomf_log_maxdist()(za, ra, zb, rb);  // weighting function needs geometry function to decay with distance
    }

    double operator()(long za, double const * ra, long zb, double const * rb, long zc, double const * rc)
    {
        CHECK( za > 0 ); log_.push_back(za); log_.insert(log_.end(), ra, ra+3);
        CHECK( zb > 0 ); log_.push_back(zb); log_.insert(log_.end(), rb, rb+3);
        CHECK( zc > 0);  log_.push_back(zc); log_.insert(log_.end(), rc, rc+3);
        return test_geomf_log_maxdist()(za, ra, zb, rb, zc, rc);
    }

    double operator()(long za, double const * ra, long zb, double const * rb, long zc, double const * rc, long zd, double const * rd)
    {
        CHECK( za > 0 ); log_.push_back(za); log_.insert(log_.end(), ra, ra+3);
        CHECK( zb > 0 ); log_.push_back(zb); log_.insert(log_.end(), rb, rb+3);
        CHECK( zc > 0 ); log_.push_back(zc); log_.insert(log_.end(), rc, rc+3);
        CHECK( zd > 0 ); log_.push_back(zd); log_.insert(log_.end(), rd, rd+3);
        return test_geomf_log_maxdist()(za, ra, zb, rb, zc, rc, zd, rd);
    }

    std::vector<double> & log_;
};

// Tests whether actually generated atoms match those generated by the expected indexing.
// res contains atom index tuples in expected order.
// For finite systems, the radius can be ignored as atom indexing does not depend on the weighting function.
template<int K, typename A> void aindexf_test_indices(TestSystems const & s, std::vector<long> const & res, double const radius = 0)
{
    using std::vector;

    const long block_size = A::is_periodic ? (K + (K-1)*3) : K ;
    const long n = res.size() / block_size;  // number of "lines" (index tuples)

    // generate atom sequence from iteration via logging class
    vector<double> log;
    auto mbtr = mbtr_call<K>(
        nullptr, s, -10, 1, 20, test_geomf_log(log), mbtr_weightf_delta_one_over_identity(radius), mbtr_normal_distribution(1e-10),
        mbtr_identity_correlation(), mbtr_eindexf_dense_noreversals(), A()
    );
    CHECK( log.size() == n*4*K );

    // generate atom sequence from passed indexing
    vector<double> reslog; auto logf = test_geomf_log(reslog);
    for(long i = 0; i < n; ++i)
    {
        const long ik = i*block_size;
        double const * const b = s.bb_;
        if( A::is_periodic )
        {
            long za, zb, zc, zd; double ra[3], rb[3], rc[3], rd[3];

            switch(K)
            {
                case 4:
                    zd = s.zz_[res[ik+12]];
                    rd[0] = s.rr_[res[ik+12]*3+0] + res[ik+9]*b[0] + res[ik+10]*b[3] + res[ik+11]*b[6];
                    rd[1] = s.rr_[res[ik+12]*3+1] + res[ik+9]*b[1] + res[ik+10]*b[4] + res[ik+11]*b[7];
                    rd[2] = s.rr_[res[ik+12]*3+2] + res[ik+9]*b[2] + res[ik+10]*b[5] + res[ik+11]*b[8];
                case 3:
                    zc = s.zz_[res[ik+8]];
                    rc[0] = s.rr_[res[ik+8]*3+0] + res[ik+5]*b[0] + res[ik+6]*b[3] + res[ik+7]*b[6];
                    rc[1] = s.rr_[res[ik+8]*3+1] + res[ik+5]*b[1] + res[ik+6]*b[4] + res[ik+7]*b[7];
                    rc[2] = s.rr_[res[ik+8]*3+2] + res[ik+5]*b[2] + res[ik+6]*b[5] + res[ik+7]*b[8];
                case 2:
                    zb = s.zz_[res[ik+4]];
                    rb[0] = s.rr_[res[ik+4]*3+0] + res[ik+1]*b[0] + res[ik+2]*b[3] + res[ik+3]*b[6];
                    rb[1] = s.rr_[res[ik+4]*3+1] + res[ik+1]*b[1] + res[ik+2]*b[4] + res[ik+3]*b[7];
                    rb[2] = s.rr_[res[ik+4]*3+2] + res[ik+1]*b[2] + res[ik+2]*b[5] + res[ik+3]*b[8];
                case 1:
                    za = s.zz_[res[ik+0]];
                    ra[0] = s.rr_[res[ik+0]*3+0];
                    ra[1] = s.rr_[res[ik+0]*3+1];
                    ra[2] = s.rr_[res[ik+0]*3+2];
            }
            switch(K)
            {
                case 1: logf(za, ra); break;
                case 2: logf(za, ra, zb, rb); break;
                case 3: logf(za, ra, zb, rb, zc, rc); break;
                case 4: logf(za, ra, zb, rb, zc, rc, zd, rd); break;
            }
        }
        else  // finite
            switch(K)
            {
                case 1: logf(s.zz_[res[ik+0]], &s.rr_[res[ik+0]*3]); break;
                case 2: logf(s.zz_[res[ik+0]], &s.rr_[res[ik+0]*3], s.zz_[res[ik+1]], &s.rr_[res[ik+1]*3]); break;
                case 3: logf(s.zz_[res[ik+0]], &s.rr_[res[ik+0]*3], s.zz_[res[ik+1]], &s.rr_[res[ik+1]*3], s.zz_[res[ik+2]], &s.rr_[res[ik+2]*3]); break;
                case 4: logf(s.zz_[res[ik+0]], &s.rr_[res[ik+0]*3], s.zz_[res[ik+1]], &s.rr_[res[ik+1]*3], s.zz_[res[ik+2]], &s.rr_[res[ik+2]*3], s.zz_[res[ik+3]], &s.rr_[res[ik+3]*3]); break;
            }
    }

    // compare generated atom sequences
    CHECK( log == reslog );
}

TEST_CASE("many-body_tensor_representation-atom_indexing_functions_finite", "[representations],[many-body_tensor]")
{
    using std::vector;
    const auto H = FiniteTestSystems::H; const auto H3 = FiniteTestSystems::H3; const auto O2 = FiniteTestSystems::O2; const auto HCl = FiniteTestSystems::HCl; const auto H2O = FiniteTestSystems::H2O; const auto C6H6 = FiniteTestSystems::C6H6; const auto HCN = FiniteTestSystems::HCN; const auto HCNO = FiniteTestSystems::HCNO; const auto CHNOS = FiniteTestSystems::CHNOS; const auto EMPTY = FiniteTestSystems::EMPTY; const auto H4 = FiniteTestSystems::H4; const auto SF6 = FiniteTestSystems::SF6;

    SECTION( "mbtr_aindexf_finite_full" )
    {
        // Special case: empty system
        {
            FiniteTestSystems s(EMPTY);
            vector<long> res = {};

            aindexf_test_indices<1, mbtr_aindexf_finite_full<long>>(s, res);
            aindexf_test_indices<2, mbtr_aindexf_finite_full<long>>(s, res);
            aindexf_test_indices<3, mbtr_aindexf_finite_full<long>>(s, res);
            aindexf_test_indices<4, mbtr_aindexf_finite_full<long>>(s, res);
        }

        // Special case: na < K = 2
        {
            FiniteTestSystems s1(EMPTY);
            FiniteTestSystems s2(H);
            vector<long> res = {};

            aindexf_test_indices<2, mbtr_aindexf_finite_full<long>>(s1, res);
            aindexf_test_indices<2, mbtr_aindexf_finite_full<long>>(s2, res);
        }

        // Special case: na < K = 3
        {
            FiniteTestSystems s1(EMPTY);
            FiniteTestSystems s2(H);
            FiniteTestSystems s3(O2);
            vector<long> res = {};

            aindexf_test_indices<3, mbtr_aindexf_finite_full<long>>(s1, res);
            aindexf_test_indices<3, mbtr_aindexf_finite_full<long>>(s2, res);
            aindexf_test_indices<3, mbtr_aindexf_finite_full<long>>(s3, res);
        }

        // Special case: na < K = 4
        {
            FiniteTestSystems s1(EMPTY);
            FiniteTestSystems s2(H);
            FiniteTestSystems s3(O2);
            FiniteTestSystems s4(H2O);
            vector<long> res = {};

            aindexf_test_indices<4, mbtr_aindexf_finite_full<long>>(s1, res);
            aindexf_test_indices<4, mbtr_aindexf_finite_full<long>>(s2, res);
            aindexf_test_indices<4, mbtr_aindexf_finite_full<long>>(s3, res);
            aindexf_test_indices<4, mbtr_aindexf_finite_full<long>>(s4, res);
        }

        // H3 - k = 1, n = 3
        {
            FiniteTestSystems s(H3);
            vector<long> res = { 0, 1, 2 };
            aindexf_test_indices<1, mbtr_aindexf_finite_full<long>>(s, res);
        }

        // HCN - k = 2, n = 3
        // -> Mathematica notebook
        {
            FiniteTestSystems s(HCN);
            vector<long> res = { 0,1,  0,2,  1,0,  1,2,  2,0,  2,1 };
            aindexf_test_indices<2, mbtr_aindexf_finite_full<long>>(s, res);
        }

        // HCN - k = 3, n = 3
        {
            FiniteTestSystems s(HCN);
            vector<long> res = { 0,1,2,  0,2,1,  1,0,2,  1,2,0,  2,0,1,  2,1,0 };
            aindexf_test_indices<3, mbtr_aindexf_finite_full<long>>(s, res);
        }

        // HCNO - k = 3, n = 4
        // -> Mathematica notebook
        {
            FiniteTestSystems s(HCNO);
            vector<long> res = {
                0,1,2,  0,1,3,  0,2,1,  0,2,3,  0,3,1,  0,3,2,
                1,0,2,  1,0,3,  1,2,0,  1,2,3,  1,3,0,  1,3,2,
                2,0,1,  2,0,3,  2,1,0,  2,1,3,  2,3,0,  2,3,1,
                3,0,1,  3,0,2,  3,1,0,  3,1,2,  3,2,0,  3,2,1
            };
            aindexf_test_indices<3, mbtr_aindexf_finite_full<long>>(s, res);
        }

        // HCNO - k = 4, n = 4
        // -> Mathematica notebook
        {
            FiniteTestSystems s(HCNO);
            vector<long> res = {
                0,1,2,3,  0,1,3,2,  0,2,1,3,  0,2,3,1,  0,3,1,2,  0,3,2,1,
                1,0,2,3,  1,0,3,2,  1,2,0,3,  1,2,3,0,  1,3,0,2,  1,3,2,0,
                2,0,1,3,  2,0,3,1,  2,1,0,3,  2,1,3,0,  2,3,0,1,  2,3,1,0,
                3,0,1,2,  3,0,2,1,  3,1,0,2,  3,1,2,0,  3,2,0,1,  3,2,1,0
            };
            aindexf_test_indices<4, mbtr_aindexf_finite_full<long>>(s, res);
        }

        // CHNOS - k = 4, n = 5
        // -> Mathematica notebook
        {
            FiniteTestSystems s(CHNOS);
            vector<long> res = {
                0,1,2,3,  0,1,2,4,  0,1,3,2,  0,1,3,4,  0,1,4,2,  0,1,4,3,   0,2,1,3,  0,2,1,4,  0,2,3,1,  0,2,3,4,  0,2,4,1,  0,2,4,3,   0,3,1,2,  0,3,1,4,  0,3,2,1,  0,3,2,4,  0,3,4,1,  0,3,4,2,   0,4,1,2,  0,4,1,3,  0,4,2,1,  0,4,2,3,  0,4,3,1,  0,4,3,2,
                1,0,2,3,  1,0,2,4,  1,0,3,2,  1,0,3,4,  1,0,4,2,  1,0,4,3,   1,2,0,3,  1,2,0,4,  1,2,3,0,  1,2,3,4,  1,2,4,0,  1,2,4,3,   1,3,0,2,  1,3,0,4,  1,3,2,0,  1,3,2,4,  1,3,4,0,  1,3,4,2,   1,4,0,2,  1,4,0,3,  1,4,2,0,  1,4,2,3,  1,4,3,0,  1,4,3,2,
                2,0,1,3,  2,0,1,4,  2,0,3,1,  2,0,3,4,  2,0,4,1,  2,0,4,3,   2,1,0,3,  2,1,0,4,  2,1,3,0,  2,1,3,4,  2,1,4,0,  2,1,4,3,   2,3,0,1,  2,3,0,4,  2,3,1,0,  2,3,1,4,  2,3,4,0,  2,3,4,1,   2,4,0,1,  2,4,0,3,  2,4,1,0,  2,4,1,3,  2,4,3,0,  2,4,3,1,
                3,0,1,2,  3,0,1,4,  3,0,2,1,  3,0,2,4,  3,0,4,1,  3,0,4,2,   3,1,0,2,  3,1,0,4,  3,1,2,0,  3,1,2,4,  3,1,4,0,  3,1,4,2,   3,2,0,1,  3,2,0,4,  3,2,1,0,  3,2,1,4,  3,2,4,0,  3,2,4,1,   3,4,0,1,  3,4,0,2,  3,4,1,0,  3,4,1,2,  3,4,2,0,  3,4,2,1,
                4,0,1,2,  4,0,1,3,  4,0,2,1,  4,0,2,3,  4,0,3,1,  4,0,3,2,   4,1,0,2,  4,1,0,3,  4,1,2,0,  4,1,2,3,  4,1,3,0,  4,1,3,2,   4,2,0,1,  4,2,0,3,  4,2,1,0,  4,2,1,3,  4,2,3,0,  4,2,3,1,   4,3,0,1,  4,3,0,2,  4,3,1,0,  4,3,1,2,  4,3,2,0,  4,3,2,1
            };
            aindexf_test_indices<4, mbtr_aindexf_finite_full<long>>(s, res);
        }
    }

    SECTION( "mbtr_aindexf_finite_noreversals" )
    {
        // Special case: empty system
        {
            FiniteTestSystems s(EMPTY);
            vector<long> res = {};

            aindexf_test_indices<1, mbtr_aindexf_finite_noreversals<long>>(s, res);
            aindexf_test_indices<2, mbtr_aindexf_finite_noreversals<long>>(s, res);
            aindexf_test_indices<3, mbtr_aindexf_finite_noreversals<long>>(s, res);
            aindexf_test_indices<4, mbtr_aindexf_finite_noreversals<long>>(s, res);
        }

        // Special case: na < K = 2
        {
            FiniteTestSystems s1(EMPTY);
            FiniteTestSystems s2(H);
            vector<long> res = {};

            aindexf_test_indices<2, mbtr_aindexf_finite_noreversals<long>>(s1, res);
            aindexf_test_indices<2, mbtr_aindexf_finite_noreversals<long>>(s2, res);
        }

        // Special case: na < K = 3
        {
            FiniteTestSystems s1(EMPTY);
            FiniteTestSystems s2(H);
            FiniteTestSystems s3(O2);
            vector<long> res = {};

            aindexf_test_indices<3, mbtr_aindexf_finite_noreversals<long>>(s1, res);
            aindexf_test_indices<3, mbtr_aindexf_finite_noreversals<long>>(s2, res);
            aindexf_test_indices<3, mbtr_aindexf_finite_noreversals<long>>(s3, res);
        }

        // Special case: na < K = 4
        {
            FiniteTestSystems s1(EMPTY);
            FiniteTestSystems s2(H);
            FiniteTestSystems s3(O2);
            FiniteTestSystems s4(H2O);
            vector<long> res = {};

            aindexf_test_indices<4, mbtr_aindexf_finite_noreversals<long>>(s1, res);
            aindexf_test_indices<4, mbtr_aindexf_finite_noreversals<long>>(s2, res);
            aindexf_test_indices<4, mbtr_aindexf_finite_noreversals<long>>(s3, res);
            aindexf_test_indices<4, mbtr_aindexf_finite_noreversals<long>>(s4, res);
        }

        // H3 - k = 1, n = 3
        {
            FiniteTestSystems s(H3);
            vector<long> res = { 0, 1, 2 };
            aindexf_test_indices<1, mbtr_aindexf_finite_noreversals<long>>(s, res);
        }

        // HCN - k = 2, n = 3
        // -> Mathematica notebook
        {
            FiniteTestSystems s(HCN);
            vector<long> res = { 0, 1,  0, 2,  1, 2 };
            aindexf_test_indices<2, mbtr_aindexf_finite_noreversals<long>>(s, res);
        }

        // HCN - k = 3, n = 3
        {
            FiniteTestSystems s(HCN);
            vector<long> res = { 0, 1, 2,  0, 2, 1,  1, 0, 2 };
            aindexf_test_indices<3, mbtr_aindexf_finite_noreversals<long>>(s, res);
        }

        // HCNO - k = 3, n = 4
        // -> Mathematica notebook
        {
            FiniteTestSystems s(HCNO);
            vector<long> res = { 0,1,2,  0,1,3,  0,2,1,  0,2,3,   0,3,1,  0,3,2,  1,0,2,  1,0,3,  1,2,3,  1,3,2,  2,0,3,  2,1,3 };
            aindexf_test_indices<3, mbtr_aindexf_finite_noreversals<long>>(s, res);
        }

        // HCNO - k = 4, n = 4
        // -> Mathematica notebook
        {
            FiniteTestSystems s(HCNO);
            vector<long> res = { 0,1,2,3,  0,1,3,2,  0,2,1,3,  0,2,3,1,  0,3,1,2,  0,3,2,1,  1,0,2,3,  1,0,3,2,  1,2,0,3,  1,3,0,2,  2,0,1,3,  2,1,0,3 };
            aindexf_test_indices<4, mbtr_aindexf_finite_noreversals<long>>(s, res);
        }

        // CHNOS - k = 4, n = 5
        // -> Mathematica notebook
        {
            FiniteTestSystems s(CHNOS);
            vector<long> res = {
                0, 1, 2, 3,  0, 1, 2, 4,  0, 1, 3, 2,  0, 1, 3, 4,  0, 1, 4, 2,  0, 1, 4, 3,  0, 2, 1, 3,  0, 2, 1, 4,  0, 2, 3, 1,  0, 2, 3, 4,
                0, 2, 4, 1,  0, 2, 4, 3,  0, 3, 1, 2,  0, 3, 1, 4,  0, 3, 2, 1,  0, 3, 2, 4,  0, 3, 4, 1,  0, 3, 4, 2,  0, 4, 1, 2,  0, 4, 1, 3,
                0, 4, 2, 1,  0, 4, 2, 3,  0, 4, 3, 1,  0, 4, 3, 2,  1, 0, 2, 3,  1, 0, 2, 4,  1, 0, 3, 2,  1, 0, 3, 4,  1, 0, 4, 2,  1, 0, 4, 3,
                1, 2, 0, 3,  1, 2, 0, 4,  1, 2, 3, 4,  1, 2, 4, 3,  1, 3, 0, 2,  1, 3, 0, 4,  1, 3, 2, 4,  1, 3, 4, 2,  1, 4, 0, 2,  1, 4, 0, 3,
                1, 4, 2, 3,  1, 4, 3, 2,  2, 0, 1, 3,  2, 0, 1, 4,  2, 0, 3, 4,  2, 0, 4, 3,  2, 1, 0, 3,  2, 1, 0, 4,  2, 1, 3, 4,  2, 1, 4, 3,
                2, 3, 0, 4,  2, 3, 1, 4,  2, 4, 0, 3,  2, 4, 1, 3,  3, 0, 1, 4,  3, 0, 2, 4,  3, 1, 0, 4,  3, 1, 2, 4,  3, 2, 0, 4,  3, 2, 1, 4
            };
            aindexf_test_indices<4, mbtr_aindexf_finite_noreversals<long>>(s, res);
        }
    }
}

TEST_CASE("many-body_tensor_representation-atom_indexing_functions_periodic", "[representations],[many-body_tensor]")
{
    SECTION( "unit cell replica full iterator: small examples" )
    {
        // center-only activity
        {
            std::vector<long> lhs; long safetyguard = 0;
            for(mbtr_replica_iterator i; i;)
            {
                ++safetyguard; if(safetyguard >= 100) break;
                lhs.insert( lhs.end(), { i.x(), i.y(), i.z() } );
                i.step( i.is_zero() );
            }
            std::vector<long> rhs { 0,0,0,   0,0,-1,   0,0,1,   0,-1,0,   0,1,0,   -1,0,0,  1,0,0 };
            CHECK( lhs == rhs );
        }

        // 3d cross
        {
            std::vector<long> lhs; long safetyguard = 0;
            for(mbtr_replica_iterator i; i;)
            {
                ++safetyguard; if(safetyguard >= 100) break;
                lhs.insert( lhs.end(), { i.x(), i.y(), i.z() } );
                i.step( std::abs(i.x()) + std::abs(i.y()) + std::abs(i.z()) <= 1 );
            }
            std::vector<long> rhs
            {
                 0,0,0,   0,0,-1,   0,0,-2,   0, 0,1,   0,0,2,   0,-1,0, 0,-1,-1,  0,-1, 1,  0,-2,0,  0, 1,0,  0,1,-1,  0,1,1,  0,2,0,
                -1,0,0,  -1,0,-1,  -1,0, 1,  -1,-1,0,  -1,1,0,  -2, 0,0, 1, 0, 0,  1, 0,-1,  1, 0,1,  1,-1,0,  1,1, 0,  2,0,0
            };
            CHECK( lhs == rhs );
        }

        // 3d star
        {
            std::vector<long> lhs; long safetyguard = 0;
            for(mbtr_replica_iterator i; i;)
            {
                ++safetyguard; if(safetyguard >= 100) break;
                lhs.insert( lhs.end(), { i.x(), i.y(), i.z() } );
                i.step( std::abs(i.x()) + std::abs(i.y()) + std::abs(i.z()) <= 2 );
            }
            std::vector<long> rhs
            {
                +0,+0,+0,  +0,+0,-1,  +0,+0,-2,  +0,+0,-3,  +0,+0,+1,  +0,+0,+2,  +0,+0,+3,
                +0,-1,+0,  +0,-1,-1,  +0,-1,-2,  +0,-1,+1,  +0,-1,+2,
                +0,-2,+0,  +0,-2,-1,  +0,-2,+1,
                +0,-3,+0,
                +0,+1,+0,  +0,+1,-1,  +0,+1,-2,  +0,+1,+1,  +0,+1,+2,
                +0,+2,+0,  +0,+2,-1,  +0,+2,+1,
                +0,+3,+0,
                -1,+0,+0,  -1,+0,-1,  -1,+0,-2,  -1,+0,+1,  -1,+0,+2,
                -1,-1,+0,  -1,-1,-1,  -1,-1,+1,  -1,-2,+0,
                -1,+1,+0,  -1,+1,-1,  -1,+1,+1,  -1,+2,+0,
                -2,+0,+0,  -2,+0,-1,  -2,+0,+1,  -2,-1,+0,  -2,+1,+0,  -3,+0,+0,
                +1,+0,+0,  +1,+0,-1,  +1,+0,-2,  +1,+0,+1,  +1,+0,+2,
                +1,-1,+0,  +1,-1,-1,  +1,-1,+1,  +1,-2,+0,
                +1,+1,+0,  +1,+1,-1,  +1,+1,+1,  +1,+2,+0,
                +2,+0,+0,  +2,+0,-1,  +2,+0,+1,  +2,-1,+0,  +2,+1,+0,  +3,+0,+0
            };
            CHECK( lhs == rhs );
        }

        // yz-dagger
        {
            std::vector<long> lhs; long safetyguard = 0;
            for(mbtr_replica_iterator i; i;)
            {
                ++safetyguard; if(safetyguard >= 100) break;
                lhs.insert( lhs.end(), { i.x(), i.y(), i.z() } );
                i.step( i.x() == 0 && ( (i.y() == 0 && i.z() >= -1 && i.z() <= 2) || (std::abs(i.y()) == 1 && i.z() == 0) ) );
            }
            std::vector<long> rhs
            {
                +0,+0,+0,  +0,+0,-1,  +0,+0,-2,  +0,+0,+1,  +0,+0,+2,  +0,+0,+3,
                +0,-1,+0,  +0,-1,-1,  +0,-1,+1,  +0,-2,+0,
                +0,+1,+0,  +0,+1,-1,  +0,+1,+1,  +0,+2,+0,
                -1,+0,+0,  +1,+0,+0
            };
            CHECK( lhs == rhs );
        }

        // xz-dagger
        {
            std::vector<long> lhs; long safetyguard = 0;
            for(mbtr_replica_iterator i; i;)
            {
                ++safetyguard; if(safetyguard >= 100) break;
                lhs.insert( lhs.end(), { i.x(), i.y(), i.z() } );
                i.step( i.y() == 0 && ( (i.x() == 0 && i.z() >= -1 && i.z() <= 2) || (std::abs(i.x()) == 1 && i.z() == 0) ) );
            }
            std::vector<long> rhs
            {
                +0,+0,+0,  +0,+0,-1,  +0,+0,-2,  +0,+0,+1,  +0,+0,+2,  +0,+0,+3,
                +0,-1,+0,  +0,+1,+0,
                -1,+0,+0,  -1,+0,-1,  -1,+0,+1,  -1,-1,+0,  -1,+1,+0,  -2,+0,+0,
                +1,+0,+0,  +1,+0,-1,  +1,+0,+1,  +1,-1,+0,  +1,+1,+0,  +2,+0,+0
            };
            CHECK( lhs == rhs );
        }

        // yx-dagger
        {
            std::vector<long> lhs; long safetyguard = 0;
            for(mbtr_replica_iterator i; i;)
            {
                ++safetyguard; if(safetyguard >= 100) break;
                lhs.insert( lhs.end(), { i.x(), i.y(), i.z() } );
                i.step( i.z() == 0 && ( (i.y() == 0 && i.x() >= -1 && i.x() <= 2) || (std::abs(i.y()) == 1 && i.x() == 0) ) );
            }
            std::vector<long> rhs
            {
                +0,+0,+0,  +0,+0,-1,  +0,+0,+1,  +0,-1,+0,  +0,-1,-1,  +0,-1,+1,  +0,-2,+0,  +0,+1,+0,  +0,+1,-1,  +0,+1,+1,  +0,+2,+0,
                -1,+0,+0,  -1,+0,-1,  -1,+0,+1,  -1,-1,+0,  -1,+1,+0,  -2,+0,+0,
                +1,+0,+0,  +1,+0,-1,  +1,+0,+1,  +1,-1,+0,  +1,+1,+0,  +2,+0,+0,  +2,+0,-1,  +2,+0,+1,  +2,-1,+0,  +2,+1,+0,  +3,+0,+0
            };
            CHECK( lhs == rhs );
        }
    }

    using std::vector;
    auto const NaCl = PeriodicTestSystems::NaCl;
    auto const Al   = PeriodicTestSystems::Al;
    auto const SF6  = PeriodicTestSystems::SF6;
    auto const M1   = PeriodicTestSystems::M1;
    auto const M2   = PeriodicTestSystems::M2;
    auto const M3   = PeriodicTestSystems::M3;
    auto const M4   = PeriodicTestSystems::M4;

    SECTION( "mbtr_aindexf_periodic_full" )
    {
        // NaCl - k = 1
        {
            PeriodicTestSystems s(NaCl);
            vector<long> res = { 0, 1 };
            aindexf_test_indices<1, mbtr_aindexf_periodic_full<long>>(s, res, 999);
        }

        // SF6 - k = 1
        {
            PeriodicTestSystems s(SF6);
            vector<long> res = { 0, 1, 2, 3, 4, 5, 6 };
            aindexf_test_indices<1, mbtr_aindexf_periodic_full<long>>(s, res, 999);
        }

        // Al - first neighbors, k = 2
        // -> Mathematica notebook
        {
            PeriodicTestSystems s(Al);
            vector<long> res =
            {
                // Replica indices (not Cartesian space) of atoms within 2.86 Angstrom
                // {-1,0,0}, {-1,0, 1}, {-1,1,0}, {0,-1,0}, {0,-1, 1}, {0,0,-1}, {0.,0.,0.},
                // { 0,0,1}, { 0,1,-1}, { 0,1,0}, {1,-1,0}, {1, 0,-1}, {1,0, 0}
                0, +0,+0,-1, 0,   0, +0,+0,-2, 0,    0, +0,+0,+1, 0,   0, +0,+0,+2, 0,   0, +0,-1,+0, 0,   0, +0,-1,-1, 0,   0, +0,-1,+1, 0,
                0, +0,-1,+2, 0,   0, +0,-2,+0, 0,    0, +0,+1,+0, 0,   0, +0,+1,-1, 0,   0, +0,+1,-2, 0,   0, +0,+1,+1, 0,   0, +0,+2,+0, 0,
                0, -1,+0,+0, 0,   0, -1,+0,-1, 0,    0, -1,+0,+1, 0,   0, -1,+0,+2, 0,   0, -1,-1,+0, 0,   0, -1,+1,+0, 0,   0, -1,+1,-1, 0,
                0, -1,+1,+1, 0,   0, -1,+2,+0, 0,    0, -2,+0,+0, 0,   0, +1,+0,+0, 0,   0, +1,+0,-1, 0,   0, +1,+0,-2, 0,   0, +1,+0,+1, 0,
                0, +1,-1,+0, 0,   0, +1,-1,-1, 0,    0, +1,-1,+1, 0,   0, +1,-2,+0, 0,   0, +1,+1,+0, 0,   0, +2,+0,+0, 0
            };

            aindexf_test_indices<2, mbtr_aindexf_periodic_full<long>>(s, res, 2.863428910414925);
        }

        // NaCl - first neighbors, k = 2
        // -> Mathematica notebook
        {
            PeriodicTestSystems s(NaCl);
            vector<long> res = {
                // Replica indices (not Cartesian space) of atoms within 2.83 Angstrom
                // {{11, 0, 0, 0}, {17, -1, -1, 0}, {17, -1, 0, -1}, {17, -1, 0, 0}, {17, 0, -1, -1}, {17, 0, -1, 0}, {17, 0, 0, -1}}  Na
                // {{11, 0, 0, 1}, {11, 0, 1, 0}, {11, 0, 1, 1}, {11, 1, 0, 0}, {11, 1, 0, 1}, {11, 1, 1, 0}, {17, 0, 0, 0}}  Cl
                0, +0,+0,+0, 1,   0, +0,+0,-1, 0,   0, +0,+0,-1, 1,   0, +0,+0,-2, 0,   0, +0,+0,-2, 1,   0, +0,+0,+1, 0,   0, +0,+0,+1, 1,
                0, +0,-1,+0, 0,   0, +0,-1,+0, 1,   0, +0,-1,-1, 0,   0, +0,-1,-1, 1,   0, +0,-1,-2, 0,   0, +0,-1,-2, 1,   0, +0,-1,+1, 0,
                0, +0,-1,+1, 1,   0, +0,-2,+0, 0,   0, +0,-2,+0, 1,   0, +0,+1,+0, 0,   0, +0,+1,+0, 1,   0, -1,+0,+0, 0,   0, -1,+0,+0, 1,
                0, -1,+0,-1, 0,   0, -1,+0,-1, 1,   0, -1,+0,-2, 0,   0, -1,+0,-2, 1,   0, -1,+0,+1, 0,   0, -1,+0,+1, 1,   0, -1,-1,+0, 0,
                0, -1,-1,+0, 1,   0, -1,-1,-1, 0,   0, -1,-1,-1, 1,   0, -1,-1,+1, 0,   0, -1,-1,+1, 1,   0, -1,-2,+0, 0,   0, -1,-2,+0, 1,
                0, -1,+1,+0, 0,   0, -1,+1,+0, 1,   0, -2,+0,+0, 0,   0, -2,+0,+0, 1,   0, +1,+0,+0, 0,   0, +1,+0,+0, 1,   // 41
                1, +0,+0,+0, 0,   1, +0,+0,-1, 0,   1, +0,+0,-1, 1,   1, +0,+0,+1, 0,   1, +0,+0,+1, 1,   1, +0,+0,+2, 0,   1, +0,+0,+2, 1,
                1, +0,-1,+0, 0,   1, +0,-1,+0, 1,   1, +0,+1,+0, 0,   1, +0,+1,+0, 1,   1, +0,+1,-1, 0,   1, +0,+1,-1, 1,   1, +0,+1,+1, 0,
                1, +0,+1,+1, 1,   1, +0,+1,+2, 0,   1, +0,+1,+2, 1,   1, +0,+2,+0, 0,   1, +0,+2,+0, 1,   1, -1,+0,+0, 0,   1, -1,+0,+0, 1,
                1, +1,+0,+0, 0,   1, +1,+0,+0, 1,   1, +1,+0,-1, 0,   1, +1,+0,-1, 1,   1, +1,+0,+1, 0,   1, +1,+0,+1, 1,   1, +1,+0,+2, 0,
                1, +1,+0,+2, 1,   1, +1,-1,+0, 0,   1, +1,-1,+0, 1,   1, +1,+1,+0, 0,   1, +1,+1,+0, 1,   1, +1,+1,-1, 0,   1, +1,+1,-1, 1,
                1, +1,+1,+1, 0,   1, +1,+1,+1, 1,   1, +1,+2,+0, 0,   1, +1,+2,+0, 1,   1, +2,+0,+0, 0,   1, +2,+0,+0, 1
            };
            aindexf_test_indices<2, mbtr_aindexf_periodic_full<long>>(s, res, 2.83);
        }

        // Al - first neighbors, k = 3
        // -> Mathematica notebook
        {
            PeriodicTestSystems s(Al);
            vector<long> res =
            {
                0, +0,+0,-1, 0, +0,+0,-2, 0,    0, +0,+0,-1, 0, +0,+0,+1, 0,    0, +0,+0,-1, 0, +0,+0,+2, 0,    0, +0,+0,-1, 0, +0,-1,+0, 0,
                0, +0,+0,-1, 0, +0,-1,-1, 0,    0, +0,+0,-1, 0, +0,-1,+1, 0,    0, +0,+0,-1, 0, +0,-1,+2, 0,    0, +0,+0,-1, 0, +0,-2,+0, 0,
                0, +0,+0,-1, 0, +0,+1,+0, 0,    0, +0,+0,-1, 0, +0,+1,-1, 0,    0, +0,+0,-1, 0, +0,+1,-2, 0,    0, +0,+0,-1, 0, +0,+1,+1, 0,
                0, +0,+0,-1, 0, +0,+2,+0, 0,    0, +0,+0,-1, 0, -1,+0,+0, 0,    0, +0,+0,-1, 0, -1,+0,-1, 0,    0, +0,+0,-1, 0, -1,+0,+1, 0,
                0, +0,+0,-1, 0, -1,+0,+2, 0,    0, +0,+0,-1, 0, -1,-1,+0, 0,    0, +0,+0,-1, 0, -1,+1,+0, 0,    0, +0,+0,-1, 0, -1,+1,-1, 0,
                0, +0,+0,-1, 0, -1,+1,+1, 0,    0, +0,+0,-1, 0, -1,+2,+0, 0,    0, +0,+0,-1, 0, -2,+0,+0, 0,    0, +0,+0,-1, 0, +1,+0,+0, 0,
                0, +0,+0,-1, 0, +1,+0,-1, 0,    0, +0,+0,-1, 0, +1,+0,-2, 0,    0, +0,+0,-1, 0, +1,+0,+1, 0,    0, +0,+0,-1, 0, +1,-1,+0, 0,
                0, +0,+0,-1, 0, +1,-1,-1, 0,    0, +0,+0,-1, 0, +1,-1,+1, 0,    0, +0,+0,-1, 0, +1,-2,+0, 0,    0, +0,+0,-1, 0, +1,+1,+0, 0,
                0, +0,+0,-1, 0, +2,+0,+0, 0,    // 33; iteration of cc,c done
                0, +0,+0,-2, 0, +0,+0,-1, 0,    0, +0,+0,-2, 0, +0,+0,+1, 0,    0, +0,+0,-2, 0, +0,-1,+0, 0,    0, +0,+0,-2, 0, +0,+1,+0, 0,
                0, +0,+0,-2, 0, -1,+0,+0, 0,    0, +0,+0,-2, 0, +1,+0,+0, 0,  // 39; bb = 0, 0, -2 done
                0, +0,+0,+1, 0, +0,+0,-1, 0,    0, +0,+0,+1, 0, +0,+0,-2, 0,    0, +0,+0,+1, 0, +0,+0,+2, 0,    0, +0,+0,+1, 0, +0,-1,+0, 0,
                0, +0,+0,+1, 0, +0,-1,-1, 0,    0, +0,+0,+1, 0, +0,-1,+1, 0,    0, +0,+0,+1, 0, +0,-1,+2, 0,    0, +0,+0,+1, 0, +0,-2,+0, 0,
                0, +0,+0,+1, 0, +0,+1,+0, 0,    0, +0,+0,+1, 0, +0,+1,-1, 0,    0, +0,+0,+1, 0, +0,+1,-2, 0,    0, +0,+0,+1, 0, +0,+1,+1, 0,
                0, +0,+0,+1, 0, +0,+2,+0, 0,    0, +0,+0,+1, 0, -1,+0,+0, 0,    0, +0,+0,+1, 0, -1,+0,-1, 0,    0, +0,+0,+1, 0, -1,+0,+1, 0,
                0, +0,+0,+1, 0, -1,+0,+2, 0,    0, +0,+0,+1, 0, -1,-1,+0, 0,    0, +0,+0,+1, 0, -1,+1,+0, 0,    0, +0,+0,+1, 0, -1,+1,-1, 0,
                0, +0,+0,+1, 0, -1,+1,+1, 0,    0, +0,+0,+1, 0, -1,+2,+0, 0,    0, +0,+0,+1, 0, -2,+0,+0, 0,    0, +0,+0,+1, 0, +1,+0,+0, 0,
                0, +0,+0,+1, 0, +1,+0,-1, 0,    0, +0,+0,+1, 0, +1,+0,-2, 0,    0, +0,+0,+1, 0, +1,+0,+1, 0,    0, +0,+0,+1, 0, +1,-1,+0, 0,
                0, +0,+0,+1, 0, +1,-1,-1, 0,    0, +0,+0,+1, 0, +1,-1,+1, 0,    0, +0,+0,+1, 0, +1,-2,+0, 0,    0, +0,+0,+1, 0, +1,+1,+0, 0,
                0, +0,+0,+1, 0, +2,+0,+0, 0,    // 72; iteration of cc,c done
                0, +0,+0,+2, 0, +0,+0,-1, 0,    0, +0,+0,+2, 0, +0,+0,+1, 0,    0, +0,+0,+2, 0, +0,-1,+0, 0,    0, +0,+0,+2, 0, +0,+1,+0, 0,
                0, +0,+0,+2, 0, -1,+0,+0, 0,    0, +0,+0,+2, 0, +1,+0,+0, 0,    // 78; iteration of cc,c done
                0, +0,-1,+0, 0, +0,+0,-1, 0,    0, +0,-1,+0, 0, +0,+0,-2, 0,    0, +0,-1,+0, 0, +0,+0,+1, 0,    0, +0,-1,+0, 0, +0,+0,+2, 0,
                0, +0,-1,+0, 0, +0,-1,-1, 0,    0, +0,-1,+0, 0, +0,-1,+1, 0,    0, +0,-1,+0, 0, +0,-1,+2, 0,    0, +0,-1,+0, 0, +0,-2,+0, 0,
                0, +0,-1,+0, 0, +0,+1,+0, 0,    0, +0,-1,+0, 0, +0,+1,-1, 0,    0, +0,-1,+0, 0, +0,+1,-2, 0,    0, +0,-1,+0, 0, +0,+1,+1, 0,
                0, +0,-1,+0, 0, +0,+2,+0, 0,    0, +0,-1,+0, 0, -1,+0,+0, 0,    0, +0,-1,+0, 0, -1,+0,-1, 0,    0, +0,-1,+0, 0, -1,+0,+1, 0,
                0, +0,-1,+0, 0, -1,+0,+2, 0,    0, +0,-1,+0, 0, -1,-1,+0, 0,    0, +0,-1,+0, 0, -1,+1,+0, 0,    0, +0,-1,+0, 0, -1,+1,-1, 0,
                0, +0,-1,+0, 0, -1,+1,+1, 0,    0, +0,-1,+0, 0, -1,+2,+0, 0,    0, +0,-1,+0, 0, -2,+0,+0, 0,    0, +0,-1,+0, 0, +1,+0,+0, 0,
                0, +0,-1,+0, 0, +1,+0,-1, 0,    0, +0,-1,+0, 0, +1,+0,-2, 0,    0, +0,-1,+0, 0, +1,+0,+1, 0,    0, +0,-1,+0, 0, +1,-1,+0, 0,
                0, +0,-1,+0, 0, +1,-1,-1, 0,    0, +0,-1,+0, 0, +1,-1,+1, 0,    0, +0,-1,+0, 0, +1,-2,+0, 0,    0, +0,-1,+0, 0, +1,+1,+0, 0,
                0, +0,-1,+0, 0, +2,+0,+0, 0,    // 111; iteration of cc,c done
                0, +0,-1,-1, 0, +0,+0,-1, 0,    0, +0,-1,-1, 0, +0,+0,+1, 0,    0, +0,-1,-1, 0, +0,-1,+0, 0,    0, +0,-1,-1, 0, +0,+1,+0, 0,
                0, +0,-1,-1, 0, -1,+0,+0, 0,    0, +0,-1,-1, 0, +1,+0,+0, 0,    // 117; cc,c done
                0, +0,-1,+1, 0, +0,+0,-1, 0,    0, +0,-1,+1, 0, +0,+0,-2, 0,    0, +0,-1,+1, 0, +0,+0,+1, 0,    0, +0,-1,+1, 0, +0,+0,+2, 0,
                0, +0,-1,+1, 0, +0,-1,+0, 0,    0, +0,-1,+1, 0, +0,-1,-1, 0,    0, +0,-1,+1, 0, +0,-1,+2, 0,    0, +0,-1,+1, 0, +0,-2,+0, 0,
                0, +0,-1,+1, 0, +0,+1,+0, 0,    0, +0,-1,+1, 0, +0,+1,-1, 0,    0, +0,-1,+1, 0, +0,+1,-2, 0,    0, +0,-1,+1, 0, +0,+1,+1, 0, // 129
                0, +0,-1,+1, 0, +0,+2,+0, 0,     // 130; until here, verified manually. TODO: verify remaining index tuples

                // begin computer-generated sequence
                0, +0,-1,+1, 0, -1,+0,+0, 0,    0, +0,-1,+1, 0, -1,+0,-1, 0,    0, +0,-1,+1, 0, -1,+0,+1, 0,    0, +0,-1,+1, 0, -1,+0,+2, 0,
                0, +0,-1,+1, 0, -1,-1,+0, 0,    0, +0,-1,+1, 0, -1,+1,+0, 0,    0, +0,-1,+1, 0, -1,+1,-1, 0,    0, +0,-1,+1, 0, -1,+1,+1, 0,
                0, +0,-1,+1, 0, -1,+2,+0, 0,    0, +0,-1,+1, 0, -2,+0,+0, 0,    0, +0,-1,+1, 0, +1,+0,+0, 0,    0, +0,-1,+1, 0, +1,+0,-1, 0,
                0, +0,-1,+1, 0, +1,+0,-2, 0,    0, +0,-1,+1, 0, +1,+0,+1, 0,    0, +0,-1,+1, 0, +1,-1,+0, 0,    0, +0,-1,+1, 0, +1,-1,-1, 0,
                0, +0,-1,+1, 0, +1,-1,+1, 0,    0, +0,-1,+1, 0, +1,-2,+0, 0,    0, +0,-1,+1, 0, +1,+1,+0, 0,    0, +0,-1,+1, 0, +2,+0,+0, 0,
                0, +0,-1,+2, 0, +0,+0,-1, 0,    0, +0,-1,+2, 0, +0,+0,+1, 0,    0, +0,-1,+2, 0, +0,-1,+0, 0,    0, +0,-1,+2, 0, +0,+1,+0, 0,
                0, +0,-1,+2, 0, -1,+0,+0, 0,    0, +0,-1,+2, 0, +1,+0,+0, 0,    0, +0,-2,+0, 0, +0,+0,-1, 0,    0, +0,-2,+0, 0, +0,+0,+1, 0,
                0, +0,-2,+0, 0, +0,-1,+0, 0,    0, +0,-2,+0, 0, +0,+1,+0, 0,    0, +0,-2,+0, 0, -1,+0,+0, 0,    0, +0,-2,+0, 0, +1,+0,+0, 0,  // 162
                0, +0,+1,+0, 0, +0,+0,-1, 0,    0, +0,+1,+0, 0, +0,+0,-2, 0,    0, +0,+1,+0, 0, +0,+0,+1, 0,    0, +0,+1,+0, 0, +0,+0,+2, 0,
                0, +0,+1,+0, 0, +0,-1,+0, 0,    0, +0,+1,+0, 0, +0,-1,-1, 0,    0, +0,+1,+0, 0, +0,-1,+1, 0,    0, +0,+1,+0, 0, +0,-1,+2, 0,
                0, +0,+1,+0, 0, +0,-2,+0, 0,    0, +0,+1,+0, 0, +0,+1,-1, 0,    0, +0,+1,+0, 0, +0,+1,-2, 0,    0, +0,+1,+0, 0, +0,+1,+1, 0,
                0, +0,+1,+0, 0, +0,+2,+0, 0,    0, +0,+1,+0, 0, -1,+0,+0, 0,    0, +0,+1,+0, 0, -1,+0,-1, 0,    0, +0,+1,+0, 0, -1,+0,+1, 0,
                0, +0,+1,+0, 0, -1,+0,+2, 0,    0, +0,+1,+0, 0, -1,-1,+0, 0,    0, +0,+1,+0, 0, -1,+1,+0, 0,    0, +0,+1,+0, 0, -1,+1,-1, 0,
                0, +0,+1,+0, 0, -1,+1,+1, 0,    0, +0,+1,+0, 0, -1,+2,+0, 0,    0, +0,+1,+0, 0, -2,+0,+0, 0,    0, +0,+1,+0, 0, +1,+0,+0, 0,
                0, +0,+1,+0, 0, +1,+0,-1, 0,    0, +0,+1,+0, 0, +1,+0,-2, 0,    0, +0,+1,+0, 0, +1,+0,+1, 0,    0, +0,+1,+0, 0, +1,-1,+0, 0,
                0, +0,+1,+0, 0, +1,-1,-1, 0,    0, +0,+1,+0, 0, +1,-1,+1, 0,    0, +0,+1,+0, 0, +1,-2,+0, 0,    0, +0,+1,+0, 0, +1,+1,+0, 0,  // 194
                0, +0,+1,+0, 0, +2,+0,+0, 0,    0, +0,+1,-1, 0, +0,+0,-1, 0,    0, +0,+1,-1, 0, +0,+0,-2, 0,    0, +0,+1,-1, 0, +0,+0,+1, 0,
                0, +0,+1,-1, 0, +0,+0,+2, 0,    0, +0,+1,-1, 0, +0,-1,+0, 0,    0, +0,+1,-1, 0, +0,-1,-1, 0,    0, +0,+1,-1, 0, +0,-1,+1, 0,
                0, +0,+1,-1, 0, +0,-1,+2, 0,    0, +0,+1,-1, 0, +0,-2,+0, 0,    0, +0,+1,-1, 0, +0,+1,+0, 0,    0, +0,+1,-1, 0, +0,+1,-2, 0,
                0, +0,+1,-1, 0, +0,+1,+1, 0,    0, +0,+1,-1, 0, +0,+2,+0, 0,    0, +0,+1,-1, 0, -1,+0,+0, 0,    0, +0,+1,-1, 0, -1,+0,-1, 0,
                0, +0,+1,-1, 0, -1,+0,+1, 0,    0, +0,+1,-1, 0, -1,+0,+2, 0,    0, +0,+1,-1, 0, -1,-1,+0, 0,    0, +0,+1,-1, 0, -1,+1,+0, 0,
                0, +0,+1,-1, 0, -1,+1,-1, 0,    0, +0,+1,-1, 0, -1,+1,+1, 0,    0, +0,+1,-1, 0, -1,+2,+0, 0,    0, +0,+1,-1, 0, -2,+0,+0, 0,
                0, +0,+1,-1, 0, +1,+0,+0, 0,    0, +0,+1,-1, 0, +1,+0,-1, 0,    0, +0,+1,-1, 0, +1,+0,-2, 0,    0, +0,+1,-1, 0, +1,+0,+1, 0,
                0, +0,+1,-1, 0, +1,-1,+0, 0,    0, +0,+1,-1, 0, +1,-1,-1, 0,    0, +0,+1,-1, 0, +1,-1,+1, 0,    0, +0,+1,-1, 0, +1,-2,+0, 0,  // 226
                0, +0,+1,-1, 0, +1,+1,+0, 0,    0, +0,+1,-1, 0, +2,+0,+0, 0,    0, +0,+1,-2, 0, +0,+0,-1, 0,    0, +0,+1,-2, 0, +0,+0,+1, 0,
                0, +0,+1,-2, 0, +0,-1,+0, 0,    0, +0,+1,-2, 0, +0,+1,+0, 0,    0, +0,+1,-2, 0, -1,+0,+0, 0,    0, +0,+1,-2, 0, +1,+0,+0, 0,
                0, +0,+1,+1, 0, +0,+0,-1, 0,    0, +0,+1,+1, 0, +0,+0,+1, 0,    0, +0,+1,+1, 0, +0,-1,+0, 0,    0, +0,+1,+1, 0, +0,+1,+0, 0,
                0, +0,+1,+1, 0, -1,+0,+0, 0,    0, +0,+1,+1, 0, +1,+0,+0, 0,    0, +0,+2,+0, 0, +0,+0,-1, 0,    0, +0,+2,+0, 0, +0,+0,+1, 0,
                0, +0,+2,+0, 0, +0,-1,+0, 0,    0, +0,+2,+0, 0, +0,+1,+0, 0,    0, +0,+2,+0, 0, -1,+0,+0, 0,    0, +0,+2,+0, 0, +1,+0,+0, 0,
                0, -1,+0,+0, 0, +0,+0,-1, 0,    0, -1,+0,+0, 0, +0,+0,-2, 0,    0, -1,+0,+0, 0, +0,+0,+1, 0,    0, -1,+0,+0, 0, +0,+0,+2, 0,
                0, -1,+0,+0, 0, +0,-1,+0, 0,    0, -1,+0,+0, 0, +0,-1,-1, 0,    0, -1,+0,+0, 0, +0,-1,+1, 0,    0, -1,+0,+0, 0, +0,-1,+2, 0,
                0, -1,+0,+0, 0, +0,-2,+0, 0,    0, -1,+0,+0, 0, +0,+1,+0, 0,    0, -1,+0,+0, 0, +0,+1,-1, 0,    0, -1,+0,+0, 0, +0,+1,-2, 0,  // 258
                0, -1,+0,+0, 0, +0,+1,+1, 0,    0, -1,+0,+0, 0, +0,+2,+0, 0,    0, -1,+0,+0, 0, -1,+0,-1, 0,    0, -1,+0,+0, 0, -1,+0,+1, 0,
                0, -1,+0,+0, 0, -1,+0,+2, 0,    0, -1,+0,+0, 0, -1,-1,+0, 0,    0, -1,+0,+0, 0, -1,+1,+0, 0,    0, -1,+0,+0, 0, -1,+1,-1, 0,
                0, -1,+0,+0, 0, -1,+1,+1, 0,    0, -1,+0,+0, 0, -1,+2,+0, 0,    0, -1,+0,+0, 0, -2,+0,+0, 0,    0, -1,+0,+0, 0, +1,+0,+0, 0,
                0, -1,+0,+0, 0, +1,+0,-1, 0,    0, -1,+0,+0, 0, +1,+0,-2, 0,    0, -1,+0,+0, 0, +1,+0,+1, 0,    0, -1,+0,+0, 0, +1,-1,+0, 0,
                0, -1,+0,+0, 0, +1,-1,-1, 0,    0, -1,+0,+0, 0, +1,-1,+1, 0,    0, -1,+0,+0, 0, +1,-2,+0, 0,    0, -1,+0,+0, 0, +1,+1,+0, 0,
                0, -1,+0,+0, 0, +2,+0,+0, 0,    0, -1,+0,-1, 0, +0,+0,-1, 0,    0, -1,+0,-1, 0, +0,+0,+1, 0,    0, -1,+0,-1, 0, +0,-1,+0, 0,
                0, -1,+0,-1, 0, +0,+1,+0, 0,    0, -1,+0,-1, 0, -1,+0,+0, 0,    0, -1,+0,-1, 0, +1,+0,+0, 0,    0, -1,+0,+1, 0, +0,+0,-1, 0,
                0, -1,+0,+1, 0, +0,+0,-2, 0,    0, -1,+0,+1, 0, +0,+0,+1, 0,    0, -1,+0,+1, 0, +0,+0,+2, 0,    0, -1,+0,+1, 0, +0,-1,+0, 0,  // 290
                0, -1,+0,+1, 0, +0,-1,-1, 0,    0, -1,+0,+1, 0, +0,-1,+1, 0,    0, -1,+0,+1, 0, +0,-1,+2, 0,    0, -1,+0,+1, 0, +0,-2,+0, 0,
                0, -1,+0,+1, 0, +0,+1,+0, 0,    0, -1,+0,+1, 0, +0,+1,-1, 0,    0, -1,+0,+1, 0, +0,+1,-2, 0,    0, -1,+0,+1, 0, +0,+1,+1, 0,
                0, -1,+0,+1, 0, +0,+2,+0, 0,    0, -1,+0,+1, 0, -1,+0,+0, 0,    0, -1,+0,+1, 0, -1,+0,-1, 0,    0, -1,+0,+1, 0, -1,+0,+2, 0,
                0, -1,+0,+1, 0, -1,-1,+0, 0,    0, -1,+0,+1, 0, -1,+1,+0, 0,    0, -1,+0,+1, 0, -1,+1,-1, 0,    0, -1,+0,+1, 0, -1,+1,+1, 0,
                0, -1,+0,+1, 0, -1,+2,+0, 0,    0, -1,+0,+1, 0, -2,+0,+0, 0,    0, -1,+0,+1, 0, +1,+0,+0, 0,    0, -1,+0,+1, 0, +1,+0,-1, 0,
                0, -1,+0,+1, 0, +1,+0,-2, 0,    0, -1,+0,+1, 0, +1,+0,+1, 0,    0, -1,+0,+1, 0, +1,-1,+0, 0,    0, -1,+0,+1, 0, +1,-1,-1, 0,
                0, -1,+0,+1, 0, +1,-1,+1, 0,    0, -1,+0,+1, 0, +1,-2,+0, 0,    0, -1,+0,+1, 0, +1,+1,+0, 0,    0, -1,+0,+1, 0, +2,+0,+0, 0,
                0, -1,+0,+2, 0, +0,+0,-1, 0,    0, -1,+0,+2, 0, +0,+0,+1, 0,    0, -1,+0,+2, 0, +0,-1,+0, 0,    0, -1,+0,+2, 0, +0,+1,+0, 0,  // 322
                0, -1,+0,+2, 0, -1,+0,+0, 0,    0, -1,+0,+2, 0, +1,+0,+0, 0,    0, -1,-1,+0, 0, +0,+0,-1, 0,    0, -1,-1,+0, 0, +0,+0,+1, 0,
                0, -1,-1,+0, 0, +0,-1,+0, 0,    0, -1,-1,+0, 0, +0,+1,+0, 0,    0, -1,-1,+0, 0, -1,+0,+0, 0,    0, -1,-1,+0, 0, +1,+0,+0, 0,
                0, -1,+1,+0, 0, +0,+0,-1, 0,    0, -1,+1,+0, 0, +0,+0,-2, 0,    0, -1,+1,+0, 0, +0,+0,+1, 0,    0, -1,+1,+0, 0, +0,+0,+2, 0,
                0, -1,+1,+0, 0, +0,-1,+0, 0,    0, -1,+1,+0, 0, +0,-1,-1, 0,    0, -1,+1,+0, 0, +0,-1,+1, 0,    0, -1,+1,+0, 0, +0,-1,+2, 0,
                0, -1,+1,+0, 0, +0,-2,+0, 0,    0, -1,+1,+0, 0, +0,+1,+0, 0,    0, -1,+1,+0, 0, +0,+1,-1, 0,    0, -1,+1,+0, 0, +0,+1,-2, 0,
                0, -1,+1,+0, 0, +0,+1,+1, 0,    0, -1,+1,+0, 0, +0,+2,+0, 0,    0, -1,+1,+0, 0, -1,+0,+0, 0,    0, -1,+1,+0, 0, -1,+0,-1, 0,
                0, -1,+1,+0, 0, -1,+0,+1, 0,    0, -1,+1,+0, 0, -1,+0,+2, 0,    0, -1,+1,+0, 0, -1,-1,+0, 0,    0, -1,+1,+0, 0, -1,+1,-1, 0,
                0, -1,+1,+0, 0, -1,+1,+1, 0,    0, -1,+1,+0, 0, -1,+2,+0, 0,    0, -1,+1,+0, 0, -2,+0,+0, 0,    0, -1,+1,+0, 0, +1,+0,+0, 0,  // 354
                0, -1,+1,+0, 0, +1,+0,-1, 0,    0, -1,+1,+0, 0, +1,+0,-2, 0,    0, -1,+1,+0, 0, +1,+0,+1, 0,    0, -1,+1,+0, 0, +1,-1,+0, 0,
                0, -1,+1,+0, 0, +1,-1,-1, 0,    0, -1,+1,+0, 0, +1,-1,+1, 0,    0, -1,+1,+0, 0, +1,-2,+0, 0,    0, -1,+1,+0, 0, +1,+1,+0, 0,
                0, -1,+1,+0, 0, +2,+0,+0, 0,    0, -1,+1,-1, 0, +0,+0,-1, 0,    0, -1,+1,-1, 0, +0,+0,+1, 0,    0, -1,+1,-1, 0, +0,-1,+0, 0,
                0, -1,+1,-1, 0, +0,+1,+0, 0,    0, -1,+1,-1, 0, -1,+0,+0, 0,    0, -1,+1,-1, 0, +1,+0,+0, 0,    0, -1,+1,+1, 0, +0,+0,-1, 0,
                0, -1,+1,+1, 0, +0,+0,+1, 0,    0, -1,+1,+1, 0, +0,-1,+0, 0,    0, -1,+1,+1, 0, +0,+1,+0, 0,    0, -1,+1,+1, 0, -1,+0,+0, 0,
                0, -1,+1,+1, 0, +1,+0,+0, 0,    0, -1,+2,+0, 0, +0,+0,-1, 0,    0, -1,+2,+0, 0, +0,+0,+1, 0,    0, -1,+2,+0, 0, +0,-1,+0, 0,
                0, -1,+2,+0, 0, +0,+1,+0, 0,    0, -1,+2,+0, 0, -1,+0,+0, 0,    0, -1,+2,+0, 0, +1,+0,+0, 0,    0, -2,+0,+0, 0, +0,+0,-1, 0,
                0, -2,+0,+0, 0, +0,+0,+1, 0,    0, -2,+0,+0, 0, +0,-1,+0, 0,    0, -2,+0,+0, 0, +0,+1,+0, 0,    0, -2,+0,+0, 0, -1,+0,+0, 0,
                0, -2,+0,+0, 0, +1,+0,+0, 0,    0, +1,+0,+0, 0, +0,+0,-1, 0,    0, +1,+0,+0, 0, +0,+0,-2, 0,    0, +1,+0,+0, 0, +0,+0,+1, 0,
                0, +1,+0,+0, 0, +0,+0,+2, 0,    0, +1,+0,+0, 0, +0,-1,+0, 0,    0, +1,+0,+0, 0, +0,-1,-1, 0,    0, +1,+0,+0, 0, +0,-1,+1, 0,
                0, +1,+0,+0, 0, +0,-1,+2, 0,    0, +1,+0,+0, 0, +0,-2,+0, 0,    0, +1,+0,+0, 0, +0,+1,+0, 0,    0, +1,+0,+0, 0, +0,+1,-1, 0,
                0, +1,+0,+0, 0, +0,+1,-2, 0,    0, +1,+0,+0, 0, +0,+1,+1, 0,    0, +1,+0,+0, 0, +0,+2,+0, 0,    0, +1,+0,+0, 0, -1,+0,+0, 0,
                0, +1,+0,+0, 0, -1,+0,-1, 0,    0, +1,+0,+0, 0, -1,+0,+1, 0,    0, +1,+0,+0, 0, -1,+0,+2, 0,    0, +1,+0,+0, 0, -1,-1,+0, 0,
                0, +1,+0,+0, 0, -1,+1,+0, 0,    0, +1,+0,+0, 0, -1,+1,-1, 0,    0, +1,+0,+0, 0, -1,+1,+1, 0,    0, +1,+0,+0, 0, -1,+2,+0, 0,
                0, +1,+0,+0, 0, -2,+0,+0, 0,    0, +1,+0,+0, 0, +1,+0,-1, 0,    0, +1,+0,+0, 0, +1,+0,-2, 0,    0, +1,+0,+0, 0, +1,+0,+1, 0,
                0, +1,+0,+0, 0, +1,-1,+0, 0,    0, +1,+0,+0, 0, +1,-1,-1, 0,    0, +1,+0,+0, 0, +1,-1,+1, 0,    0, +1,+0,+0, 0, +1,-2,+0, 0,
                0, +1,+0,+0, 0, +1,+1,+0, 0,    0, +1,+0,+0, 0, +2,+0,+0, 0,    0, +1,+0,-1, 0, +0,+0,-1, 0,    0, +1,+0,-1, 0, +0,+0,-2, 0,
                0, +1,+0,-1, 0, +0,+0,+1, 0,    0, +1,+0,-1, 0, +0,+0,+2, 0,    0, +1,+0,-1, 0, +0,-1,+0, 0,    0, +1,+0,-1, 0, +0,-1,-1, 0,
                0, +1,+0,-1, 0, +0,-1,+1, 0,    0, +1,+0,-1, 0, +0,-1,+2, 0,    0, +1,+0,-1, 0, +0,-2,+0, 0,    0, +1,+0,-1, 0, +0,+1,+0, 0,
                0, +1,+0,-1, 0, +0,+1,-1, 0,    0, +1,+0,-1, 0, +0,+1,-2, 0,    0, +1,+0,-1, 0, +0,+1,+1, 0,    0, +1,+0,-1, 0, +0,+2,+0, 0,
                0, +1,+0,-1, 0, -1,+0,+0, 0,    0, +1,+0,-1, 0, -1,+0,-1, 0,    0, +1,+0,-1, 0, -1,+0,+1, 0,    0, +1,+0,-1, 0, -1,+0,+2, 0,
                0, +1,+0,-1, 0, -1,-1,+0, 0,    0, +1,+0,-1, 0, -1,+1,+0, 0,    0, +1,+0,-1, 0, -1,+1,-1, 0,    0, +1,+0,-1, 0, -1,+1,+1, 0,
                0, +1,+0,-1, 0, -1,+2,+0, 0,    0, +1,+0,-1, 0, -2,+0,+0, 0,    0, +1,+0,-1, 0, +1,+0,+0, 0,    0, +1,+0,-1, 0, +1,+0,-2, 0,
                0, +1,+0,-1, 0, +1,+0,+1, 0,    0, +1,+0,-1, 0, +1,-1,+0, 0,    0, +1,+0,-1, 0, +1,-1,-1, 0,    0, +1,+0,-1, 0, +1,-1,+1, 0,
                0, +1,+0,-1, 0, +1,-2,+0, 0,    0, +1,+0,-1, 0, +1,+1,+0, 0,    0, +1,+0,-1, 0, +2,+0,+0, 0,    0, +1,+0,-2, 0, +0,+0,-1, 0,
                0, +1,+0,-2, 0, +0,+0,+1, 0,    0, +1,+0,-2, 0, +0,-1,+0, 0,    0, +1,+0,-2, 0, +0,+1,+0, 0,    0, +1,+0,-2, 0, -1,+0,+0, 0,
                0, +1,+0,-2, 0, +1,+0,+0, 0,    0, +1,+0,+1, 0, +0,+0,-1, 0,    0, +1,+0,+1, 0, +0,+0,+1, 0,    0, +1,+0,+1, 0, +0,-1,+0, 0,
                0, +1,+0,+1, 0, +0,+1,+0, 0,    0, +1,+0,+1, 0, -1,+0,+0, 0,    0, +1,+0,+1, 0, +1,+0,+0, 0,    0, +1,-1,+0, 0, +0,+0,-1, 0,
                0, +1,-1,+0, 0, +0,+0,-2, 0,    0, +1,-1,+0, 0, +0,+0,+1, 0,    0, +1,-1,+0, 0, +0,+0,+2, 0,    0, +1,-1,+0, 0, +0,-1,+0, 0,
                0, +1,-1,+0, 0, +0,-1,-1, 0,    0, +1,-1,+0, 0, +0,-1,+1, 0,    0, +1,-1,+0, 0, +0,-1,+2, 0,    0, +1,-1,+0, 0, +0,-2,+0, 0,
                0, +1,-1,+0, 0, +0,+1,+0, 0,    0, +1,-1,+0, 0, +0,+1,-1, 0,    0, +1,-1,+0, 0, +0,+1,-2, 0,    0, +1,-1,+0, 0, +0,+1,+1, 0,
                0, +1,-1,+0, 0, +0,+2,+0, 0,    0, +1,-1,+0, 0, -1,+0,+0, 0,    0, +1,-1,+0, 0, -1,+0,-1, 0,    0, +1,-1,+0, 0, -1,+0,+1, 0,
                0, +1,-1,+0, 0, -1,+0,+2, 0,    0, +1,-1,+0, 0, -1,-1,+0, 0,    0, +1,-1,+0, 0, -1,+1,+0, 0,    0, +1,-1,+0, 0, -1,+1,-1, 0,
                0, +1,-1,+0, 0, -1,+1,+1, 0,    0, +1,-1,+0, 0, -1,+2,+0, 0,    0, +1,-1,+0, 0, -2,+0,+0, 0,    0, +1,-1,+0, 0, +1,+0,+0, 0,
                0, +1,-1,+0, 0, +1,+0,-1, 0,    0, +1,-1,+0, 0, +1,+0,-2, 0,    0, +1,-1,+0, 0, +1,+0,+1, 0,    0, +1,-1,+0, 0, +1,-1,-1, 0,
                0, +1,-1,+0, 0, +1,-1,+1, 0,    0, +1,-1,+0, 0, +1,-2,+0, 0,    0, +1,-1,+0, 0, +1,+1,+0, 0,    0, +1,-1,+0, 0, +2,+0,+0, 0,
                0, +1,-1,-1, 0, +0,+0,-1, 0,    0, +1,-1,-1, 0, +0,+0,+1, 0,    0, +1,-1,-1, 0, +0,-1,+0, 0,    0, +1,-1,-1, 0, +0,+1,+0, 0,
                0, +1,-1,-1, 0, -1,+0,+0, 0,    0, +1,-1,-1, 0, +1,+0,+0, 0,    0, +1,-1,+1, 0, +0,+0,-1, 0,    0, +1,-1,+1, 0, +0,+0,+1, 0,
                0, +1,-1,+1, 0, +0,-1,+0, 0,    0, +1,-1,+1, 0, +0,+1,+0, 0,    0, +1,-1,+1, 0, -1,+0,+0, 0,    0, +1,-1,+1, 0, +1,+0,+0, 0,
                0, +1,-2,+0, 0, +0,+0,-1, 0,    0, +1,-2,+0, 0, +0,+0,+1, 0,    0, +1,-2,+0, 0, +0,-1,+0, 0,    0, +1,-2,+0, 0, +0,+1,+0, 0,
                0, +1,-2,+0, 0, -1,+0,+0, 0,    0, +1,-2,+0, 0, +1,+0,+0, 0,    0, +1,+1,+0, 0, +0,+0,-1, 0,    0, +1,+1,+0, 0, +0,+0,+1, 0,
                0, +1,+1,+0, 0, +0,-1,+0, 0,    0, +1,+1,+0, 0, +0,+1,+0, 0,    0, +1,+1,+0, 0, -1,+0,+0, 0,    0, +1,+1,+0, 0, +1,+0,+0, 0,
                0, +2,+0,+0, 0, +0,+0,-1, 0,    0, +2,+0,+0, 0, +0,+0,+1, 0,    0, +2,+0,+0, 0, +0,-1,+0, 0,    0, +2,+0,+0, 0, +0,+1,+0, 0,
                0, +2,+0,+0, 0, -1,+0,+0, 0,    0, +2,+0,+0, 0, +1,+0,+0, 0
                // end computer-generated sequence
            };
            aindexf_test_indices<3, mbtr_aindexf_periodic_full<long>>(s, res, 3.0);
        }
    }

    SECTION( "mbtr_aindexf_periodic_noreversals" )
    {
        // NaCl - k = 1
        {
            PeriodicTestSystems s(NaCl);
            vector<long> res = { 0, 1 };
            aindexf_test_indices<1, mbtr_aindexf_periodic_noreversals<long>>(s, res, 999);
        }

        // SF6 - k = 1
        {
            PeriodicTestSystems s(SF6);
            vector<long> res = { 0, 1, 2, 3, 4, 5, 6 };
            aindexf_test_indices<1, mbtr_aindexf_periodic_noreversals<long>>(s, res, 999);
        }

        // M1 - k = 2
        {
            PeriodicTestSystems s(M1);
            vector<long> res {
                0, +0,+0,+1, 0,    0, +0,+0,+2, 0,    0, +0,+1,+0, 0,    0, +0,+1,+1, 0,    0, +0,+2,+0, 0,    0, +1,+0,+0, 0,    0, +1,+0,+1, 0,
                0, +1,+1,+0, 0,    0, +2,+0,+0, 0
            };
            aindexf_test_indices<2, mbtr_aindexf_periodic_noreversals<long>>(s, res, 1.0);
        }

        // M2 - k = 2
        {
            PeriodicTestSystems s(M2);
            vector<long> res {
                0, +0,+0,+0, 1,    0, +0,+0,+1, 0,    0, +0,+0,+1, 1,    0, +0,+0,+2, 0,    0, +0,+0,+2, 1,    0, +0,+1,+0, 0,    0, +0,+1,+0, 1,  //  7
                0, +0,+1,+1, 0,    0, +0,+1,+1, 1,    0, +0,+2,+0, 0,    0, +0,+2,+0, 1,    0, +1,+0,+0, 0,    0, +1,+0,+0, 1,    0, +1,+0,+1, 0,  // 14
                0, +1,+0,+1, 1,    0, +1,+1,+0, 0,    0, +1,+1,+0, 1,    0, +2,+0,+0, 0,    0, +2,+0,+0, 1,    // 19, a = 0 done
                1, +0,+0,+1, 0,    1, +0,+0,+1, 1,    1, +0,+0,+2, 0,    1, +0,+0,+2, 1,    1, +0,+1,+0, 0,    1, +0,+1,+0, 1,    1, +0,+1,+1, 0,  // 26
                1, +0,+1,+1, 1,    1, +0,+1,+2, 0,    1, +0,+1,+2, 1,    1, +0,+2,+0, 0,    1, +0,+2,+0, 1,    1, +1,+0,+0, 0,    1, +1,+0,+0, 1,  // 33
                1, +1,+0,+1, 0,    1, +1,+0,+1, 1,    1, +1,+0,+2, 0,    1, +1,+0,+2, 1,    1, +1,+1,+0, 0,    1, +1,+1,+0, 1,    1, +1,+1,+1, 0,  // 40
                1, +1,+1,+1, 1,    1, +1,+1,+2, 0,    1, +1,+1,+2, 1,    1, +1,+2,+0, 0,    1, +1,+2,+0, 1,    1, +2,+0,+0, 0,    1, +2,+0,+0, 1   // 47
            };
            aindexf_test_indices<2, mbtr_aindexf_periodic_noreversals<long>>(s, res, 1.0);
        }

        // M3 - k = 2
        {
            PeriodicTestSystems s(M3);
            vector<long> res {
                0, +0,+0,+0, 1,    0, +0,+0,+0, 2,    0, +0,+0,+1, 0,    0, +0,+0,+1, 1,    0, +0,+0,+1, 2,    0, +0,+1,+0, 0,    0, +0,+1,+0, 1,
                0, +0,+1,+0, 2,    0, +1,+0,+0, 0,    0, +1,+0,+0, 1,    0, +1,+0,+0, 2,    1, +0,+0,+0, 2,    1, +0,+0,+1, 0,    1, +0,+0,+1, 1,
                1, +0,+0,+1, 2,    1, +0,+1,+0, 0,    1, +0,+1,+0, 1,    1, +0,+1,+0, 2,    1, +1,+0,+0, 0,    1, +1,+0,+0, 1,    1, +1,+0,+0, 2,
                2, +0,+0,+1, 0,    2, +0,+0,+1, 1,    2, +0,+0,+1, 2,    2, +0,+1,+0, 0,    2, +0,+1,+0, 1,    2, +0,+1,+0, 2,    2, +1,+0,+0, 0,
                2, +1,+0,+0, 1,    2, +1,+0,+0, 2
            };
            aindexf_test_indices<2, mbtr_aindexf_periodic_noreversals<long>>(s, res, 0.5);
        }

        // M1 - k = 3
        {
            PeriodicTestSystems s(M1);
            vector<long> res {
                0, +0,+0,+1, 0, +0,+0,+2, 0,    0, +0,+0,+1, 0, +0,+1,+0, 0,    0, +0,+0,+1, 0, +0,+1,+1, 0,    0, +0,+0,+1, 0, +0,+2,+0, 0,  // 4
                0, +0,+0,+1, 0, +1,+0,+0, 0,    0, +0,+0,+1, 0, +1,+0,+1, 0,    0, +0,+0,+1, 0, +1,+1,+0, 0,    0, +0,+0,+1, 0, +2,+0,+0, 0,  // 8
                0, +0,+0,+2, 0, +0,+0,+1, 0,    0, +0,+0,+2, 0, +0,+1,+0, 0,    0, +0,+0,+2, 0, +1,+0,+0, 0,    0, +0,+1,+0, 0, +0,+0,+1, 0,  // 12
                0, +0,+1,+0, 0, +0,+0,+2, 0,    0, +0,+1,+0, 0, +0,+1,+1, 0,    0, +0,+1,+0, 0, +0,+2,+0, 0,    0, +0,+1,+0, 0, +1,+0,+0, 0,  // 16
                0, +0,+1,+0, 0, +1,+0,+1, 0,    0, +0,+1,+0, 0, +1,+1,+0, 0,    0, +0,+1,+0, 0, +2,+0,+0, 0,    0, +0,+1,+1, 0, +0,+0,+1, 0,  // 20
                0, +0,+1,+1, 0, +0,+1,+0, 0,    0, +0,+1,+1, 0, +1,+0,+0, 0,    0, +0,+2,+0, 0, +0,+0,+1, 0,    0, +0,+2,+0, 0, +0,+1,+0, 0,  // 24
                0, +0,+2,+0, 0, +1,+0,+0, 0,    0, +1,+0,+0, 0, +0,+0,+1, 0,    0, +1,+0,+0, 0, +0,+0,+2, 0,    0, +1,+0,+0, 0, +0,+1,+0, 0,  // 28
                0, +1,+0,+0, 0, +0,+1,+1, 0,    0, +1,+0,+0, 0, +0,+2,+0, 0,    0, +1,+0,+0, 0, +1,+0,+1, 0,    0, +1,+0,+0, 0, +1,+1,+0, 0,  // 32
                0, +1,+0,+0, 0, +2,+0,+0, 0,    0, +1,+0,+1, 0, +0,+0,+1, 0,    0, +1,+0,+1, 0, +0,+1,+0, 0,    0, +1,+0,+1, 0, +1,+0,+0, 0,  // 36
                0, +1,+1,+0, 0, +0,+0,+1, 0,    0, +1,+1,+0, 0, +0,+1,+0, 0,    0, +1,+1,+0, 0, +1,+0,+0, 0,    0, +2,+0,+0, 0, +0,+0,+1, 0,  // 40
                0, +2,+0,+0, 0, +0,+1,+0, 0,    0, +2,+0,+0, 0, +1,+0,+0, 0
            };
            aindexf_test_indices<3, mbtr_aindexf_periodic_noreversals<long>>(s, res, 1.0);
        }

        // M4 - k = 3
        {
            PeriodicTestSystems s(M4);
            vector<long> res {
                0, +0,+0,+0, 1, +0,+0,+0, 2,    0, +0,+0,+0, 1, +0,+0,+1, 0,    0, +0,+0,+0, 1, +0,+0,+1, 1,    0, +0,+0,+0, 1, +0,+0,+1, 2,  //   4
                0, +0,+0,+0, 1, +0,+1,+0, 0,    0, +0,+0,+0, 1, +0,+1,+0, 1,    0, +0,+0,+0, 1, +0,+1,+0, 2,    0, +0,+0,+0, 1, +1,+0,+0, 0,  //   8
                0, +0,+0,+0, 1, +1,+0,+0, 1,    0, +0,+0,+0, 1, +1,+0,+0, 2,    0, +0,+0,+0, 2, +0,+0,+0, 1,    0, +0,+0,+0, 2, +0,+0,+1, 0,  //  12
                0, +0,+0,+0, 2, +0,+0,+1, 1,    0, +0,+0,+0, 2, +0,+0,+1, 2,    0, +0,+0,+0, 2, +0,+1,+0, 0,    0, +0,+0,+0, 2, +0,+1,+0, 1,  //  16
                0, +0,+0,+0, 2, +0,+1,+0, 2,    0, +0,+0,+0, 2, +1,+0,+0, 0,    0, +0,+0,+0, 2, +1,+0,+0, 1,    0, +0,+0,+0, 2, +1,+0,+0, 2,  //  20
                0, +0,+0,+1, 0, +0,+0,+0, 1,    0, +0,+0,+1, 0, +0,+0,+0, 2,    0, +0,+0,+1, 0, +0,+0,+1, 1,    0, +0,+0,+1, 0, +0,+0,+1, 2,  //  24
                0, +0,+0,+1, 0, +0,+1,+0, 0,    0, +0,+0,+1, 0, +0,+1,+0, 1,    0, +0,+0,+1, 0, +0,+1,+0, 2,    0, +0,+0,+1, 0, +1,+0,+0, 0,  //  28
                0, +0,+0,+1, 0, +1,+0,+0, 1,    0, +0,+0,+1, 0, +1,+0,+0, 2,    0, +0,+0,+1, 1, +0,+0,+0, 1,    0, +0,+0,+1, 1, +0,+0,+0, 2,  //  32
                0, +0,+0,+1, 1, +0,+0,+1, 0,    0, +0,+0,+1, 1, +0,+0,+1, 2,    0, +0,+0,+1, 1, +0,+1,+0, 0,    0, +0,+0,+1, 1, +0,+1,+0, 1,  //  36
                0, +0,+0,+1, 1, +0,+1,+0, 2,    0, +0,+0,+1, 1, +1,+0,+0, 0,    0, +0,+0,+1, 1, +1,+0,+0, 1,    0, +0,+0,+1, 1, +1,+0,+0, 2,  //  40
                0, +0,+0,+1, 2, +0,+0,+0, 1,    0, +0,+0,+1, 2, +0,+0,+0, 2,    0, +0,+0,+1, 2, +0,+0,+1, 0,    0, +0,+0,+1, 2, +0,+0,+1, 1,  //  44
                0, +0,+0,+1, 2, +0,+1,+0, 0,    0, +0,+0,+1, 2, +0,+1,+0, 1,    0, +0,+0,+1, 2, +0,+1,+0, 2,    0, +0,+0,+1, 2, +1,+0,+0, 0,  //  48
                0, +0,+0,+1, 2, +1,+0,+0, 1,    0, +0,+0,+1, 2, +1,+0,+0, 2,    0, +0,+1,+0, 0, +0,+0,+0, 1,    0, +0,+1,+0, 0, +0,+0,+0, 2,  //  52
                0, +0,+1,+0, 0, +0,+0,+1, 0,    0, +0,+1,+0, 0, +0,+0,+1, 1,    0, +0,+1,+0, 0, +0,+0,+1, 2,    0, +0,+1,+0, 0, +0,+1,+0, 1,  //  56
                0, +0,+1,+0, 0, +0,+1,+0, 2,    0, +0,+1,+0, 0, +1,+0,+0, 0,    0, +0,+1,+0, 0, +1,+0,+0, 1,    0, +0,+1,+0, 0, +1,+0,+0, 2,  //  60
                0, +0,+1,+0, 1, +0,+0,+0, 1,    0, +0,+1,+0, 1, +0,+0,+0, 2,    0, +0,+1,+0, 1, +0,+0,+1, 0,    0, +0,+1,+0, 1, +0,+0,+1, 1,  //  64
                0, +0,+1,+0, 1, +0,+0,+1, 2,    0, +0,+1,+0, 1, +0,+1,+0, 0,    0, +0,+1,+0, 1, +0,+1,+0, 2,    0, +0,+1,+0, 1, +1,+0,+0, 0,  //  68
                0, +0,+1,+0, 1, +1,+0,+0, 1,    0, +0,+1,+0, 1, +1,+0,+0, 2,    0, +0,+1,+0, 2, +0,+0,+0, 1,    0, +0,+1,+0, 2, +0,+0,+0, 2,  //  72
                0, +0,+1,+0, 2, +0,+0,+1, 0,    0, +0,+1,+0, 2, +0,+0,+1, 1,    0, +0,+1,+0, 2, +0,+0,+1, 2,    0, +0,+1,+0, 2, +0,+1,+0, 0,  //  76
                0, +0,+1,+0, 2, +0,+1,+0, 1,    0, +0,+1,+0, 2, +1,+0,+0, 0,    0, +0,+1,+0, 2, +1,+0,+0, 1,    0, +0,+1,+0, 2, +1,+0,+0, 2,  //  80
                0, +1,+0,+0, 0, +0,+0,+0, 1,    0, +1,+0,+0, 0, +0,+0,+0, 2,    0, +1,+0,+0, 0, +0,+0,+1, 0,    0, +1,+0,+0, 0, +0,+0,+1, 1,  //  84
                0, +1,+0,+0, 0, +0,+0,+1, 2,    0, +1,+0,+0, 0, +0,+1,+0, 0,    0, +1,+0,+0, 0, +0,+1,+0, 1,    0, +1,+0,+0, 0, +0,+1,+0, 2,  //  88
                0, +1,+0,+0, 0, +1,+0,+0, 1,    0, +1,+0,+0, 0, +1,+0,+0, 2,    0, +1,+0,+0, 1, +0,+0,+0, 1,    0, +1,+0,+0, 1, +0,+0,+0, 2,  //  92
                0, +1,+0,+0, 1, +0,+0,+1, 0,    0, +1,+0,+0, 1, +0,+0,+1, 1,    0, +1,+0,+0, 1, +0,+0,+1, 2,    0, +1,+0,+0, 1, +0,+1,+0, 0,  //  96
                0, +1,+0,+0, 1, +0,+1,+0, 1,    0, +1,+0,+0, 1, +0,+1,+0, 2,    0, +1,+0,+0, 1, +1,+0,+0, 0,    0, +1,+0,+0, 1, +1,+0,+0, 2,  // 100
                0, +1,+0,+0, 2, +0,+0,+0, 1,    0, +1,+0,+0, 2, +0,+0,+0, 2,    0, +1,+0,+0, 2, +0,+0,+1, 0,    0, +1,+0,+0, 2, +0,+0,+1, 1,  // 104
                0, +1,+0,+0, 2, +0,+0,+1, 2,    0, +1,+0,+0, 2, +0,+1,+0, 0,    0, +1,+0,+0, 2, +0,+1,+0, 1,    0, +1,+0,+0, 2, +0,+1,+0, 2,  // 108
                0, +1,+0,+0, 2, +1,+0,+0, 0,    0, +1,+0,+0, 2, +1,+0,+0, 1,    1, +0,+0,+0, 0, +0,+0,+0, 2,    1, +0,+0,+0, 0, +0,+0,+1, 0,  // 112
                1, +0,+0,+0, 0, +0,+0,+1, 1,    1, +0,+0,+0, 0, +0,+0,+1, 2,    1, +0,+0,+0, 0, +0,+1,+0, 0,    1, +0,+0,+0, 0, +0,+1,+0, 1,  // 116
                1, +0,+0,+0, 0, +0,+1,+0, 2,    1, +0,+0,+0, 0, +1,+0,+0, 0,    1, +0,+0,+0, 0, +1,+0,+0, 1,    1, +0,+0,+0, 0, +1,+0,+0, 2,  // 120
                1, +0,+0,+0, 2, +0,+0,+1, 0,    1, +0,+0,+0, 2, +0,+0,+1, 1,    1, +0,+0,+0, 2, +0,+0,+1, 2,    1, +0,+0,+0, 2, +0,+1,+0, 0,  // 124
                1, +0,+0,+0, 2, +0,+1,+0, 1,    1, +0,+0,+0, 2, +0,+1,+0, 2,    1, +0,+0,+0, 2, +1,+0,+0, 0,    1, +0,+0,+0, 2, +1,+0,+0, 1,  // 128
                1, +0,+0,+0, 2, +1,+0,+0, 2,    1, +0,+0,+1, 0, +0,+0,+0, 2,    1, +0,+0,+1, 0, +0,+0,+1, 1,    1, +0,+0,+1, 0, +0,+0,+1, 2,  // 132
                1, +0,+0,+1, 0, +0,+1,+0, 0,    1, +0,+0,+1, 0, +0,+1,+0, 1,    1, +0,+0,+1, 0, +0,+1,+0, 2,    1, +0,+0,+1, 0, +1,+0,+0, 0,  // 136
                1, +0,+0,+1, 0, +1,+0,+0, 1,    1, +0,+0,+1, 0, +1,+0,+0, 2,    1, +0,+0,+1, 1, +0,+0,+0, 2,    1, +0,+0,+1, 1, +0,+0,+1, 0,  // 140
                1, +0,+0,+1, 1, +0,+0,+1, 2,    1, +0,+0,+1, 1, +0,+1,+0, 0,    1, +0,+0,+1, 1, +0,+1,+0, 1,    1, +0,+0,+1, 1, +0,+1,+0, 2,  // 144
                1, +0,+0,+1, 1, +1,+0,+0, 0,    1, +0,+0,+1, 1, +1,+0,+0, 1,    1, +0,+0,+1, 1, +1,+0,+0, 2,    1, +0,+0,+1, 2, +0,+0,+0, 2,  // 148
                1, +0,+0,+1, 2, +0,+0,+1, 0,    1, +0,+0,+1, 2, +0,+0,+1, 1,    1, +0,+0,+1, 2, +0,+1,+0, 0,    1, +0,+0,+1, 2, +0,+1,+0, 1,  // 152
                1, +0,+0,+1, 2, +0,+1,+0, 2,    1, +0,+0,+1, 2, +0,+1,+1, 0,    1, +0,+0,+1, 2, +0,+1,+1, 1,    1, +0,+0,+1, 2, +0,+1,+1, 2,  // 156
                1, +0,+0,+1, 2, +0,+2,+0, 0,    1, +0,+0,+1, 2, +0,+2,+0, 1,    1, +0,+0,+1, 2, +0,+2,+0, 2,    1, +0,+0,+1, 2, +1,+0,+0, 0,  // 160
                1, +0,+0,+1, 2, +1,+0,+0, 1,    1, +0,+0,+1, 2, +1,+0,+0, 2,    1, +0,+0,+2, 0, +0,+0,+0, 2,    1, +0,+0,+2, 0, +0,+0,+1, 0,  // 164
                1, +0,+0,+2, 0, +0,+0,+1, 1,    1, +0,+0,+2, 0, +0,+0,+1, 2,    1, +0,+0,+2, 0, +0,+1,+0, 0,    1, +0,+0,+2, 0, +0,+1,+0, 1,  // 168
                1, +0,+0,+2, 0, +0,+1,+0, 2,    1, +0,+0,+2, 0, +1,+0,+0, 0,    1, +0,+0,+2, 0, +1,+0,+0, 1,    1, +0,+0,+2, 0, +1,+0,+0, 2,  // 172
                1, +0,+0,+2, 1, +0,+0,+0, 2,    1, +0,+0,+2, 1, +0,+0,+1, 0,    1, +0,+0,+2, 1, +0,+0,+1, 1,    1, +0,+0,+2, 1, +0,+0,+1, 2,  // 176
                1, +0,+0,+2, 1, +0,+1,+0, 0,    1, +0,+0,+2, 1, +0,+1,+0, 1,    1, +0,+0,+2, 1, +0,+1,+0, 2,    1, +0,+0,+2, 1, +1,+0,+0, 0,  // 180
                1, +0,+0,+2, 1, +1,+0,+0, 1,    1, +0,+0,+2, 1, +1,+0,+0, 2,    1, +0,+0,+2, 2, +0,+0,+0, 2,    1, +0,+0,+2, 2, +0,+0,+1, 0,  // 184
                1, +0,+0,+2, 2, +0,+0,+1, 1,    1, +0,+0,+2, 2, +0,+0,+1, 2,    1, +0,+0,+2, 2, +0,+1,+0, 0,    1, +0,+0,+2, 2, +0,+1,+0, 1,  // 188
                1, +0,+0,+2, 2, +0,+1,+0, 2,    1, +0,+0,+2, 2, +1,+0,+0, 0,    1, +0,+0,+2, 2, +1,+0,+0, 1,    1, +0,+0,+2, 2, +1,+0,+0, 2,  // 192
                1, +0,+1,+0, 0, +0,+0,+0, 2,    1, +0,+1,+0, 0, +0,+0,+1, 0,    1, +0,+1,+0, 0, +0,+0,+1, 1,    1, +0,+1,+0, 0, +0,+0,+1, 2,  // 196
                1, +0,+1,+0, 0, +0,+0,+2, 0,    1, +0,+1,+0, 0, +0,+0,+2, 1,    1, +0,+1,+0, 0, +0,+0,+2, 2,    1, +0,+1,+0, 0, +0,+1,+0, 1,  // 200
                1, +0,+1,+0, 0, +0,+1,+0, 2,    1, +0,+1,+0, 0, +1,+0,+0, 0,    1, +0,+1,+0, 0, +1,+0,+0, 1,    1, +0,+1,+0, 0, +1,+0,+0, 2,  // 204
                1, +0,+1,+0, 1, +0,+0,+0, 2,    1, +0,+1,+0, 1, +0,+0,+1, 0,    1, +0,+1,+0, 1, +0,+0,+1, 1,    1, +0,+1,+0, 1, +0,+0,+1, 2,  // 208
                1, +0,+1,+0, 1, +0,+1,+0, 0,    1, +0,+1,+0, 1, +0,+1,+0, 2,    1, +0,+1,+0, 1, +1,+0,+0, 0,    1, +0,+1,+0, 1, +1,+0,+0, 1,  // 212
                1, +0,+1,+0, 1, +1,+0,+0, 2,    1, +0,+1,+0, 2, +0,+0,+0, 2,    1, +0,+1,+0, 2, +0,+0,+1, 0,    1, +0,+1,+0, 2, +0,+0,+1, 1,  // 216
                1, +0,+1,+0, 2, +0,+0,+1, 2,    1, +0,+1,+0, 2, +0,+1,+0, 0,    1, +0,+1,+0, 2, +0,+1,+0, 1,    1, +0,+1,+0, 2, +1,+0,+0, 0,  // 220
                1, +0,+1,+0, 2, +1,+0,+0, 1,    1, +0,+1,+0, 2, +1,+0,+0, 2,    1, +0,+1,+1, 0, +0,+0,+0, 2,    1, +0,+1,+1, 0, +0,+0,+1, 0,  // 224
                1, +0,+1,+1, 0, +0,+0,+1, 1,    1, +0,+1,+1, 0, +0,+0,+1, 2,    1, +0,+1,+1, 0, +0,+1,+0, 0,    1, +0,+1,+1, 0, +0,+1,+0, 1,  // 228
                1, +0,+1,+1, 0, +0,+1,+0, 2,    1, +0,+1,+1, 0, +1,+0,+0, 0,    1, +0,+1,+1, 0, +1,+0,+0, 1,    1, +0,+1,+1, 0, +1,+0,+0, 2,  // 232
                1, +0,+1,+1, 1, +0,+0,+0, 2,    1, +0,+1,+1, 1, +0,+0,+1, 0,    1, +0,+1,+1, 1, +0,+0,+1, 1,    1, +0,+1,+1, 1, +0,+0,+1, 2,  // 236
                1, +0,+1,+1, 1, +0,+1,+0, 0,    1, +0,+1,+1, 1, +0,+1,+0, 1,    1, +0,+1,+1, 1, +0,+1,+0, 2,    1, +0,+1,+1, 1, +1,+0,+0, 0,  // 240
                1, +0,+1,+1, 1, +1,+0,+0, 1,    1, +0,+1,+1, 1, +1,+0,+0, 2,    1, +0,+1,+1, 2, +0,+0,+0, 2,    1, +0,+1,+1, 2, +0,+0,+1, 0,  // 244
                1, +0,+1,+1, 2, +0,+0,+1, 1,    1, +0,+1,+1, 2, +0,+0,+1, 2,    1, +0,+1,+1, 2, +0,+1,+0, 0,    1, +0,+1,+1, 2, +0,+1,+0, 1,  // 248
                1, +0,+1,+1, 2, +0,+1,+0, 2,    1, +0,+1,+1, 2, +1,+0,+0, 0,    1, +0,+1,+1, 2, +1,+0,+0, 1,    1, +0,+1,+1, 2, +1,+0,+0, 2,  // 252
                1, +0,+2,+0, 0, +0,+0,+0, 2,    1, +0,+2,+0, 0, +0,+0,+1, 0,    1, +0,+2,+0, 0, +0,+0,+1, 1,    1, +0,+2,+0, 0, +0,+0,+1, 2,  // 256
                1, +0,+2,+0, 0, +0,+1,+0, 0,    1, +0,+2,+0, 0, +0,+1,+0, 1,    1, +0,+2,+0, 0, +0,+1,+0, 2,    1, +0,+2,+0, 0, +1,+0,+0, 0,  // 260
                1, +0,+2,+0, 0, +1,+0,+0, 1,    1, +0,+2,+0, 0, +1,+0,+0, 2,    1, +0,+2,+0, 1, +0,+0,+0, 2,    1, +0,+2,+0, 1, +0,+0,+1, 0,  // 264
                1, +0,+2,+0, 1, +0,+0,+1, 1,    1, +0,+2,+0, 1, +0,+0,+1, 2,    1, +0,+2,+0, 1, +0,+1,+0, 0,    1, +0,+2,+0, 1, +0,+1,+0, 1,  // 268
                1, +0,+2,+0, 1, +0,+1,+0, 2,    1, +0,+2,+0, 1, +1,+0,+0, 0,    1, +0,+2,+0, 1, +1,+0,+0, 1,    1, +0,+2,+0, 1, +1,+0,+0, 2,  // 272
                1, +0,+2,+0, 2, +0,+0,+0, 2,    1, +0,+2,+0, 2, +0,+0,+1, 0,    1, +0,+2,+0, 2, +0,+0,+1, 1,    1, +0,+2,+0, 2, +0,+0,+1, 2,  // 276
                1, +0,+2,+0, 2, +0,+1,+0, 0,    1, +0,+2,+0, 2, +0,+1,+0, 1,    1, +0,+2,+0, 2, +0,+1,+0, 2,    1, +0,+2,+0, 2, +1,+0,+0, 0,  // 280
                1, +0,+2,+0, 2, +1,+0,+0, 1,    1, +0,+2,+0, 2, +1,+0,+0, 2,    1, +1,+0,+0, 0, +0,+0,+0, 2,    1, +1,+0,+0, 0, +0,+0,+1, 0,  // 284
                1, +1,+0,+0, 0, +0,+0,+1, 1,    1, +1,+0,+0, 0, +0,+0,+1, 2,    1, +1,+0,+0, 0, +0,+1,+0, 0,    1, +1,+0,+0, 0, +0,+1,+0, 1,  // 288
                1, +1,+0,+0, 0, +0,+1,+0, 2,    1, +1,+0,+0, 0, +1,+0,+0, 1,    1, +1,+0,+0, 0, +1,+0,+0, 2,    1, +1,+0,+0, 1, +0,+0,+0, 2,  // 292
                1, +1,+0,+0, 1, +0,+0,+1, 0,    1, +1,+0,+0, 1, +0,+0,+1, 1,    1, +1,+0,+0, 1, +0,+0,+1, 2,    1, +1,+0,+0, 1, +0,+1,+0, 0,  // 296
                1, +1,+0,+0, 1, +0,+1,+0, 1,    1, +1,+0,+0, 1, +0,+1,+0, 2,    1, +1,+0,+0, 1, +1,+0,+0, 0,    1, +1,+0,+0, 1, +1,+0,+0, 2,  // 300
                1, +1,+0,+0, 2, +0,+0,+0, 2,    1, +1,+0,+0, 2, +0,+0,+1, 0,    1, +1,+0,+0, 2, +0,+0,+1, 1,    1, +1,+0,+0, 2, +0,+0,+1, 2,  // 304
                1, +1,+0,+0, 2, +0,+1,+0, 0,    1, +1,+0,+0, 2, +0,+1,+0, 1,    1, +1,+0,+0, 2, +0,+1,+0, 2,    1, +1,+0,+0, 2, +1,+0,+0, 0,  // 308
                1, +1,+0,+0, 2, +1,+0,+0, 1,    2, +0,+0,+0, 0, +0,+0,+1, 0,    2, +0,+0,+0, 0, +0,+0,+1, 1,    2, +0,+0,+0, 0, +0,+0,+1, 2,  // 312
                2, +0,+0,+0, 0, +0,+1,+0, 0,    2, +0,+0,+0, 0, +0,+1,+0, 1,    2, +0,+0,+0, 0, +0,+1,+0, 2,    2, +0,+0,+0, 0, +1,+0,+0, 0,  // 316
                2, +0,+0,+0, 0, +1,+0,+0, 1,    2, +0,+0,+0, 0, +1,+0,+0, 2,    2, +0,+0,+0, 1, +0,+0,+1, 0,    2, +0,+0,+0, 1, +0,+0,+1, 1,  // 320
                2, +0,+0,+0, 1, +0,+0,+1, 2,    2, +0,+0,+0, 1, +0,+1,+0, 0,    2, +0,+0,+0, 1, +0,+1,+0, 1,    2, +0,+0,+0, 1, +0,+1,+0, 2,  // 324
                2, +0,+0,+0, 1, +1,+0,+0, 0,    2, +0,+0,+0, 1, +1,+0,+0, 1,    2, +0,+0,+0, 1, +1,+0,+0, 2,    2, +0,+0,+1, 0, +0,+0,+1, 1,  // 328
                2, +0,+0,+1, 0, +0,+0,+1, 2,    2, +0,+0,+1, 0, +0,+1,+0, 0,    2, +0,+0,+1, 0, +0,+1,+0, 1,    2, +0,+0,+1, 0, +0,+1,+0, 2,  // 332
                2, +0,+0,+1, 0, +1,+0,+0, 0,    2, +0,+0,+1, 0, +1,+0,+0, 1,    2, +0,+0,+1, 0, +1,+0,+0, 2,    2, +0,+0,+1, 1, +0,+0,+1, 0,  // 336
                2, +0,+0,+1, 1, +0,+0,+1, 2,    2, +0,+0,+1, 1, +0,+1,+0, 0,    2, +0,+0,+1, 1, +0,+1,+0, 1,    2, +0,+0,+1, 1, +0,+1,+0, 2,  // 340
                2, +0,+0,+1, 1, +1,+0,+0, 0,    2, +0,+0,+1, 1, +1,+0,+0, 1,    2, +0,+0,+1, 1, +1,+0,+0, 2,    2, +0,+0,+1, 2, +0,+0,+1, 0,  // 344
                2, +0,+0,+1, 2, +0,+0,+1, 1,    2, +0,+0,+1, 2, +0,+1,+0, 0,    2, +0,+0,+1, 2, +0,+1,+0, 1,    2, +0,+0,+1, 2, +0,+1,+0, 2,  // 348
                2, +0,+0,+1, 2, +1,+0,+0, 0,    2, +0,+0,+1, 2, +1,+0,+0, 1,    2, +0,+0,+1, 2, +1,+0,+0, 2,    2, +0,+1,+0, 0, +0,+0,+1, 0,  // 352
                2, +0,+1,+0, 0, +0,+0,+1, 1,    2, +0,+1,+0, 0, +0,+0,+1, 2,    2, +0,+1,+0, 0, +0,+1,+0, 1,    2, +0,+1,+0, 0, +0,+1,+0, 2,  // 356
                2, +0,+1,+0, 0, +1,+0,+0, 0,    2, +0,+1,+0, 0, +1,+0,+0, 1,    2, +0,+1,+0, 0, +1,+0,+0, 2,    2, +0,+1,+0, 1, +0,+0,+1, 0,  // 360
                2, +0,+1,+0, 1, +0,+0,+1, 1,    2, +0,+1,+0, 1, +0,+0,+1, 2,    2, +0,+1,+0, 1, +0,+1,+0, 0,    2, +0,+1,+0, 1, +0,+1,+0, 2,  // 364
                2, +0,+1,+0, 1, +1,+0,+0, 0,    2, +0,+1,+0, 1, +1,+0,+0, 1,    2, +0,+1,+0, 1, +1,+0,+0, 2,    2, +0,+1,+0, 2, +0,+0,+1, 0,  // 368
                2, +0,+1,+0, 2, +0,+0,+1, 1,    2, +0,+1,+0, 2, +0,+0,+1, 2,    2, +0,+1,+0, 2, +0,+1,+0, 0,    2, +0,+1,+0, 2, +0,+1,+0, 1,  // 372
                2, +0,+1,+0, 2, +1,+0,+0, 0,    2, +0,+1,+0, 2, +1,+0,+0, 1,    2, +0,+1,+0, 2, +1,+0,+0, 2,    2, +1,+0,+0, 0, +0,+0,+1, 0,  // 376
                2, +1,+0,+0, 0, +0,+0,+1, 1,    2, +1,+0,+0, 0, +0,+0,+1, 2,    2, +1,+0,+0, 0, +0,+1,+0, 0,    2, +1,+0,+0, 0, +0,+1,+0, 1,  // 380
                2, +1,+0,+0, 0, +0,+1,+0, 2,    2, +1,+0,+0, 0, +1,+0,+0, 1,    2, +1,+0,+0, 0, +1,+0,+0, 2,    2, +1,+0,+0, 1, +0,+0,+1, 0,  // 384
                2, +1,+0,+0, 1, +0,+0,+1, 1,    2, +1,+0,+0, 1, +0,+0,+1, 2,    2, +1,+0,+0, 1, +0,+1,+0, 0,    2, +1,+0,+0, 1, +0,+1,+0, 1,  // 388
                2, +1,+0,+0, 1, +0,+1,+0, 2,    2, +1,+0,+0, 1, +1,+0,+0, 0,    2, +1,+0,+0, 1, +1,+0,+0, 2,    2, +1,+0,+0, 2, +0,+0,+1, 0,  // 392
                2, +1,+0,+0, 2, +0,+0,+1, 1,    2, +1,+0,+0, 2, +0,+0,+1, 2,    2, +1,+0,+0, 2, +0,+1,+0, 0,    2, +1,+0,+0, 2, +0,+1,+0, 1,  // 396
                2, +1,+0,+0, 2, +0,+1,+0, 2,    2, +1,+0,+0, 2, +1,+0,+0, 0,    2, +1,+0,+0, 2, +1,+0,+0, 1
            };
            aindexf_test_indices<3, mbtr_aindexf_periodic_noreversals<long>>(s, res, 0.5);
        }
    }
}


//  ////////////////////////
//  //  Many-body tensor  //
//  ////////////////////////

TEST_CASE("many-body_tensor_representation_finite", "[representations],[many-body_tensor]")
{
    using std::vector;
    const auto H = FiniteTestSystems::H; const auto H3 = FiniteTestSystems::H3; const auto O2 = FiniteTestSystems::O2; const auto HCl = FiniteTestSystems::HCl; const auto H2O = FiniteTestSystems::H2O; const auto C6H6 = FiniteTestSystems::C6H6; const auto HCN = FiniteTestSystems::HCN; const auto HCNO = FiniteTestSystems::HCNO; const auto CHNOS = FiniteTestSystems::CHNOS; const auto EMPTY = FiniteTestSystems::EMPTY; const auto H4 = FiniteTestSystems::H4; const auto SF6 = FiniteTestSystems::SF6;

    SECTION( "k = 1, simple test cases" )
    {
        // H - unity, unity
        {
            FiniteTestSystems s(H);
            const long n = 1 * 1 * 1;  // ns * nec * xdim
            double mbtr[n]; std::fill(mbtr, mbtr+n, -99.);
            mbtr_call<1>(mbtr, s, 0.5, 1, 1, mbtr_geomf_unity(), mbtr_weightf_unity(), mbtr_normal_distribution(1e-10),
                mbtr_identity_correlation(), mbtr_eindexf_dense_full(), mbtr_aindexf_finite_full<long>());
            CHECK( vector<double>(mbtr, mbtr+n) == (vector<double>{ 1 }) );
        }

        // H3 - count, unity
        {
            FiniteTestSystems s(H3);
            const long n = 1 * 1 * 3;  // ns * nec * xdim
            double mbtr[n]; std::fill(mbtr, mbtr+n, -99.);
            mbtr_call<1>(mbtr, s, 0.5, 1, 3, mbtr_geomf1_count<long>(), mbtr_weightf_unity(), mbtr_normal_distribution(1e-9),
                mbtr_identity_correlation(), mbtr_eindexf_dense_full(), mbtr_aindexf_finite_full<long>());
            CHECK( vector<double>(mbtr, mbtr+n) == (vector<double>{ 0, 0, 3 }) );
        }

        // H + H3 - count, 1/identity
        {
            FiniteTestSystems s(H | H3);
            const long n = 2 * 1 * 3;  // ns * nec * xdim
            double mbtr[n]; std::fill(mbtr, mbtr+n, -99.);
            mbtr_call<1>(mbtr, s, 0.5, 1, 3, mbtr_geomf1_count<long>(), mbtr_weightf_one_over_identity(), mbtr_normal_distribution(1e-8),
                mbtr_identity_correlation(), mbtr_eindexf_dense_full(), mbtr_aindexf_finite_full<long>());
            CHECK( vector<double>(mbtr, mbtr+n) == (vector<double>{ 1, 0, 0,  0, 0, 1 }) );
        }

        // H + H3 - unity, unity; 2 systems
        {
            FiniteTestSystems s(H | H3);
            const long n = 2 * 1 * 3;  // ns * nec * xdim
            double mbtr[n]; std::fill(mbtr, mbtr+n, -99.);
            mbtr_call<1>(mbtr, s, -0.5, 1, 3, mbtr_geomf_unity(), mbtr_weightf_unity(), mbtr_normal_distribution(1e-7),
                mbtr_identity_correlation(), mbtr_eindexf_dense_full(), mbtr_aindexf_finite_full<long>());
            CHECK( vector<double>(mbtr, mbtr+n) == (vector<double>{ 0, 1, 0,  0, 3, 0 }) );
        }

        // H3 + O2 - unity, unity; multiple elements
        {
            FiniteTestSystems s(H3 | O2);
            const long n = 2 * 2 * 3;  // ns * nec * xdim
            double mbtr[n]; std::fill(mbtr, mbtr+n, -99.);
            mbtr_call<1>(mbtr, s, -0.5, 1, 3, mbtr_geomf_unity(), mbtr_weightf_unity(), mbtr_normal_distribution(1e-7),
                mbtr_identity_correlation(), mbtr_eindexf_dense_full(), mbtr_aindexf_finite_full<long>());
            CHECK( vector<double>(mbtr, mbtr+n) == (vector<double>{ 0, 3, 0,  0, 0, 0,    0, 0, 0,  0, 2, 0 }) );
        }

        // H - count, 1/identity; normal distribution
        // res: Table[Integrate[PDF[NormalDistribution[1., 0.5], x], {x, i, i + 1}], {i, Most[Range[-4, 6, 1]]}]
        {
            FiniteTestSystems s(H);
            const long n = 1 * 1 * 10;  // ns * nec * xdim
            double mbtr[n]; std::fill(mbtr, mbtr+n, -99.);
            mbtr_call<1>(mbtr, s, -4, 1, 10, mbtr_geomf1_count<long>(), mbtr_weightf_one_over_identity(), mbtr_normal_distribution(0.5),
                mbtr_identity_correlation(), mbtr_eindexf_dense_full(), mbtr_aindexf_finite_full<long>());
            const double res[] = { 6.20081e-16, 9.86587e-10, 0.0000316703, 0.0227185, 0.47725, 0.47725, 0.0227185, 0.0000316703, 9.86587e-10, 6.20081e-16 };
            CHECK( round(mbtr, n, 1e-5) == round(res, n, 1e-5) );
        }
    }

    SECTION( "k = 2, simple test cases" )
    {
        // O2 - 1/distance, unity; single peak with >100 bins
        // res: f[x_] := CDF[NormalDistribution[0.6756756756756757, 10^-6]][x]; t = Chop[Table[f[x+d]-f[x], {x, Most[Range[-7.5, 8.5, 0.1]]}]]
        {
            FiniteTestSystems s(O2);
            const long n = 1 * 1 * 160;  // ns * nec * xdim
            double mbtr[n]; std::fill(mbtr, mbtr+n, -99.);
            mbtr_call<2>(mbtr, s, -7.5, 0.1, 160, mbtr_geomf2_one_over_distance(), mbtr_weightf_unity(), mbtr_normal_distribution(1e-6),
                mbtr_identity_correlation(), mbtr_eindexf_dense_full(), mbtr_aindexf_finite_full<long>());
            double res[] = {
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
            };
            CHECK( round(mbtr, n, 1e-5) == round(res, n, 1e-5) );

            // Same with Reversals-corrected (non-redundant) atom indexing
            mbtr_call<2>(mbtr, s, -7.5, 0.1, 160, mbtr_geomf2_one_over_distance(), mbtr_weightf_unity(), mbtr_normal_distribution(1e-6),
                mbtr_identity_correlation(), mbtr_eindexf_dense_full(), mbtr_aindexf_finite_noreversals<long>());
            res[81] = 1;
            CHECK( round(mbtr, n, 1e-5) == round(res, n, 1e-5) );
        }

        // O2 (distribution with >100 bins)
        // res: Table[NIntegrate[PDF[NormalDistribution[0.6756756756756757, 1.5], x], {x, i, i + 0.1}], {i, Most[Range[-7.5, 8.5, 0.1]]}]
        {
            FiniteTestSystems s(O2);
            const long n = 1 * 1 * 160;  // ns * nec * xdim
            double mbtr[n]; std::fill(mbtr, mbtr+n, -99.);
            mbtr_call<2>(mbtr, s, -7.5, 0.1, 160, mbtr_geomf2_one_over_distance(), mbtr_weightf_unity(), mbtr_normal_distribution(1.5),
                mbtr_identity_correlation(), mbtr_eindexf_dense_noreversals(), mbtr_aindexf_finite_noreversals<long>());
            const double res[] = {
                1.13469e-8  , 1.62441e-8  , 2.31517e-8  , 3.28506e-8  , 4.6406e-8   , 6.52642e-8  , 9.1379e-8   , 1.27376e-7  , 1.76767e-7  , 2.44222e-7  ,
                3.35921e-7  , 4.60004e-7  , 6.27128e-7  , 8.5118e-7   , 1.15016e-6  , 1.54726e-6  , 2.07224e-6  , 2.76304e-6  , 3.66779e-6  , 4.84722e-6  ,
                6.37751e-6  , 8.35373e-6  , 0.0000108938, 0.0000141433, 0.0000182806, 0.0000235235, 0.0000301359, 0.0000384358, 0.0000488044, 0.0000616953,
                0.0000776455, 0.000097286 , 0.000121354 , 0.000150706 , 0.000186327 , 0.000229347 , 0.000281048 , 0.000342876 , 0.000416452 , 0.000503574 ,
                0.000606223 , 0.000726561 , 0.000866926 , 0.00102982  , 0.00121791  , 0.00143395  , 0.00168084  , 0.00196151  , 0.00227889  , 0.00263588  ,
                0.00303529  , 0.00347973  , 0.00397155  , 0.0045128   , 0.00510508  , 0.00574948  , 0.00644653  , 0.00719605  , 0.00799709  , 0.00884791  ,
                0.00974586  , 0.0106874   , 0.0116678   , 0.0126818   , 0.0137228   , 0.0147834   , 0.0158554   , 0.0169297   , 0.0179967   , 0.0190461   ,
                0.0200674   , 0.0210497   , 0.0219822   , 0.0228543   , 0.0236556   , 0.0243765   , 0.025008    , 0.0255421   , 0.025972    , 0.0262921   ,
                0.0264981   , 0.0265873   , 0.0265586   , 0.0264123   , 0.0261504   , 0.0257763   , 0.0252949   , 0.0247125   , 0.0240364   , 0.0232753   ,
                0.0224383   , 0.0215355   , 0.0205774   , 0.0195748   , 0.0185385   , 0.0174792   , 0.0164074   , 0.015333    , 0.0142655   , 0.0132134   ,
                0.0121847   , 0.0111863   , 0.0102242   , 0.00930334  , 0.00842793  , 0.00760105  , 0.0068249   , 0.00610085  , 0.00542943  , 0.00481049  ,
                0.0042432   , 0.00372623  , 0.00325774  , 0.00283552  , 0.00245708  , 0.00211971  , 0.00182056  , 0.0015567   , 0.00132518  , 0.00112309  ,
                0.000947597 , 0.000795985 , 0.000665665 , 0.000554214 , 0.000459378 , 0.000379082 , 0.000311434 , 0.000254724 , 0.000207417 , 0.000168147 ,
                0.000135708 , 0.000109041 , 0.0000872263, 0.0000694664, 0.0000550772, 0.0000434751, 0.0000341648, 0.0000267293, 0.0000208194, 0.0000161442,
                0.0000124634, 9.5792e-6   , 7.32978e-6  , 5.58371e-6  , 4.23473e-6  , 3.19741e-6  , 2.40349e-6  , 1.79869e-6  , 1.34011e-6  , 9.94024e-7  ,
                7.34045e-7  , 5.39658e-7  , 3.9499e-7   , 2.87821e-7  , 2.088e-7    , 1.50803e-7  , 1.08432e-7  , 7.76206e-8  , 5.53181e-8  , 3.92489e-8
            };
            CHECK( round(mbtr, n, 1e-5) == round(res, n, 1e-5) );
        }

        // H2O (two elements: 2-body element indexing)
        {
            FiniteTestSystems s(H2O);
            const long n = 1 * 3 * 20;  // ns * nec * xdim
            double mbtr[n]; std::fill(mbtr, mbtr+n, -99.);
            mbtr_call<2, mbtr_geomf2_one_over_distance, mbtr_weightf_unity, mbtr_normal_distribution, mbtr_identity_correlation,
                mbtr_eindexf_dense_noreversals, mbtr_aindexf_finite_noreversals<long>>(mbtr, s, 0.5, 0.05, 20, mbtr_geomf2_one_over_distance(),
                mbtr_weightf_unity(), mbtr_normal_distribution(1e-6));
            const double res[] = {
                0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  // HH
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0,  // HO
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0   // OO
            };
            CHECK( round(mbtr, n, 1e-5) == round(res, n, 1e-5) );
        }
    }

    SECTION( "k = 3, angle, unity, simple test cases" )
    {
        // H2O (two elements: 3-body element indexing)
        {
            FiniteTestSystems s(H2O);
            const long n = 1 * 6 * 20;  // ns * nec * xdim
            double mbtr[n]; std::fill(mbtr, mbtr+n, -99.);
            mbtr_call<3, mbtr_geomf3_angle, mbtr_weightf_unity, mbtr_normal_distribution, mbtr_identity_correlation, mbtr_eindexf_dense_noreversals,
                mbtr_aindexf_finite_noreversals<long>>(mbtr, s, 0, 0.15707963267948966, 20, mbtr_geomf3_angle(), mbtr_weightf_unity(),
                mbtr_normal_distribution(1e-6));
            const double res[] = {
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  // HHH
                0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  // HHO  37.775 degree
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,  // HOH  104.45 degree
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  // HOO
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  // OHO
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0   // OOO
            };
            CHECK( round(mbtr, n, 1e-5) == round(res, n, 1e-5) );
        }
    }

    SECTION( "k = 4, unity, unity, simple test cases" )
    {
        //  (two elements: 4-body element indexing)
        {
            FiniteTestSystems s(C6H6);
            const long n = 1 * 10 * 1;  // ns * nec * xdim
            double mbtr[n]; std::fill(mbtr, mbtr+n, -99.);
            mbtr_call<4, mbtr_geomf_unity, mbtr_weightf_unity, mbtr_normal_distribution, mbtr_identity_correlation,
                mbtr_eindexf_dense_noreversals, mbtr_aindexf_finite_noreversals<long>>(mbtr, s, 0.5, 1, 1, mbtr_geomf_unity(),
                mbtr_weightf_unity(), mbtr_normal_distribution(1e-6));
            const double res[] = {
                180,  // HHHH  Length[DeleteDuplicates[DeleteCases[Tuples[Range[6], 4], _?(Length[Union[#]] != 4 &)], (#1 == #2 || #1 == Reverse[#2] &)]]
                720,  // HHHC  Length[DeleteCases[Tuples[Range[6], 3], _?(Length[Union[#]] != 3 &)]]*6
                720,  // HHCH  dito
                900,  // HHCC  dito
                900,  // HCHC  Length[DeleteCases[Tuples[Range[6], 2], _?(Length[Union[#]] != 2 &)]]^2
                450,  // HCCH  Length[DeleteDuplicates[DeleteCases[Tuples[Range[12], 4], _?(Length[Union[#]] != 4 || #[[1]] > 6 || #[[4]] > 6 || #[[2]] < 7 || #[[3]] < 7 &)], (#1 == #2 || #1 == Reverse[#2] &)]]
                720,  // HCCC  as HHHC
                450,  // CHHC  as HCCH
                720,  // CHCC  as HHCH
                180   // CCCC  as HHHH
            };
            CHECK( round(mbtr, n, 1e-5) == round(res, n, 1e-5) );
        }
    }
}

TEST_CASE("many-body_tensor_representation_periodic", "[representations],[many-body_tensor]")
{
    using std::vector;
    auto const NaCl = PeriodicTestSystems::NaCl; auto const Al = PeriodicTestSystems::Al; auto const SF6 = PeriodicTestSystems::SF6;
    typedef mbtr_normal_distribution normal;
    typedef mbtr_identity_correlation ident;

    SECTION( "k = 1, unity, unity, simple test cases" )
    {
        // NaCl
        {
            PeriodicTestSystems s(NaCl);
            const long n = 1 * 2 * 3;
            double mbtr[n]; std::fill(mbtr, mbtr+n, -99.);
            mbtr_call<1, mbtr_geomf_unity, mbtr_weightf_unity, normal, ident, mbtr_eindexf_dense_noreversals, mbtr_aindexf_periodic_full<long>>
                (mbtr, s, -0.5, 1, 3, mbtr_geomf_unity(), mbtr_weightf_unity(), normal(1e-10));
            CHECK( vector<double>(mbtr, mbtr+n) == (vector<double>{ 0, 1, 0,  0, 1, 0 }) );
        }
    }

    SECTION( "k = 2, inverse distance, delta weighting" )
    {
        // Al - no neighbors
        {
            typedef mbtr_weightf_delta_one_over_identity weightf_t;
            PeriodicTestSystems s(Al);
            weightf_t weightf(2.8);
            const long n = 1 * 1 * 1;  // ns * nec * xdim
            double mbtr[n]; std::fill(mbtr, mbtr+n, -99.);
            mbtr_call<2, mbtr_geomf2_one_over_distance, weightf_t, normal, ident, mbtr_eindexf_dense_noreversals, mbtr_aindexf_periodic_full<long>>
                (mbtr, s, -10, 20, 1, mbtr_geomf2_one_over_distance(), weightf, normal(1e-10));
            CHECK( vector<double>(mbtr, mbtr+n) == (vector<double>{ 0 }) );
        }

        // Al - first neighbors
        {
            typedef mbtr_weightf_delta_one_over_identity weightf_t;
            PeriodicTestSystems s(Al);
            weightf_t weightf(2.87);
            const long n = 1 * 1 * 10;  // ns * nec * xdim
            double mbtr[n]; std::fill(mbtr, mbtr+n, -99.);
            mbtr_call<2, mbtr_geomf2_one_over_distance, weightf_t, normal, ident, mbtr_eindexf_dense_noreversals, mbtr_aindexf_periodic_full<long>>
                (mbtr, s, 0, 0.2, 10, mbtr_geomf2_one_over_distance(), weightf, normal(1e-10));
            CHECK( vector<double>(mbtr, mbtr+n) == (vector<double>{ 0, 12, 0, 0, 0, 0, 0, 0, 0, 0 }) );
        }

        // Al - more neighbors
        {
            typedef mbtr_weightf_delta_one_over_identity weightf_t;
            PeriodicTestSystems s(Al);
            weightf_t weightf(5.72687);
            const long n = 1 * 1 * 20;  // ns * nec * xdim
            double mbtr[n]; std::fill(mbtr, mbtr+n, -99.);
            mbtr_call<2, mbtr_geomf2_one_over_distance, weightf_t, normal, ident, mbtr_eindexf_dense_full, mbtr_aindexf_periodic_full<long>>
                (mbtr, s, 0, 0.025, 20, mbtr_geomf2_one_over_distance(), weightf, normal(1e-10));
            CHECK( vector<double>(mbtr, mbtr+n) == (vector<double>{ 0, 0, 0, 0, 0, 0, 12, 0, 24, 6, 0, 0, 0, 12, 0, 0, 0, 0, 0, 0 }) );
        }
    }
}
